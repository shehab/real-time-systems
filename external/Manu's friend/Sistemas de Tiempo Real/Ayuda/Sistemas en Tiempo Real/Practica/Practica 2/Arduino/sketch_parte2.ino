
#define SAMPLE_RATE 4000
#define MUTE_BUTTON_CYCLE_MS 100
#define PIN_AUDIO 11
#define PIN_BUTTON 7
#define PIN_LED 13



int mute;
long initial_time=0;
long final_time=0;
int button_status;


void setup(){ 
pinMode(PIN_AUDIO, OUTPUT);
pinMode(PIN_LED,OUTPUT);
pinMode(PIN_BUTTON,INPUT);

button_status=LOW;
mute=0;


Serial.begin(115200);

//configuramos el timer 1 para lanzar una interrupcion 4000 veces por segundo
        
//en primer lugar configuramos el modo (CTC con maximo en ICR1 y preescalado 1)        
               TCCR1B=_BV(WGM12)|_BV(WGM13)|_BV(CS10);
               TCCR1A=0;

//en segundo lugar establecemos el maximo que tomara el timer, al alcanzar este valor lanzara una interrupcion
        
              ICR1=F_CPU/SAMPLE_RATE; //16e6/4e3=4e3HZ;
       
//por ultimo configuramos la interrupcion      
              
                TIMSK1 = _BV(OCIE1A);


//configuramos el timer 2 para generar una señal PWM en modo FPWM, preescalado 1 y salida no invertida
                TCCR2A=_BV(WGM20)|_BV(WGM21)|_BV(COM2A1);
                TCCR2B=_BV(CS20);







//obtenemos el tiempo inicial
initial_time=millis();

}



ISR(TIMER1_COMPA_vect)        
{
  static unsigned char data = 0;
    
    
    if (Serial.available()>1) {
           data = Serial.read();
        }
      if(!mute){
         OCR2A=data;
      }else{
         OCR2A=0;
      }
}


int update_mute(){
 int button_value=digitalRead(PIN_BUTTON);
  //hemos detectado un cambio en el estado del boton
  if(button_value==HIGH && button_status==LOW){ 
			
			//desactivamos las interrupciones para que la modificacion de la variable  mute no altere la lectura en la interrupcion
			//en este caso, desactivar las interrupciones equivale a bloquear un mutex
		noInterrupts();
   mute=HIGH;  
			//volvemos a activar las interrupciones
		  interrupts();
  }else if(button_value==HIGH && button_status==HIGH){
   digitalWrite(PIN_LED,LOW); 
  		  noInterrupts();
   mute=LOW;  
		  interrupts();
}   
  digitalWrite(PIN_LED,mute); 
  button_status=!button_status;
 return 0; 
}





void loop(){

  
  update_mute();
  
  final_time=millis();
    //si nos hemos pasado del tiempo del ciclo-> error
  if(MUTE_BUTTON_CYCLE_MS-(final_time-initial_time)<0){  
    Serial.println("ERROR TEMPORAL"); 
  }
  
  
  //dormimos la diferencia
  delay(MUTE_BUTTON_CYCLE_MS-(final_time-initial_time));
  //añadimos 100ms al tiempo inical
  initial_time+=+MUTE_BUTTON_CYCLE_MS;
  
  
}
