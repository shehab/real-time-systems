// --------------------------------------
// Include
// --------------------------------------
#include <string.h>
#include <stdio.h>






// --------------------------------------
// PINs 
// --------------------------------------
#define GASPIN 13
#define BRKPIN 12
#define MIXPIN 11
#define SWITCH1 8
#define SWITCH2 9
#define SPEEDPIN  10
#define LAMPPIN 7
#define LITPIN  A0
#define STPPIN  A1
#define BTNPIN 2
#define A  4
#define B  6
#define C  5
#define D  3

//0->seleccion de distancia
//1->Modo acercamiento
//2->Modo parada
//3->Modo emergencia
#define MODO_PARADA 2
#define MODO_SELECCION 0
#define MODO_ACERCAMIENTO 1
#define MODO_PARADA_EMERGENCIA 3

// --------------------------------------
// Global Variables
// --------------------------------------
int BRK=0; //freno
int GAS=0; //acelerador
int MIX=0; //mezclador
int SLP=0; //pendiente
int LAM=0; //faros
int LIT=0; //luminosidad



float velocidad_carretilla;   //velocidad carretilla
long distancia_leida=0;   //distancia leida desde el potenciometro
long distancia_actual=0;    //distancia actual al deposito
long Tinit=0;   //tiempo inicial
long Tend=0;    //tiempo final
int ciclo_secundario;   //ciclo secundario actual
int NUMERO_CICLOS;    //numero de ciclos secundarios
float DURACION_CS;    //duracion del ciclo secundario
int valor_boton=0;    //ultimo valor leido desde el boton
int modo=0;   //modo actual de ejecucion


//borrado del numero mostrado en el display
void clean(){

  digitalWrite(A,LOW);
  digitalWrite(B,LOW);
  digitalWrite(C,LOW);
  digitalWrite(D,LOW);


}

//funcion para mostrar un numero (0..9) en el display
void draw(int number){

  switch(number){

  case 0:

    break;

  case 1:
    digitalWrite(A,HIGH);

    break;
  case 2:
    digitalWrite(B,HIGH);

    break;
  case 3:
    digitalWrite(A,HIGH);
    digitalWrite(B,HIGH);
    break;
  case 4:
    digitalWrite(C,HIGH);
    break;
  case 5:
    digitalWrite(C,HIGH);
    digitalWrite(A,HIGH);

    break;
  case 6:
    digitalWrite(C,HIGH);
    digitalWrite(B,HIGH);
    break;
  case 7:
    digitalWrite(A,HIGH);
    digitalWrite(B,HIGH);
    digitalWrite(C,HIGH);
    break;
  case 8:
    digitalWrite(D,HIGH);

    break;  
  case 9:
    digitalWrite(D,HIGH);
    digitalWrite(A,HIGH);


    break;
//en caso de un valor distinto de los aceptados, el display se apaga
  default:
digitalWrite(D,HIGH);
digitalWrite(C,HIGH);
    break; 

  };

} 



int comm_server()
{
  int i;
  char request[10];
  char answer[10];
  int speed_int;
  int speed_dec;

  //si hay 9 caracteres listos para leer
  if (Serial.available() >= 9) {
    // leemos la peticion
    i=0; 
    while ( (i<9) && (Serial.available() >= (9-i)) ) {
      // leemos de 1 en 1 y copiamos la request al vector
      request[i]=Serial.read();

      // si el caracter de fin de linea no esta puesto en el lugar correcto 
      if ( (i!=8) && (request[i]=='\n') ) {
        // Pintamos un error y comenzamos de nuevo
        sprintf(answer,"MSG: ERR\n");
        Serial.print(answer);
        memset(request,'\0', 9);
        i=0;
      } 
      else {
        //si no hay error, seguimos leyendo
        i++;
      }
    }
    //ponemos el caracter de fin de cadena al final
    request[9]='\0';

    // PETICION DE VELOCIDAD
    if (0 == strcmp("SPD: REQ\n",request)) {
      //enviamos la respuesta a la peticion
      speed_int = (int)velocidad_carretilla;
      speed_dec = ((int)(velocidad_carretilla*10)) % 10;
      sprintf(answer,"SPD:%02d.%d\n",speed_int,speed_dec);
      Serial.print(answer);


      //PETICION PENDIENTE
    }
    else if(0==strcmp("SLP: REQ\n",request)){

      //respondemos con el valor de la pendiente

      switch(SLP){

      case 0:
        //la carretilla va en terreno plano
        sprintf(answer,"SLP:FLAT\n");
        Serial.print(answer); 

        break;

      case 1:
        //la carretilla va en penidente ascendente
        sprintf(answer,"SLP:  UP\n");
        Serial.print(answer); 

        break;

      case -1:
        //la carretilla va en penidente desscendente
        sprintf(answer,"SLP:DOWN\n");
        Serial.print(answer); 


        break;

      }


      //ACTIVAR FRENO
    }
    else if(0==strcmp("BRK: SET\n",request)){

      //si el FRENO esta desactivado
      if(!BRK){
        //actualizamos el estado del freno
        BRK=1;
        //enviamos la respuesta
        sprintf(answer,"BRK:  OK\n");
        Serial.print(answer);

        //si el FRENO ya esta activado -> Error
      }
      else{
        sprintf(answer,"MSG: ERR\n");
        Serial.print(answer);

      }





      //DESCACTIVAR FRENO
    }
    else if(0==strcmp("BRK: CLR\n",request)){

      //si el FRENO esta activado
      if(BRK){
        //actualizamos el estado del freno
        BRK=0;
        //enviamos la respuesta
        sprintf(answer,"BRK:  OK\n");
        Serial.print(answer);

        //si el FRENO ya esta desactivado -> Error
      }
      else{
        sprintf(answer,"MSG: ERR\n");
        Serial.print(answer);

      }





      //ACTIVAR ACELERADOR
    }
    else if(0==strcmp("GAS: SET\n",request)){

      //si el acelerador esta desactivado
      if(!GAS){
        //actualizamos el estado del acelerador
        GAS=1;
        //enviamos la respuesta
        sprintf(answer,"GAS:  OK\n");
        Serial.print(answer);

        //si el acelerador ya esta activado -> Error
      }
      else{
        sprintf(answer,"MSG: ERR\n");
        Serial.print(answer);

      }


      //DESACTIVAR ACELERADOR
    }
    else if(0==strcmp("GAS: CLR\n",request)){
      //si el acelerador esta activado
      if(GAS){
        //actualizamos el estado del acelerador
        GAS=0;
        //enviamos la respuesta
        sprintf(answer,"GAS:  OK\n");
        Serial.print(answer);

        //si el acelerador ya esta desactivado -> Error
      }
      else{
        sprintf(answer,"MSG: ERR\n");
        Serial.print(answer);

      }


      //ACTIVAR MIXER
    }
    else if(0==strcmp("MIX: SET\n",request)){

      //si el MIXER esta desactivado
      if(!MIX){
        //actualizamos el estado del mixe
        MIX=1;
        //enviamos la respuesta
        sprintf(answer,"MIX:  OK\n");
        Serial.print(answer);

        //si el mixer ya esta activado -> Error
      }
      else{
        sprintf(answer,"MSG: ERR\n");
        Serial.print(answer);

      }     


      //desactivar MIXER
    }
    else if(0==strcmp("MIX: CLR\n",request)){

      //si el MIXER esta activado
      if(MIX){
        //actualizamos el estado del mixer
        MIX=0;
        //enviamos la respuesta
        sprintf(answer,"MIX:  OK\n");
        Serial.print(answer);

        //si el mixer ya esta desactivado -> Error
      }
      else{
        sprintf(answer,"MSG: ERR\n");
        Serial.print(answer);

      }



      //PETICION LUMINOSIDAD
    }
    else if(0==strcmp("LIT: REQ\n",request)){


      //enviamos la respuesta
      sprintf(answer,"LIT: %02d%%\n",LIT);
      Serial.print(answer); 




    }//ENCENDER FAROS
    else if(0==strcmp("LAM: SET\n",request)){

     

        LAM=1;
        sprintf(answer,"LAM:  OK\n");
        Serial.print(answer);


     

    }
    //APAGAR FAROS
    else if(0==strcmp("LAM: CLR\n",request)){

      //si los faros estan encendidos, los apagamos
      

        LAM=0;
        sprintf(answer,"LAM:  OK\n");
        Serial.print(answer);


      
      
      //PETICION ESTADO MOVIMIENTO
    }
    else if(0==strcmp("STP: REQ\n",request)){

      if(modo==MODO_SELECCION||modo==MODO_ACERCAMIENTO){
 
         sprintf(answer,"STP:  GO\n");
          Serial.print(answer);
 
      }
      else{
        sprintf(answer,"STP:STOP\n");
        Serial.print(answer);
      }

      



    } 
    //PETICION DISTANCIA
    else if(0==strcmp("DS:  REQ\n",request)){

      sprintf(answer,"DS:%5li\n",distancia_actual);
      Serial.print(answer);

      //PASO A MODO EMERGENCIA
    }else if(0==strcmp("ERR: SET\n",request)){
     
     sprintf(answer,"ERR:  OK\n");
     
     modo=MODO_PARADA_EMERGENCIA;
     //apagamos el display
     draw(10);
      Serial.print(answer);
     
      
    }else{

      //no se ha podido iodentificar la peticion
      sprintf(answer,"MSG: ERR\n");
      Serial.print(answer);

    }


  }
  return 0;
}

int leer_distancia(){

  distancia_leida=analogRead(STPPIN);
  
distancia_actual=map(distancia_leida,817,1023,10000,90000); 

  return 0;
}

int acelerador(){
  //actualizamos el estado del acelerador
  
  
  if(modo==MODO_PARADA_EMERGENCIA){
  GAS=0;
  }  
  digitalWrite(GASPIN,GAS);
  return 0; 
}


int freno(){ 
  //actualizamos el estado del freno 
  
  
    if(modo==MODO_PARADA_EMERGENCIA){
  BRK=1;
  }
  digitalWrite(BRKPIN,BRK);
  return 0; 
}


int velocidad(){
  //calculamos la velocidad segun la ec. del mov. uniformemente acelerado
  float aceleracion=0; 
  int velocidad_led=0;
  //si el modo parada no esta activo
  //si el acelerador esta activo(GAS) la aceleracion sera 0.5m/s2
  //si el acelerador esta activo(BRK) la aceleracion sera -0.5m/s2
  //segun el valor de la pendiente (SLP) la aceleracion sera a favor (SLP*0.5 >0)  o en contra (SLP*0.5 <0)
  
  aceleracion=(GAS*0.5)-(BRK*0.5)-(SLP*0.25);

  //en el M.U.A la velocidad se calcula como V=v0+a*t. Como la medicion se hace cada 100ms t=100ms

  velocidad_carretilla=velocidad_carretilla+(aceleracion*(DURACION_CS/1000.0));  

  //el brillo del LED varia segun la velocidad
  velocidad_led=map(velocidad_carretilla,40,70,0,255);

    //si la velocidad esta fuera de los limites, apagamos el led
  if(velocidad_carretilla>70 || velocidad_carretilla<40){

    velocidad_led=LOW; 
  }


   


  analogWrite(SPEEDPIN,velocidad_led);

//mantenemos la velocidad a 0 en cas de que sea menor
if(velocidad_carretilla<0){
  velocidad_carretilla=0;
}

  
  return 0; 
}



int mezclador(){

  //actualizamos el estado del mezclador
  digitalWrite(MIXPIN,MIX);
  return 0;
}






int pendiente(){


  if(digitalRead(SWITCH1)==HIGH && digitalRead(SWITCH2)==LOW ){

    //pendiente ascendente
    SLP=1;
  } 


  if(digitalRead(SWITCH1)==LOW && digitalRead(SWITCH2)==LOW ){

    //sin pendiente (terreno plano)
    SLP=0;  
  }

  if(digitalRead(SWITCH1)==LOW && digitalRead(SWITCH2)==HIGH ){


    //pendiente descendente
    SLP=-1; 
  }
}


int focos(){

    //actualizamos el valor de los focos
     if(modo==MODO_PARADA_EMERGENCIA){
 LAM=1;
  }
  digitalWrite(LAMPPIN,LAM);

  return 0; 
}


int velocidad_parada(){
  velocidad_carretilla=0;
  analogWrite(SPEEDPIN,velocidad_carretilla);
  

  //al entrar en modo parada apagamos el display
  clean();
  draw(10);
  
 return 0; 
}


int mostrar_distancia(){
  clean();

  int distancia_miles=distancia_actual/10000;

  draw(distancia_miles);
}





int lectura_luminosidad(){
  int luminosidad=0;

  //leemos de la entrada analogica conectada a la LDR
  luminosidad=analogRead(LITPIN);

  //en base al valor leido, obtenemos un valor dentro de la escala adecuad
  LIT=map(luminosidad,0,1023,0,100); 


  return 0; 
}


int estado_boton(){


  if(digitalRead(BTNPIN)==HIGH){
    valor_boton=HIGH;
  
  }else if(digitalRead(BTNPIN)==0 && valor_boton==HIGH){
    valor_boton=LOW;
   
    if(modo==MODO_SELECCION){
      modo=MODO_ACERCAMIENTO;
      
      
    }else if(modo==MODO_PARADA){
     modo=MODO_SELECCION; 
     
    }

  }
  return 0;
}

int calcular_distancia(){


  
  
 distancia_actual=distancia_actual-(velocidad_carretilla*(DURACION_CS/1000.0));
 
  if(distancia_actual<=0 && velocidad_carretilla< 10){
    
    distancia_actual=0;
    modo=MODO_PARADA;
    
    
  }else if( distancia_actual<=0 && velocidad_carretilla>10){
    
    distancia_actual=0;
    modo=MODO_SELECCION;
    
  }
  
  clean();
  draw(distancia_actual/10000);
  
  
  return 0;
}


void setup(){

  //obtenemos el tiempo inicial
  Tinit=millis();

  //inicializamos los pines que se usaran, en los correspondientes modos

    //pines para salida digital(freno,acelerador y mezclador)
  pinMode(BRKPIN,OUTPUT);
  pinMode(GASPIN,OUTPUT);
  pinMode(MIXPIN,OUTPUT);

  //pin para salida analogica (LED velocidad)
  pinMode(SPEEDPIN,OUTPUT);

  //pines para entrada digital (pulsador de 3 posiciones para pendiente)
  pinMode(SWITCH1,INPUT);
  pinMode(SWITCH2,INPUT);
  pinMode(BTNPIN,INPUT);
  pinMode(A,OUTPUT);
  pinMode(B,OUTPUT);
  pinMode(C,OUTPUT);
  pinMode(D,OUTPUT);



  //iniciamos el bus serial a 9600 baudios
  Serial.begin(9600);


  //establecemos la velocidad de la carretilla a un valor seguro
  velocidad_carretilla=55.0;

  //LA CARRWETILLA SIEMPRE SE MUEVE HASTA QUE SE CAMBIA EL MODO
  //comenzamos en modo seleccion
 modo=MODO_SELECCION;
  //establecemos el numero de ciclos, el ciclo inicial y la duracion de cada ciclo
  ciclo_secundario=0;

  //200/100=2
  NUMERO_CICLOS=2;

  //cada CS dura 100ms (el min. de los periodos)
  DURACION_CS=100;

  //obtenemos el tiempo inicial
  Tinit=millis();

}

void modo_seleccion(){
   // Serial.println("**********************MODO SELECCION**********************");

  acelerador();
  freno();
  velocidad();
  mezclador();
  pendiente();
  focos();
  lectura_luminosidad();
  leer_distancia();
  mostrar_distancia();

  estado_boton();

}


void modo_acercamiento(){
    //  Serial.println("**********************MODO ACERCAMIENTO**********************");

  acelerador();
  freno();
  velocidad();
  mezclador();
  pendiente();
  focos();
  lectura_luminosidad();
  calcular_distancia();


}
void modo_parada(){
//Serial.println("**********************MODO PARADA**********************");
  
  mezclador();
  pendiente();
  focos();
  velocidad_parada();
  estado_boton();


}

void modo_emergencia(){
//Serial.println("**********************MODO EMERGENCIA**********************");

  acelerador();
  freno();
  velocidad();
  mezclador();
  pendiente();
  focos();

}


void loop(){


  switch(ciclo_secundario){

  case 0:

    comm_server();


    if(modo==MODO_SELECCION){

      modo_seleccion();
    }
    else if(modo==MODO_ACERCAMIENTO){

      modo_acercamiento();


    }else if(modo==MODO_PARADA){
      
      modo_parada();
      
    }else if(modo==MODO_PARADA_EMERGENCIA){
     
      modo_emergencia();
     
    } 
    

    break;


  case 1:

    if(modo==MODO_SELECCION){
    
      modo_seleccion();

    }
    else if(modo==MODO_ACERCAMIENTO){
    
      modo_acercamiento();

    }else if(modo==MODO_PARADA){
  
        modo_parada();
      
    }else if(modo==MODO_PARADA_EMERGENCIA){
        
      modo_emergencia();
  
    }    

    break;

  }

  //nos movemos hasta el proximo ciclo
  ciclo_secundario=(ciclo_secundario+1)%NUMERO_CICLOS;
  //obtenemos el tiempo final
  Tend=millis();


  //si se ha tardado mas tiempo que el dedicado a un CS-> error
  if((DURACION_CS-(Tend-Tinit))<0){

    Serial.print("ERROR TEMPORAL");

  }


  //completamos el CS
  delay(DURACION_CS - (Tend-Tinit));

  //calculamos un nuevo tiempo inicial
  Tinit=Tinit+DURACION_CS;




}











