/**********************************************************
 *  INCLUDES
 *********************************************************/
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <ioLib.h>
#include "displayC.h"
#include "serialcallLib.h"

/**********************************************************
 *  Constants
 **********************************************************/
#define NS_PER_S 1000000000
#define TIEMPO_CICLO_SECUNDARIO_SEGUNDOS 5 
#define TIEMPO_CICLO_SECUNDARIO_NANOSEGUNDOS 0
#define NUM_CICLOS_SECUNDARIOS 2

/**********************************************************
 *  Global Variables 
 *********************************************************/
int GAS = 0;
int BRK = 0;
int MIX = 0;
int SLP = 0;
int SPD = 0;
int LIT = 0;
int LAM = -1;
int STP = 1;
int DS = -1;
float speed = 0.0;
char request[9];
char answer[9];
struct timespec time_mezclador, tiempo, time_mezcla, comparar;

/**********************************************************
 *  Function: addTime 
 *********************************************************/
void addTime(struct timespec end, 
			  struct timespec start, 
			  struct timespec *add) 
{
	unsigned long aux;
	aux = start.tv_nsec + end.tv_nsec;
	add->tv_sec = start.tv_sec + end.tv_sec + 
			      (aux / NS_PER_S);
	add->tv_nsec = aux % NS_PER_S;
}

/**********************************************************
 *  Function: diffTime 
 *********************************************************/
void diffTime(struct timespec end, 
			  struct timespec start, 
			  struct timespec *diff) 
{
	if (end.tv_nsec < start.tv_nsec) {
		diff->tv_nsec = NS_PER_S - start.tv_nsec + end.tv_nsec;
		diff->tv_sec = end.tv_sec - (start.tv_sec+1);
	} else {
		diff->tv_nsec = end.tv_nsec - start.tv_nsec;
		diff->tv_sec = end.tv_sec - start.tv_sec;
	}
}

/**********************************************************
 *  Function: compTime 
 *********************************************************/
int compTime(struct timespec t1, 
			  struct timespec t2)
{
	if (t1.tv_sec == t2.tv_sec) {
		if (t1.tv_nsec == t2.tv_nsec) {
			return (0);
		} else if (t1.tv_nsec > t2.tv_nsec) {
			return (1);
		} else if (t1.tv_sec < t2.tv_sec) {
			return (-1);
		}
	} else if (t1.tv_sec > t2.tv_sec) {
		return (1);
	} else if (t1.tv_sec < t2.tv_sec) {
		return (-1);
	} 
	return (0);
}

/**********************************************************
 *  Function: pendiente
 *********************************************************/
int pendiente() {
	//clear request and answer
	memset(request, '\0', 9);
	memset(answer, '\0', 9);
	
	// request slope
	strcpy(request, "SLP: REQ\n");
	
	//uncomment to use the simulator
	simulator(request, answer);
	// uncoment to access serial module
	//writeSerialMod_9(request);
	//readSerialMod_9(answer);
	
	// display slope
	if (0 == strcmp(answer, "SLP:DOWN\n"))
		displaySlope(-1);
	if (0 == strcmp(answer, "SLP:FLAT\n"))
		displaySlope(0);
	if (0 == strcmp(answer, "SLP:  UP\n"))
		displaySlope(1);

	return 0;
}

/**********************************************************
 *  Function: velocidad
 *********************************************************/
int velocidad() {
	//clear request and answer
	memset(request, '\0', 9);
	memset(answer, '\0', 9);
	
	// Mensaje de peticion de velocidad
	strcpy(request,"SPD: REQ\n");
	
	//uncomment to use the simulator
	simulator(request, answer);
	// uncoment to access serial module
	//writeSerialMod_9(request);
	//readSerialMod_9(answer);
	
	if (1 == sscanf (answer,"SPD:%f\n",&speed)) {
		// display speed
		displaySpeed(speed);
		if (speed < 55.0) {
			BRK = 0;
			GAS = 1;
		}
		if (speed >= 55.0) {
			BRK = 1;
			GAS = 0;
		}
	}
	return 0;
}

/**********************************************************
 *  Function: velocidadModoFrenado
 *********************************************************/
int velocidadModoFrenado() {
	//clear request and answer
	memset(request, '\0', 9);
	memset(answer, '\0', 9);
	
	// Mensaje de peticion de velocidad
	strcpy(request,"SPD: REQ\n");
	
	//uncomment to use the simulator
	simulator(request, answer);
	// uncoment to access serial module
	//writeSerialMod_9(request);
	//readSerialMod_9(answer);
	
	if (1 == sscanf (answer,"SPD:%f\n",&speed)) {
		// display speed
		displaySpeed(speed);
		if (speed < 2.5) {
			BRK = 0;
			GAS = 1;
		}else{
			BRK = 1;
			GAS = 0;
		}
	}
	return 0;
}

/**********************************************************
 *  Function: acelerador
 *********************************************************/
int acelerador() {
	//clear request and answer
	memset(request, '\0', 9);
	memset(answer, '\0', 9);
	if (GAS == 1) {
		// Activamos el acelerador
		strcpy(request, "GAS: SET\n");
	}
	if (GAS == 0) {
		// Desactivamos el acelerador
		strcpy(request, "GAS: CLR\n");
	}
	
	//uncomment to use the simulator
	simulator(request, answer);
	// uncoment to access serial module
	//writeSerialMod_9(request);
	//readSerialMod_9(answer);
	
	// Si la respuesta es OK lo mostramos
	if (0 == strcmp(answer, "GAS:  OK\n")) {
		displayGas(GAS);
	}
	return 0;
}

/**********************************************************
 *  Function: freno
 *********************************************************/
int freno() {
	//clear request and answer
	memset(request, '\0', 9);
	memset(answer, '\0', 9);
	if (BRK == 1) {
		strcpy(request, "BRK: SET\n");
	}
	if (BRK == 0) {
		strcpy(request, "BRK: CLR\n");
	}
	
	//uncomment to use the simulator
	simulator(request, answer);
	// uncoment to access serial module
	//writeSerialMod_9(request);
	//readSerialMod_9(answer);
	
	if (0 == strcmp(answer, "BRK:  OK\n")) {
		displayBrake(BRK);
	}

	return 0;
}

/**********************************************************
 *  Function: mezclador
 *********************************************************/
int mezclador() {
	//clear request and answer
	memset(request, '\0', 9);
	memset(answer, '\0', 9);
	
	comparar.tv_sec = 30;
	comparar.tv_nsec = 0;
	clock_gettime(CLOCK_REALTIME, &tiempo);
	
	diffTime(tiempo, time_mezclador, &time_mezcla);
	
	if (compTime(time_mezcla, comparar) == 1) {
		if (MIX == 0) {
			MIX = 1;
			clock_gettime(CLOCK_REALTIME, &time_mezclador);
			strcpy(request, "MIX: SET\n");
		} else {
			if (MIX == 1) {
				MIX = 0;
				clock_gettime(CLOCK_REALTIME, &time_mezclador);
				strcpy(request, "MIX: CLR\n");
			}
		}
		
		//uncomment to use the simulator
		simulator(request, answer);
		// uncoment to access serial module
		//writeSerialMod_9(request);
		//readSerialMod_9(answer);
	
		if (0 == strcmp(answer, "MIX:  OK\n")) {
			displayMix(MIX);
		}
	}
	return 0;
}

/**********************************************************
 *  Function: luminosidad
 *********************************************************/
int luminosidad() {
	//clear request and answer
	memset(request, '\0', 9);
	memset(answer, '\0', 9);
	// Peticion de luminosidad
	strcpy(request, "LIT: REQ\n");
	
	//uncomment to use the simulator
	simulator(request, answer);
	// uncoment to access serial module
	//writeSerialMod_9(request);
	//readSerialMod_9(answer);
	if (1 == sscanf(answer, "LIT: %i%\n", &LIT)) {
		if (LIT <= 50) {
			LAM = 1;
			displayLightSensor(LAM);
		} else {
			LAM = 0;
			displayLightSensor(LAM);
		}
	}
	return 0;
}

/**********************************************************
 *  Function: focos
 *********************************************************/
int focos() {
	//clear request and answer
	memset(request, '\0', 9);
	memset(answer, '\0', 9);
	if (LAM == 1) {
		// Peticion de activar los focos
		strcpy(request, "LAM: SET\n");
	}else{
		// Peticion de activar los focos
		strcpy(request, "LAM: CLR\n");
	}
	
	//uncomment to use the simulator
	simulator(request, answer);
	// uncoment to access serial module
	//writeSerialMod_9(request);
	//readSerialMod_9(answer);
	
	if (0 == strcmp(answer, "LAM:  OK\n")) {
		displayLamps(LAM);
	}
	return 0;
}

/**********************************************************
 *  Function: distancia
 *********************************************************/
int distancia() {
	//clear request and answer
	memset(request, '\0', 9);
	memset(answer, '\0', 9);
	// Peticion de distancia a la que se encuentra del deposito
	strcpy(request, "DS:  REQ\n");
	
	//uncomment to use the simulator
	simulator(request, answer);
	// uncoment to access serial module
	//writeSerialMod_9(request);
	//readSerialMod_9(answer);
	
	if (1 == sscanf(answer, "DS:%5d\n", &DS)) {
		if (DS >= 0) {
			displayDistance(DS);
		}
	}

	return 0;
}

/**********************************************************
 *  Function: finDescarga
 *********************************************************/
int finDescarga() {
	//clear request and answer
	memset(request, '\0', 9);
	memset(answer, '\0', 9);
	strcpy(request, "STP: REQ\n");
	
	//uncomment to use the simulator
	simulator(request, answer);
	// uncoment to access serial module
	//writeSerialMod_9(request);
	//readSerialMod_9(answer);
	
	if (0 == strcmp(answer, "STP:  GO\n")) {
		STP = 0;
		displayStop(STP);
	}
	if (0 == strcmp(answer, "STP:STOP\n")) {
		STP = 1;
		displayStop(STP);
	}
	return 0;
}

/**********************************************************
 *  Function: modoNormal
 *********************************************************/
int modoNormal() {
	//clear request and answer
	memset(request, '\0', 9);
	memset(answer, '\0', 9);
	struct timespec timeInit, timeEnd, timeDiff, timePeriod;
	
	int cicloSecundario = 0;
	timePeriod.tv_sec = TIEMPO_CICLO_SECUNDARIO_SEGUNDOS;
	timePeriod.tv_nsec = TIEMPO_CICLO_SECUNDARIO_NANOSEGUNDOS;
	
	// Obtiene el tiempo inicial
    clock_gettime(CLOCK_REALTIME, &timeInit);
	
	// Comprobamos la distancia a la que se encuentra del deposito
	distancia();
	
	while (DS >= 11000) {
		switch (cicloSecundario) {
		case 0:
			luminosidad();
			focos();
			pendiente();
			velocidad();
			acelerador();
			break;
		case 1:
			luminosidad();
			focos();
			freno();
			mezclador();
			distancia();
			break;
		}
		cicloSecundario = (cicloSecundario + 1) % NUM_CICLOS_SECUNDARIOS;
		clock_gettime(CLOCK_REALTIME, &timeEnd);
		
		diffTime(timeEnd, timeInit, &timeDiff);
		diffTime(timePeriod, timeDiff, &timeDiff);
		
		if (compTime(timePeriod, timeDiff)<0 ) printf("TIEMPO NEGATIVO");
		
		nanosleep(&timeDiff, NULL);
		addTime(timeInit, timePeriod, &timeInit);
	}
	return 0;
}

/**********************************************************
 *  Function: modoFrenado
 *********************************************************/
int modoFrenado() {
	//clear request and answer
	memset(request, '\0', 9);
	memset(answer, '\0', 9);
	struct timespec timeInit, timeEnd, timeDiff, timePeriod;
	
	int cicloSecundario = 0;
	timePeriod.tv_sec = TIEMPO_CICLO_SECUNDARIO_SEGUNDOS;
	timePeriod.tv_nsec = TIEMPO_CICLO_SECUNDARIO_NANOSEGUNDOS;
	
	// Obtiene el tiempo inicial
    clock_gettime(CLOCK_REALTIME, &timeInit);
	
	// Activamos los focos de forma permanente en este modo
	LAM = 1;
	
	// Comprobamos la distancia a la que se encuentra del deposito
	distancia();
	
	while ((DS > 0) | (speed >= 10)) {
		switch (cicloSecundario) {
		case 0:
			velocidadModoFrenado();
			acelerador();
			freno();
			pendiente();
			mezclador();
			break;
		case 1:
			velocidadModoFrenado();
			acelerador();
			freno();
			distancia();
			focos();
			break;
		}
		cicloSecundario = (cicloSecundario + 1) % NUM_CICLOS_SECUNDARIOS;
		clock_gettime(CLOCK_REALTIME, &timeEnd);
		
		diffTime(timeEnd, timeInit, &timeDiff);
		diffTime(timePeriod, timeDiff, &timeDiff);
		
		if (compTime(timePeriod, timeDiff)<0 ) printf("TIEMPO NEGATIVO");
		
		nanosleep(&timeDiff, NULL);
		addTime(timeInit, timePeriod, &timeInit);
	}
	return 0;
}

/**********************************************************
 *  Function: modoDescarga
 *********************************************************/
int modoDescarga() {
	//clear request and answer
	memset(request, '\0', 9);
	memset(answer, '\0', 9);
	struct timespec timeInit, timeEnd, timeDiff, timePeriod;
	
	timePeriod.tv_sec = TIEMPO_CICLO_SECUNDARIO_SEGUNDOS;
	timePeriod.tv_nsec = TIEMPO_CICLO_SECUNDARIO_NANOSEGUNDOS;
	
	// Obtiene el tiempo inicial
    clock_gettime(CLOCK_REALTIME, &timeInit);
	
	// Activamos los focos de forma permanente en este modo
	LAM = 1;
	
	while (STP == 1) {
		finDescarga();
		mezclador();
		focos();
		
		clock_gettime(CLOCK_REALTIME, &timeEnd);
		
		diffTime(timeEnd, timeInit, &timeDiff);
		diffTime(timePeriod, timeDiff, &timeDiff);
		
		if (compTime(timePeriod, timeDiff)<0 ) printf("TIEMPO NEGATIVO");
		
		nanosleep(&timeDiff, NULL);
		addTime(timeInit, timePeriod, &timeInit);
	}
	// Volvemos a poner el valor a uno para la siguiente descarga
	STP = 1;
	return 0;
}

/**********************************************************
 *  Function: controller
 *********************************************************/
void *controller(void *arg)
{
	// Obtiene el tiempo inicial del mezclador
	clock_gettime(CLOCK_REALTIME, &time_mezclador);
    while(1) {
		modoNormal();
		modoFrenado();
		modoDescarga();
    }
}

/**********************************************************
 *  Function: main
 *********************************************************/
int main ()
{
    pthread_t thread_ctrl;
	sigset_t alarm_sig;
	int i;
	/* Block all real time signals so they can be used for the timers.
	   Note: this has to be done in main() before any threads are created
	   so they all inherit the same mask. Doing it later is subject to
	   race conditions */
	
	sigemptyset (&alarm_sig);
	for (i = SIGRTMIN; i <= SIGRTMAX; i++) {
		sigaddset (&alarm_sig, i);
	}
	sigprocmask (SIG_BLOCK, &alarm_sig, NULL);
	
    // init display
	displayInit(SIGRTMAX);
	
	// initSerialMod_9600 uncomment to work with serial module
	//initSerialMod_WIN_9600 ();
	
    /* Create first thread */
    pthread_create (&thread_ctrl, NULL, controller, NULL);
    pthread_join (thread_ctrl, NULL);
    return (0);
}
