#define LDR A3
#define LED 9





void setup(){
//configuramos el led para que actue como una salida  
pinMode(LED,OUTPUT);  
/*configuramos el puerto serie para comunicar a 
115200 baudios
*/
Serial.begin(115200);
  
  
}



void loop(){
  
  //leemos el valor devuelto por la fotoresistencia
  int value=analogRead(LDR);

  /* pintamos por el monitor serial el valor leido
  (Herramientas->monitor serial)
  */
  Serial.print("Valor leido: ");
  Serial.println(value);
  
  //delay de medio segundo
  delay(500);
  
//obtenemos el valor del brillo del led de la funcion map
int value_led=map(value,100,600,0,255);
//escrimos el valor obtenido en mad en el led a traves de la salida PWM
analogWrite(LED,value_led);
  
  
  
  
  
}
