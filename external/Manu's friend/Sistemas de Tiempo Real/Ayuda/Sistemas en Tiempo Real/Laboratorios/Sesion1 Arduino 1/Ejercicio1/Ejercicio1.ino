


#define BUTTON 2
#define LED 4

const boolean debug=true;
//variables para controlar el estado del boton y el LED
int value;
int old;

void setup(){
  
value=0;
old=0;
  
  //indicamos el modo de los dos pines digitales que se van a user
  pinMode(LED,OUTPUT);
  pinMode(BUTTON,INPUT);
  
  /*indicamos que la comunicacion por puerto serie
  Sera a 115200 baudios (bps). Utilizaremos el monitor serial para depurar
  */
  Serial.begin(115200);
  
}



void loop(){
  
  int current=digitalRead(BUTTON);
   
   //si la depuracion esta activada
   if(debug){
     //pintamos por puerto serie el valor leido
    Serial.println("Estado-> "+current); 
   }
 
 //si se lee un 1 y el valor anterior era un 0
  if(current==HIGH && old==LOW){
    //cambiamos el valor del LED
    value=HIGH-value;
     
  }
  //escribimos en el LED el valor
  digitalWrite(LED,value);
   //cambiamos el valor anterior por el actual
  old=current;
  
  
  
}
