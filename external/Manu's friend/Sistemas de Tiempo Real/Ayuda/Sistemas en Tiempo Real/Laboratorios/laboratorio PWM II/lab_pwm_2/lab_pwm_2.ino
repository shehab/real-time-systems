
   ///tones = {pausa, do, re, mi, fa, sol, la, si, Do}
//tones = {0, 1, 2, 3, 4, 5, 6, 7, 8}
long tones[] = {0, 261, 294, 329, 349, 392, 440, 493, 523};
//Song: notes and times
long melody[] = {3,4,5,5, 4,3,2,1, 1,2,3,3, 2,3,4,5, 5,4,3,2,
1,1,2,3, 2,1,0,0};
long time[] = {2,1,1,1, 1,1,1,1, 1,1,1,2, 2,2,1,1, 1,1,1,1, 1,1,1,1,
2,1,4,4};
long tempo=180;
long MAX_NOTAS=28;
long compValue=0;
long CLOCK_SYSTEM = 16000000;
long preEscalado = 64;
int nota;
int esNota=0;
void setup(){
  
 pinMode(11,OUTPUT);
 pinMode(3,OUTPUT);

 //configuracion timer 2 (musica)
 TCCR2A=_BV(COM2A0)|_BV(WGM20)|_BV(COM2B1)| _BV(WGM21);
 TCCR2B=_BV(CS22)|_BV(WGM22);
 
 //configuracion timer 1 (interrupcion)
 TCCR1A=_BV(COM1A0)|_BV(COM1B0);
 TCCR1B=_BV(WGM12)|_BV(CS12)|_BV(CS10);
 TIMSK1=_BV(OCIE1A);
  
}

ISR(TIMER1_COMPA_vect) {
  if (esNota == 1) {
  compValue=CLOCK_SYSTEM/(preEscalado * 4 * tones[melody[nota]]);
  if (melody[nota] == 0)
  compValue = 0;
  OCR2A=compValue;
  OCR2B=compValue/2;
  OCR1A=(time[nota]*tempo)*16;
  esNota=1-esNota;
  nota = nota +1;
  if (nota == MAX_NOTAS)
  nota = 0;
  } else {
  OCR2A=0;
  OCR2B=0;
  OCR1A = (tempo/4)*16;
  esNota=1-esNota;
  }
}


void loop(){
  

/*for (nota=0;nota<MAX_NOTAS;nota++) {
compValue=CLOCK_SYSTEM/(preEscalado * 4 * tones[melody[nota]]);
if (melody[nota] == 0)
compValue = 0;
OCR2A=compValue;
OCR2B=compValue/2;
delay(time[nota]*tempo);
OCR2A=0;
OCR2B=0;
delay(tempo/4);
}*/
  
  
}
