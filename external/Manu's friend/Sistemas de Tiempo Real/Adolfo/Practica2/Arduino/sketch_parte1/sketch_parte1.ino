#define soundPin 11
#define botonPin 7
#define ledPin 13
#define SAMPLE_TIME 100 
#define NUM_MUESTRAS 4000

unsigned long timeOrig;
int boton;
int mute=0;

ISR(TIMER1_COMPA_vect) 
{
  static int bitwise = 1;
  static unsigned char data = 0;
    
    bitwise = (bitwise * 2);
    if (bitwise > 128) {
        bitwise = 1;
        if (Serial.available()>1) {
           data = Serial.read();
        }
    }
    if(!mute){
        digitalWrite(soundPin, (data & bitwise) );
    }
}
// *************************
// **funcion: estado del botón
// *************************
void estado_boton()
{
  int lectura_boton = digitalRead(botonPin);
  if(lectura_boton==HIGH && boton==LOW){
      noInterrupts(); //lock, para que mute no altere la lectura de la interrupcion
          mute = 1;
      interrupts(); //unlock
  }
  else if(lectura_boton==HIGH && boton==HIGH){
       noInterrupts(); //lock
          mute = 0;
       interrupts(); //unlock
  }
  digitalWrite(ledPin,mute);

  if(boton==LOW){boton = HIGH;}
  else {boton = LOW;}
}

void setup ()
{
    pinMode(soundPin, OUTPUT);
    pinMode(botonPin,INPUT);
    pinMode(ledPin, OUTPUT);
    Serial.begin(115200);
    
    //inicializamos las variables globales
    boton= LOW;
    
    //CTC, con pre-escalado de 001(divisor de reloj por 1)
    TCCR1B = _BV(WGM12) | _BV(CS10);
    TCCR1A=0;  
    OCR1A = NUM_MUESTRAS;
    
    TIMSK1 = _BV(OCIE1A); 
    
    timeOrig = micros();    
}

void loop ()
{
    unsigned long timeDiff;

    estado_boton();
    timeDiff = SAMPLE_TIME - (micros() - timeOrig);
    timeOrig = timeOrig + SAMPLE_TIME;
    delayMicroseconds(timeDiff);
}
