/**********************************************************
 *  INCLUDES
 *********************************************************/
#include <vxWorksCommon.h>
#include <vxWorks.h>
#include <stdio.h>
#include <fcntl.h>
#include <ioLib.h>
#include <string.h>
#include <pthread.h>
#include <time.h>
#include <selectLib.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/select.h>
#include <sioLib.h>
// Uncomment to test on the Arduino with the serial module
#include "serialcallLib.h"
// Uncomment to test on the PC
//#include "audiocallLib.h"



/**********************************************************
 *  CONSTANTS
 *********************************************************/
#define NSEC_PER_SEC 1000000000UL
#define DEV_NAME "/tyCo/1"

// Path of audio file in windows
#define FILE_NAME "host:/Z/windows/escritorio/let_it_be.raw"


// Uncomment to test on the Arduino
#define PERIOD_TASK_SEC	0			/* Period of Task   */
#define PERIOD_TASK_NSEC  64000000	/* Period of Task   */
#define SEND_SIZE 256    /* BYTES */

// Uncomment to test on the PC
//#define PERIOD_TASK_SEC	1			/* Period of Task   */
//#define PERIOD_TASK_NSEC  0	/* Period of Task   */
//#define SEND_SIZE 4000    /* BYTES */




/**********************************************************
 *  GLOBALS
 *********************************************************/
pthread_mutex_t mutex; //declaramos el mutex
unsigned char buf[SEND_SIZE];

int control=48; 

/**********************************************************
 *  IMPLEMENTATION
 *********************************************************/

/*
 * Function: unblockRead
 */
int unblockRead(int fd, char *buffer, int size)
{
    fd_set readfd;
    struct timeval wtime;
	int ret;
    
    FD_ZERO(&readfd);
    FD_SET(fd, &readfd);
    wtime.tv_sec=0;
	wtime.tv_usec=0;
    ret = select(2048,&readfd,NULL,NULL,&wtime);
    if (ret < 0) {
    	perror("ERROR: select");
		return ret;
    }
    if (FD_ISSET(fd, &readfd)) {
        ret = read(fd, buffer, size);
        return (ret);
    } else {
 		return (0);
	}
}

/*
 * Function: diffTime
 */
void diffTime(struct timespec end, 
			  struct timespec start, 
			  struct timespec *diff) 
{
	if (end.tv_nsec < start.tv_nsec) {
		diff->tv_nsec = NSEC_PER_SEC - start.tv_nsec + end.tv_nsec;
		diff->tv_sec = end.tv_sec - (start.tv_sec+1);
	} else {
		diff->tv_nsec = end.tv_nsec - start.tv_nsec;
		diff->tv_sec = end.tv_sec - start.tv_sec;
	}
}

/*
 * Function: addTime
 */
void addTime(struct timespec end, 
			  struct timespec start, 
			  struct timespec *add) 
{
	unsigned long aux;
	aux = start.tv_nsec + end.tv_nsec;
	add->tv_sec = start.tv_sec + end.tv_sec + 
			      (aux / NSEC_PER_SEC);
	add->tv_nsec = aux % NSEC_PER_SEC;
}

/*
 * Function: compTime
 */
int compTime(struct timespec t1, 
			  struct timespec t2)
{
	if (t1.tv_sec == t2.tv_sec) {
		if (t1.tv_nsec == t2.tv_nsec) {
			return (0);
		} else if (t1.tv_nsec > t2.tv_nsec) {
			return (1);
		} else if (t1.tv_sec < t2.tv_sec) {
			return (-1);
		}
	} else if (t1.tv_sec > t2.tv_sec) {
		return (1);
	} else if (t1.tv_sec < t2.tv_sec) {
		return (-1);
	} 
	return (0);
}
/*
 * Function: play
 * se encarga de realizar la lectura del fichero
 */
void play(int fd_file){
	//Variables locales----
	struct timespec start,end,diff,cycle;
	int ret;
	int aux;
	int i;
		
    // loading cycle time
    cycle.tv_sec=PERIOD_TASK_SEC;
    cycle.tv_nsec=PERIOD_TASK_NSEC;
    
    clock_gettime(CLOCK_REALTIME,&start);
	while (1) {
		
		//Se comprueba el estado del reproductor.
		//Se bloquea la seccion critica con mutex para evitar..
		//..que la variable compartida sea "machacada" por otra tarea
		pthread_mutex_lock(&mutex);
			aux=control;
		pthread_mutex_unlock(&mutex); // Se desbloquea la variable compartida
		
		
		//printf("antes del if");
		if (aux == 48){
			//read from music file
			ret=read(fd_file,buf,SEND_SIZE);
			//printf("valor del ret: %d\n",ret);
			if (ret < 0) {
				printf("read: error reading file\n");
				return NULL;
			}
		}
		// si no se envian ceros
		else{
			//printf("no entro en el if");
			//Se envían ceros a cada posición del buffer 
			for (i=0;i<SEND_SIZE;i++){
				buf[i]=0;
			}
		}
		
		// write to serial port  
		/*########## Uncomment to test on the ARDUINO ##########*/
		ret = writeSerialMod_256 (buf);
		
		/*########## Uncomment to test on the PC ##########*/
		//ret = reproducir_4000 (buf);
		if (ret < 0) {
			printf("write: error writting serial\n");
			return NULL;
		}
	
		// get end time, calculate lapso and sleep
		clock_gettime(CLOCK_REALTIME,&end);
		diffTime(end,start,&diff);
		if (0 >= compTime(cycle,diff)) {
			printf("ERROR: lasted long than the cycle\n");
			return NULL;
	    }
	    diffTime(cycle,diff,&diff);
		nanosleep(&diff,NULL);   
	    addTime(start,cycle,&start);
	}
}	
/*
 * Function: ver_estado 
 * esta función se encarga de mostrar el estado de la reproducción
 */
void ver_estado(){
	//Variables locales
	struct timespec start,end,diff,cycle;
	
    // loading cycle time
    cycle.tv_sec=5;//la tarea debe comprobarse cada 5 segundos
    cycle.tv_nsec=0;
    
    clock_gettime(CLOCK_REALTIME,&start);
	while (1) {
		
		//Bloqueamos la seccion critica
		pthread_mutex_lock(&mutex);
		//Comprobamos si la reproduccion esta en marcha o en.. 
		//..pausa a traves de la variable de control
			if(control!=48){
				printf("Reproduccion en pausa\n");
			}else{
				printf("Reproduccion en marcha\n");
			}
		pthread_mutex_unlock(&mutex);// Desbloqueamos
		
		
		// get end time, calculate lapso and sleep
	    clock_gettime(CLOCK_REALTIME,&end);
	    diffTime(end,start,&diff);
	    if (0 >= compTime(cycle,diff)) {
			printf("ERROR: lasted long than the cycle\n");
			return NULL;
	    }
	    diffTime(cycle,diff,&diff);
		nanosleep(&diff,NULL);   
	    addTime(start,cycle,&start);
	}
}
/*
 * Function: modificar_estado 
 * esta tarea se encarga de activar o desactivar el mute
 */
void modificar_estado(){
	
	struct timespec start,end,diff,cycle;
	int ret;
	char aux;
	
    // loading cycle time
    cycle.tv_sec=2; //la tarea debe comprobarse cada 2 segundos
    cycle.tv_nsec=0;
    
    clock_gettime(CLOCK_REALTIME,&start);
	while (1) {
		//Se lee por teclado y sin bloqueo
		ret= unblockRead(STDIN_FILENO, &aux, 1);
		if (ret<0){
			return NULL;
		}
		
		//Bloqueamos la seccion critica, para evitar que el nuevo valor sea
		//machacado por otra tarea
		pthread_mutex_lock(&mutex);
		  //Se cambia el valor  de control por el valor de aux (mute>Off ó mute>On).
			if ( (aux == '0') || (aux == '1') ){
				control=aux;
			}
		pthread_mutex_unlock(&mutex); //Desbloqueamos
		
		
		// get end time, calculate lapso and sleep
	    clock_gettime(CLOCK_REALTIME,&end);
	    diffTime(end,start,&diff);
	    if (0 >= compTime(cycle,diff)) {
			printf("ERROR: lasted long than the cycle\n");
			return NULL;
	    }
	    diffTime(cycle,diff,&diff);
		nanosleep(&diff,NULL);   
	    addTime(start,cycle,&start);
	}
}
/*****************************************************************************
 * Function: main()
 *****************************************************************************/
int main()
{
    int fd_file = -1;
    int fd_serie = -1;
    int ret = 0;
    
	// Uncomment to test on the Arduino
    fd_serie = initSerialMod_WIN_115200 ();

	// Uncomment to test on the PC
    //	iniciarAudio_Windows ();

	/* Open music file */
	printf("open file %s begin\n",FILE_NAME);
	fd_file = open (FILE_NAME, O_RDONLY, 0644);
	if (fd_file < 0) {
		printf("open: error opening file\n");
		return -1;
	}
	//printf("fichero abiierto, fd_file %d\n",fd_file);
	
	//Inicializamos mutex
	pthread_mutex_init(&mutex,NULL);
		
	/*Creamos un hilo para cada funcion creada
	 * 	Function: play
	 *  Function: modificar_estado
	 *  Function: ver_estado
	*/
    pthread_t fd[3];
    
	// Primer hilo
	if (pthread_create(&fd[0],NULL,(void *)play,fd_file)<0){
	    printf("Error al crear el proceso\n");
	    return NULL;
	}
	// Segundo hilo
	if (pthread_create(&fd[1],NULL,(void *)modificar_estado,NULL)<0){
	    printf("Error al crear el proceso\n");
	    return NULL;
	}
	// Tercer hilo
	if (pthread_create(&fd[2],NULL,(void *)ver_estado,NULL)<0){
	    printf("Error al crear el proceso\n");
	    return NULL;
	}
	    
	//Esperamos por nuestros tres hilos
	 int i;
	 for(i=0;i<3;i++){
	  	pthread_join(fd[i],NULL);
	 }
	 //Por último, destruimos el mutex
	 pthread_mutex_destroy(&mutex);
}
