// --------------------------------------
// Global Variables
// --------------------------------------
#include <string.h>
#include <stdio.h>

#define GAS_PIN 13
#define BRK_PIN 12
#define MIX_PIN 11
#define SPD_PIN 10
#define SLP_INIT 9
#define SLP_FIN 8
#define LIT_PIN A0 
#define LAM_PIN 7 
#define STP_PIN A1 // Leer estado movimiento
#define BTN_PIN 6 // Botón para validar distancia
#define DISP_A 2  // Primer bit empezando por la derecha (XXXA)
#define DISP_B 3  // Segundo bit empezando por la derecha(XXBX)
#define DISP_C 4  // Tercer bit empezando por la derecha (XCXX)
#define DISP_D 5  // Cuarto bit empezando por la derecha (DXXX)

//MODOS DE OPERACIÓN 
#define MODO_SELECCION_DISTANCIA 0
#define MODO_ACERCAMIENTO_DEPOSITO 1
#define MODO_PARADA 2

#define N_CYCLES 2 
// --------------------------------------
// Global Variables
// --------------------------------------
float speed =55.0;
signed int slp = 0; //Puede valer -1, 0 ó 1.
int mix=0; //Puede valer 0 ó 1.
int brk=0; //Puede valer 0 ó 1.
int gas=0; //Puede valer 0 ó 1.
int lit=0;
int lam=0;
int modo=0; // Puede valer 0, 1 ó 2. (stp). Inicialmente se encuentra en modo SELEC
int distancia=0; //Guarda el valor actual de la distancia.
int boton=0; // Valor actual del botón.
int CS=0; //Ciclo secundario actual.
float TIME_CYCLE=100.0;//Cada Ciclo secundario dura 100 ms
int Tiempo_inicial=0;    //tiempo inicial
int Tiempo_final=0;   //tiempo final

// --------------------------------------
// Function: comm_server
// --------------------------------------
int comm_server()
{
    int i;
    char request[10];
    char answer[10];
    int speed_int;
    int speed_dec;
    
    // while there is enough data for a request
    if (Serial.available() >= 9) {
        // read the request
        i=0; 
        while ( (i<9) && (Serial.available() >= (9-i)) ) {
            // read one character
            request[i]=Serial.read();
           
            // if the new line is positioned wrong 
            if ( (i!=8) && (request[i]=='\n') ) {
                // Send error and start all over again
                sprintf(answer,"MSG: ERR\n");
                Serial.print(answer);
                memset(request,'\0', 9);
                i=0;
            } else {
              // read the next character
              i++;
            }
        }
        request[9]='\0';
        
        //----ACELERADOR
        if(0 == strcmp("GAS: SET\n",request)) {
            // Comprobamos que el acelerador está desactivado para activarlo
		if(gas == 0) {gas=1;
	          	      sprintf(answer,"GAS:  OK\n");}
                //si el acelerador ya está activado devolvemos error.
		else {sprintf(answer,"MSG: ERR\n");}
	}
	else if(0 == strcmp("GAS: CLR\n",request)) {
            // Comprobamos que el acelerador está activado para desactivarlo
		if(gas == 1) {gas=0;
			      sprintf(answer,"GAS:  OK\n");}
                //si el acelerador ya está desactivado devolvemos error.
		else {sprintf(answer,"MSG: ERR\n");}
	}
        //----FRENO
	else if(0 == strcmp("BRK: SET\n",request)) {
             // Comprobamos que el freno está desactivado para activarlo
		if(brk == 0) {brk=1;
	          	      sprintf(answer,"BRK:  OK\n");}
                //si el freno ya está activado devolvemos error.
		else {sprintf(answer,"MSG: ERR\n");}
	}
	else if(0 == strcmp("BRK: CLR\n",request)) {
            // Comprobamos que el freno está activado para desactivarlo
		if(brk == 1) {brk=0;
			      sprintf(answer,"BRK:  OK\n");}
                //si el freno ya está desactivado devolvemos error.
		else {sprintf(answer,"MSG: ERR\n");}
	}
	//----MEZCLADOR
	else if(0 == strcmp("MIX: SET\n",request)) {
            // Comprobamos que el mezclador está desactivado para activarlo
		if(mix == 0) {mix=1;
	          	      sprintf(answer,"MIX:  OK\n");}
                //si el mezclador ya está activado devolvemos error.
		else {sprintf(answer,"MSG: ERR\n");}
	}
	else if(0 == strcmp("MIX: CLR\n",request)) {
            // Comprobamos que el mezclador está activado para desactivarlo
		if(mix == 1) {mix=0;
			      sprintf(answer,"MIX:  OK\n");}
                //si el mezclador ya está desactivado devolvemos error.
		else {sprintf(answer,"MSG: ERR\n");}
	}			
        //----PENDIENTE
	else if(0 == strcmp("SLP: REQ\n",request)) {
            // leemos el estado de la pendiente
		if(slp == -1) {sprintf(answer,"SLP:DOWN\n");}
		else if(slp == 1) {sprintf(answer,"SLP:  UP\n");}
		else {sprintf(answer,"SLP:FLAT\n");}
	}		
	//---VELOCIDAD
	else if (0 == strcmp("SPD: REQ\n",request)) {
          // send the answer for speed request
            speed_int = (int)speed;
            speed_dec = ((int)(speed*10)) % 10;
            sprintf(answer,"SPD:%02d.%d\n",speed_int,speed_dec); 
        }
        //----LEER SENSOR LUZ
        else if (0 == strcmp("LIT: REQ\n",request)) {
          // send the answer for speed request
            sprintf(answer,"LIT: %02d%%\n",lit); 
        }
        //----FOCOS
        else if (0 == strcmp("LAM: SET\n",request)) {
          // send the answer for speed request
            lam=100;
            sprintf(answer,"LAM:  OK\n",lit); 
        }
        else if (0 == strcmp("LAM: CLR\n",request)) {
          // send the answer for speed request
            lam=0;
            sprintf(answer,"LAM:  OK\n",lit); 
        }
        //----LEER ESTADO MOVIMIENTO
        else if (0 == strcmp("STP: REQ\n",request)) {
          // send the answer for speed request
            if(modo == MODO_SELECCION_DISTANCIA || modo==MODO_ACERCAMIENTO_DEPOSITO){
              sprintf(answer,"STP:  GO\n",modo); 
            }else{
              sprintf(answer,"STP:STOP\n",modo); 
            }   
        }
        //----DISTANCIA
        else if(0==strcmp("DS:  REQ\n",request)){
            sprintf(answer,"DS:%5d\n",distancia);
        }        
        //----ERROR
        else {
            // error, send error message
            sprintf(answer,"MSG: ERR\n");
        }
	Serial.print(answer);
    }
    return 0;
}
// --------------------------------------
// Function: acelerador
// --------------------------------------
int acelerador(){
  //actualizamos el estado del acelerador
  digitalWrite(GAS_PIN,gas);
  return 0; 
}
// --------------------------------------
// Function: freno
// --------------------------------------
int freno(){ 
  //actualizamos el estado del freno 
  digitalWrite(BRK_PIN,brk);
  return 0; 
}
// --------------------------------------
// Function: mezclador
// --------------------------------------
int mezclador(){
  //actualizamos el estado del mezclador
  digitalWrite(MIX_PIN,mix);
  return 0;
}
// --------------------------------------
// Function: pendiente
// --------------------------------------
int pendiente(){
  if(digitalRead(SLP_INIT)==LOW && digitalRead(SLP_FIN)==HIGH ){
    //La pendiente será ascendente.
    slp=1; 
  }
  if(digitalRead(SLP_INIT)==HIGH && digitalRead(SLP_FIN)==LOW ){
    //La pendiente será descendente.
    slp=-1;
  } 
  if(digitalRead(SLP_INIT)==LOW && digitalRead(SLP_FIN)==LOW ){
    //No hay pendiente, terreno llano.
    slp=0;  
  }
  return 0;
}
// --------------------------------------
// Function: velocidad
// --------------------------------------
int velocidad(){
 //Calculamos la velocidad a partir de la ecuación del movimiento rectilineo uniformemente acelerado
 //.. para ello, primero calcularemos la aceleración
    float aceleracion=0;
    aceleracion= (0.5*gas)-(0.5*brk)-(0.25*slp);
  // Si acelerador(gas) activo la aceleración será de +0.5m/s2.
  // Si freno(brk) activo la aceleración será de -0.5m/s2.
  //Dependiendo del valor de la pendiente(slp) la aceleración será +0.25m/s2, 0.00m/s2 ó -0,25m/s2

  //Fórmula MRUA: V=v0+a*t;
    speed=speed + aceleracion * (TIME_CYCLE/1000.0);
  
  //el brillo del led varía según la velocidad(speed) obtenida:
    int v_led= map(speed,40,70,0,255);
  
  //Si la velocidad no es segura (entre 40 y 70) apagamos el led
    if(speed > 70 || speed < 40){
     v_led=LOW; 
    }

  // Escribimos el led en el pin de velocidad:
    analogWrite(SPD_PIN, v_led);
  
  //Por último queda comprobar si la velocidad llega a cero
    if(speed < 0){
      speed=0.0; //en caso de que llege a cero, se establecerá éste como su nuevo valor inicial.
    }
  return 0;
}
// --------------------------------------
// Function: lectura sensor luz
// --------------------------------------
int lectura_sensor_luz(){
  //Guardaremos en una variable la lectura del sensor de luz
  int luz=0;  
  luz= analogRead(LIT_PIN);
  
  // el brillo del led varía según la oscuridad de la zona
  lit= map(luz,0,1023,0,99);
  
  return 0;
}
// --------------------------------------
// Function: focos
// --------------------------------------
int focos(){
  //actualizamos el valor de los focos
  digitalWrite(LAM_PIN,lam);
  
  return 0; 
}
// --------------------------------------
// Function: lectura_distancia_deposito
// --------------------------------------
int lectura_distancia_deposito(){
  //Ontenemos el valor de la distancia
  int distancia_sensor=0;
  distancia_sensor= analogRead(STP_PIN);
  
  distancia= map(distancia_sensor,666, 1023, 10000, 90000);
  //Serial.print(distancia);
  //Serial.print("\n");
  return 0; 
}
// --------------------------------------
// Function: mostrar_distancia_deposito
// --------------------------------------
int mostrar_distancia_deposito(){
  //Obtenemos el valor de la distancia
  int distancia_aux=0;
  float resto_dist_aux= distancia%10000;
  //Serial.print(resto_dist_aux);
  //Serial.print("\n");
  if(resto_dist_aux<0){
  dibujar_display(15);
  }else{
  distancia_aux= (int)distancia/10000;
  //Serial.print(distancia_aux);
  //Serial.print("\n");
  //Serial.print("------------\n");
  
  //Llamamos a la función que pinta el display---------------
  dibujar_display(distancia_aux);
  }
  return 0; 
}
// --------------------------------------
// Function: Boton
// --------------------------------------
int estado_btn(){
  if(digitalRead(BTN_PIN)==HIGH){
   boton= HIGH;
  }
  else if(digitalRead(BTN_PIN)==0 && boton== HIGH){
    boton= LOW;
    //Ahora controlamos el modo en el que nos encontramos
    if(modo==MODO_PARADA){
      modo=MODO_SELECCION_DISTANCIA;
    }
    else if(modo==MODO_SELECCION_DISTANCIA){
      modo=MODO_ACERCAMIENTO_DEPOSITO;
    }
  }
  return 0;
}
// --------------------------------------
// Function: calcular distancia
// --------------------------------------
int calcular_distancia(){
  //Limpiamos el display------------------
  limpiar_display();
 
  distancia=distancia-(speed*(TIME_CYCLE/1000.0));
  //Serial.print(distancia);
  //Serial.print("\n");
  
  if(distancia<=0 && speed<=10){
    distancia=0;
    modo=MODO_PARADA;
  }
  else if(distancia<=0 && speed>10){
    distancia=0;
    modo=MODO_SELECCION_DISTANCIA;
  }
  
  dibujar_display((int)distancia/10000);
  return 0;
}
// --------------------------------------
// Function: limpiar display
// --------------------------------------
void limpiar_display(){
  digitalWrite(DISP_A,LOW);
  digitalWrite(DISP_B,LOW);
  digitalWrite(DISP_C,LOW);
  digitalWrite(DISP_D,LOW);
  //delay(5000); //Con esto vemos que se produce el cambio de estado en el display 
}
// --------------------------------------
// Function: dibujar display
// --------------------------------------
void dibujar_display(int num){
  switch(num){
    case 0:
        digitalWrite(DISP_A,LOW);
        digitalWrite(DISP_B,LOW);
        digitalWrite(DISP_C,LOW);
        digitalWrite(DISP_D,LOW);
        break;
    case 1: 
        digitalWrite(DISP_A,HIGH);
        digitalWrite(DISP_B,LOW);
        digitalWrite(DISP_C,LOW);
        digitalWrite(DISP_D,LOW);
        break;
    case 2:
        digitalWrite(DISP_A,LOW);
        digitalWrite(DISP_B,HIGH);
        digitalWrite(DISP_C,LOW);
        digitalWrite(DISP_D,LOW);
        break;
    case 3:
        digitalWrite(DISP_A,HIGH);
        digitalWrite(DISP_B,HIGH);
        digitalWrite(DISP_C,LOW);
        digitalWrite(DISP_D,LOW);
        break;
    case 4:
        digitalWrite(DISP_A,LOW);
        digitalWrite(DISP_B,LOW);
        digitalWrite(DISP_C,HIGH);
        digitalWrite(DISP_D,LOW);
        break;
    case 5:
        digitalWrite(DISP_A,HIGH);
        digitalWrite(DISP_B,LOW);
        digitalWrite(DISP_C,HIGH);
        digitalWrite(DISP_D,LOW);
        break;
    case 6:
        digitalWrite(DISP_A,LOW);
        digitalWrite(DISP_B,HIGH);
        digitalWrite(DISP_C,HIGH);
        digitalWrite(DISP_D,LOW);
        break;
    case 7:
        digitalWrite(DISP_A,HIGH);
        digitalWrite(DISP_B,HIGH);
        digitalWrite(DISP_C,HIGH);
        digitalWrite(DISP_D,LOW);
        break;
    case 8:
        digitalWrite(DISP_A,LOW);
        digitalWrite(DISP_B,LOW);
        digitalWrite(DISP_C,LOW);
        digitalWrite(DISP_D,HIGH);
        break;
    case 9:       
        digitalWrite(DISP_A,HIGH);
        digitalWrite(DISP_B,LOW);
        digitalWrite(DISP_C,LOW);
        digitalWrite(DISP_D,HIGH);
        break;
    default:
        digitalWrite(DISP_A,HIGH);
        digitalWrite(DISP_B,HIGH);
        digitalWrite(DISP_C,HIGH);
        digitalWrite(DISP_D,HIGH);
        break;
  }
}
// --------------------------------------
// Function: velocidad parada
// --------------------------------------
int velocidad_parada(){
  speed=0;
  analogWrite(SPD_PIN,speed);
  limpiar_display(); //limpiamos el display
  dibujar_display(15); //mostramos 15, es decir, display apagado.  
}
// --------------------------------------
// Function: modo seleccion distancia
// --------------------------------------
void modo_seleccion_distancia(){
        acelerador();
        freno();
        mezclador();
        pendiente();
        velocidad();
        lectura_sensor_luz();
        focos();
        lectura_distancia_deposito();
        mostrar_distancia_deposito();
        estado_btn();
}
// --------------------------------------
// Function: modo acercamiento deposito
// --------------------------------------
void modo_acercamiento_deposito(){
        acelerador();
        freno();
        mezclador();
        pendiente();
        velocidad();
        lectura_sensor_luz();
        focos();
        calcular_distancia();  
}
// --------------------------------------
// Function: modo parada
// --------------------------------------
void modo_parada(){
        mezclador();
        pendiente();
        velocidad_parada();
        focos();
        estado_btn();
}
// --------------------------------------
// Function: setup
// --------------------------------------
void setup() {  
   //Bus serial iniciado a 9600 baudios
    Serial.begin(9600);

    //pines digitales:
    pinMode(GAS_PIN,OUTPUT);
    pinMode(BRK_PIN,OUTPUT);
    pinMode(MIX_PIN,OUTPUT);
    pinMode(SLP_INIT,INPUT);
    pinMode(SLP_FIN,INPUT);
    pinMode(SPD_PIN,OUTPUT); 
    pinMode(LAM_PIN, OUTPUT);
    pinMode(BTN_PIN, OUTPUT);
    pinMode(DISP_A, OUTPUT);
    pinMode(DISP_B, OUTPUT);
    pinMode(DISP_C, OUTPUT);
    pinMode(DISP_D, OUTPUT);

    //pines analogicos:
    pinMode(LIT_PIN,INPUT);
    pinMode (STP_PIN,INPUT);
   
   //Calculamos el tiempo inicial
   Tiempo_inicial=millis();
}
// --------------------------------------
// Function: loop
// --------------------------------------
void loop() {
  
  switch(CS){
    case 0:
      comm_server();
      if(modo==MODO_SELECCION_DISTANCIA){
        modo_seleccion_distancia();
      }
      else if(modo==MODO_ACERCAMIENTO_DEPOSITO){
       modo_acercamiento_deposito();
      }
      else if(modo==MODO_PARADA){
        modo_parada();
      }
      break;
    
    case 1:
      if(modo==MODO_SELECCION_DISTANCIA){
        modo_seleccion_distancia();
      }
      else if(modo==MODO_ACERCAMIENTO_DEPOSITO){
         modo_acercamiento_deposito();
      }
      else if(modo==MODO_PARADA){
       modo_parada();
      }
      break;
    }
 
 //Calculamos siguiente ciclo
  CS=(CS+1)%N_CYCLES;
 
 //Calculamos el tiempo final para saber si se ha excedido el tiempo máximo del CS
  Tiempo_final=millis();
  if( (TIME_CYCLE-(Tiempo_final-Tiempo_inicial)) < 0){
    Serial.print("Error, tiempo máximo del CS excedido.");
  }
  
  delay(TIME_CYCLE-(Tiempo_final-Tiempo_inicial));
  Tiempo_inicial= Tiempo_inicial+TIME_CYCLE;
}

