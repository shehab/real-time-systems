// --------------------------------------
// Global Variables
// --------------------------------------
#include <string.h>
#include <stdio.h>

#define GAS_PIN 13
#define BRK_PIN 12
#define MIX_PIN 11
#define SPD_PIN 10
#define SLP_INIT 9
#define SLP_FIN 8

 
// --------------------------------------
// Global Variables
// --------------------------------------
float speed=55.0;
signed int slp = 0; //Puede valer -1, 0 ó 1.
int mix=0; //Puede valer 0 ó 1.
int brk=0; //Puede valer 0 ó 1.
int gas=0; //Puede valer 0 ó 1.
int CS=0; //Ciclo secundario actual.
int N_CYCLES=2;
float TIME_CYCLE=100.0;//Cada Ciclo secundario dura 100 ms
int Tiempo_inicial=0;    //tiempo inicial
int Tiempo_final=0;   //tiempo final

// --------------------------------------
// Function: comm_server
// --------------------------------------
int comm_server()
{    
    int i;
    char request[10];
    char answer[10];
    int speed_int;
    int speed_dec;
    
    // while there is enough data for a request
    if (Serial.available() >= 9) {
        // read the request
        i=0; 
        while ( (i<9) && (Serial.available() >= (9-i)) ) {
            // read one character
            request[i]=Serial.read();
           
            // if the new line is positioned wrong 
            if ( (i!=8) && (request[i]=='\n') ) {
                // Send error and start all over again
                sprintf(answer,"MSG: ERR\n");
                Serial.print(answer);
                memset(request,'\0', 9);
                i=0;
            } else {
              // read the next character
              i++;
            }
        }
        request[9]='\0';
        
        //----ACELERADOR
        if(0 == strcmp("GAS: SET\n",request)) {
            // Comprobamos que el acelerador está desactivado para activarlo
		if(gas == 0) {gas=1;
	          	      sprintf(answer,"GAS:  OK\n");Serial.print(answer);}
                //si el acelerador ya está activado devolvemos error.
		else {sprintf(answer,"MSG: ERR\n");Serial.print(answer);}
	}
	else if(0 == strcmp("GAS: CLR\n",request)) {
            // Comprobamos que el acelerador está activado para desactivarlo
		if(gas == 1) {gas=0;
			      sprintf(answer,"GAS:  OK\n");Serial.print(answer);}
                //si el acelerador ya está desactivado devolvemos error.
		else {sprintf(answer,"MSG: ERR\n");Serial.print(answer);}
	}
        //----FRENO
	else if(0 == strcmp("BRK: SET\n",request)) {
             // Comprobamos que el freno está desactivado para activarlo
		if(brk == 0) {brk=1;
	          	      sprintf(answer,"BRK:  OK\n");Serial.print(answer);}
                //si el freno ya está activado devolvemos error.
		else {sprintf(answer,"MSG: ERR\n");Serial.print(answer);}
	}
	else if(0 == strcmp("BRK: CLR\n",request)) {
            // Comprobamos que el freno está activado para desactivarlo
		if(brk == 1) {brk=0;
			      sprintf(answer,"BRK:  OK\n");Serial.print(answer);}
                //si el freno ya está desactivado devolvemos error.
		else {sprintf(answer,"MSG: ERR\n");Serial.print(answer);}
	}
	//----MEZCLADOR
	else if(0 == strcmp("MIX: SET\n",request)) {
            // Comprobamos que el mezclador está desactivado para activarlo
		if(mix == 0) {mix=1;
	          	      sprintf(answer,"MIX:  OK\n");Serial.print(answer);}
                //si el mezclador ya está activado devolvemos error.
		else {sprintf(answer,"MSG: ERR\n");Serial.print(answer);}
	}
	else if(0 == strcmp("MIX: CLR\n",request)) {
            // Comprobamos que el mezclador está activado para desactivarlo
		if(mix == 1) {mix=0;
			      sprintf(answer,"MIX:  OK\n");Serial.print(answer);}
                //si el mezclador ya está desactivado devolvemos error.
		else {sprintf(answer,"MSG: ERR\n");Serial.print(answer);}
	}			
        //----PENDIENTE
	else if(0 == strcmp("SLP: REQ\n",request)) {
            // leemos el estado de la pendiente
		if(slp == -1) {sprintf(answer,"SLP:DOWN\n");Serial.print(answer);}
		else if(slp == 1) {sprintf(answer,"SLP:  UP\n");Serial.print(answer);}
		else {sprintf(answer,"SLP:FLAT\n");Serial.print(answer);}
	}		
	//---VELOCIDAD
	else if (0 == strcmp("SPD: REQ\n",request)) {
          // send the answer for speed request
            speed_int = (int)speed;
            speed_dec = ((int)(speed*10)) % 10;
            sprintf(answer,"SPD:%02d.%d\n",speed_int,speed_dec); 
            Serial.print(answer);
        } else {
            // error, send error message
            sprintf(answer,"MSG: ERR\n");
            Serial.print(answer);
        }
	
    }         
    return 0;

}
// --------------------------------------
// Function: acelerador
// --------------------------------------
int acelerador(){

  //actualizamos el estado del acelerador
  digitalWrite(GAS_PIN,gas);
  return 0; 
}
// --------------------------------------
// Function: freno
// --------------------------------------
int freno(){ 
  //actualizamos el estado del freno 
  digitalWrite(BRK_PIN,brk);
  return 0; 
}
// --------------------------------------
// Function: mezclador
// --------------------------------------
int mezclador(){
  //actualizamos el estado del mezclador
  digitalWrite(MIX_PIN,mix);
  return 0;
}
// --------------------------------------
// Function: pendiente
// --------------------------------------
int pendiente(){
  if(digitalRead(SLP_INIT)==LOW && digitalRead(SLP_FIN)==HIGH ){
    //La pendiente será ascendente.
    slp=1; 
  }
  if(digitalRead(SLP_INIT)==HIGH && digitalRead(SLP_FIN)==LOW ){
    //La pendiente será descendente.
    slp=-1;
  } 
  if(digitalRead(SLP_INIT)==LOW && digitalRead(SLP_FIN)==LOW ){
    //No hay pendiente, terreno llano.
    slp=0;  
  }
  return 0;
}
// --------------------------------------
// Function: velocidad
// --------------------------------------
int velocidad(){
 //Calculamos la velocidad a partir de la ecuación del movimiento rectilineo uniformemente acelerado
 //.. para ello, primero calcularemos la aceleración
  float aceleracion=0;
  aceleracion= (0.5*gas)-(0.5*brk)-(0.25*slp);
  // Si acelerador(gas) activo la aceleración será de +0.5m/s2.
  // Si freno(brk) activo la aceleración será de -0.5m/s2.
  //Dependiendo del valor de la pendiente(slp) la aceleración será +0.25m/s2, 0.00m/s2 ó -0,25m/s2

  //Fórmula MRUA: V=v0+a*t;
  speed=speed + aceleracion * (TIME_CYCLE/1000.0);
  
  //el brillo del led varía según la velocidad(speed) obtenida:
  int v_led= map(speed,40,70,0,255);
  
  //Si la velocidad no es segura (entre 30 y 70) apagamos el led
  if(speed > 70 || speed < 40){
   v_led=LOW; 
  }

  // Escribimos el led en el pin de velocidad:
  analogWrite(SPD_PIN, v_led);
  
  //Por último queda comprobar si la velocidad llega a cero
  if(speed < 0){
    speed=0.0; //en caso de que llege a cero, se establecerá éste como su nuevo valor inicial.
  }
  
  return 0;
}
// --------------------------------------
// Function: setup
// --------------------------------------
void setup() {  
   //Bus serial iniciado a 9600 baudios
    Serial.begin(9600);

    //pines salidas digitales(freno,acelerador, mezclador y pendiente)
    pinMode(GAS_PIN,OUTPUT);
    pinMode(BRK_PIN,OUTPUT);
    pinMode(MIX_PIN,OUTPUT);
    pinMode(SLP_INIT,INPUT);
    pinMode(SLP_FIN,INPUT); 
    pinMode(SPD_PIN,OUTPUT);
   
   //Calculamos el tiempo inicial
   Tiempo_inicial=millis();
}

// --------------------------------------
// Function: loop
// --------------------------------------
void loop() {
  switch(CS){
    case 0:
      comm_server();
      acelerador();
      freno();
      mezclador();
      pendiente();
      velocidad();
      break;
    case 1:
      acelerador();
      freno();
      mezclador();
      pendiente();
      velocidad(); 
      break;
    }
 
 //Calculamos siguiente ciclo
  CS=(CS+1)%N_CYCLES;
 
 //Calculamos el tiempo final para saber si se ha excedido el tiempo máximo del CS
  Tiempo_final=millis();
  if( (TIME_CYCLE-(Tiempo_final-Tiempo_inicial)) < 0){
    Serial.print("Error, tiempo máximo del CS excedido.");
  }
  
  delay(TIME_CYCLE-(Tiempo_final-Tiempo_inicial));
  Tiempo_inicial= Tiempo_inicial+TIME_CYCLE;
}

