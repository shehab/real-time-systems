/**********************************************************
 *  INCLUDES
 *********************************************************/
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <ioLib.h>
#include "displayC.h"
#include "serialcallLib.h"

/**********************************************************
 *  Constants
 **********************************************************/
#define NS_PER_S 1000000000
#define TIEMPO_CICLO_SECUNDARIO_SEGUNDOS 5 
#define TIEMPO_CICLO_SECUNDARIO_NANOSEGUNDOS 0
#define NUM_CICLOS_SECUNDARIOS 2
 
/**********************************************************
 *  Global Variables 
 *********************************************************/
float speed = 0.0;		
int GAS = 0;
int BRK = 0;
int MIX = 0;
int LIT = 0;
int LAM = -1;
int STP = 1;
int DS = -1;
struct timespec tiempo_mezclador;

char request[10];
char answer[10];

/**********************************************************
 *  Function: task_speed
 *********************************************************/
int task_speed(){

    //clear request and answer
    memset(request,'\0',10);
    memset(answer,'\0',10);
    
    // request speed
	strcpy(request,"SPD: REQ\n");
		
	//uncomment to use the simulator
	simulator(request, answer);
	// uncoment to access serial module
	//writeSerialMod_9(request);
	//readSerialMod_9(answer);
	
	// display speed
	if (1 == sscanf (answer,"SPD:%f\n",&speed)) {
		displaySpeed(speed); 
		//Speed control
		if (speed < 55.0) {
			BRK = 0;
			GAS = 1;
		}
		if (speed >= 55.0) {
			BRK = 1;
			GAS = 0;
		}
	}
	return 0;
}

/**********************************************************
 *  Function: task_slope
 *********************************************************/
int task_slope(){

	//clear request and answer
	memset(request,'\0',10);
	memset(answer,'\0',10);

	// request slope
	strcpy(request,"SLP: REQ\n");

	//uncomment to use the simulator
	simulator(request, answer);

	// uncoment to access serial module
	//writeSerialMod_9(request);
	//readSerialMod_9(answer);  

	// display slope
	if (0 == strcmp(answer,"SLP:DOWN\n")) displaySlope(-1);	
	if (0 == strcmp(answer,"SLP:FLAT\n")) displaySlope(0);	
	if (0 == strcmp(answer,"SLP:  UP\n")) displaySlope(1);

	return 0;
}

/**********************************************************
 *  Function: acelerador
 *********************************************************/
int acelerador() {
	//clear request and answer
	memset(request, '\0', 10);
	memset(answer, '\0', 10);
	
	//Comprobamos la variable GAS
	if (GAS == 1){
		//Se activa el acelerador
		strcpy(request, "GAS: SET\n");
	}
	if (GAS == 0){
		// Se desactiva el acelerador
		strcpy(request, "GAS: CLR\n");
	}
	
	//uncomment to use the simulator
	simulator(request, answer);
	// uncoment to access serial module
	//writeSerialMod_9(request);
	//readSerialMod_9(answer);
	
	// Se muestra la respuesta si recibimos OK
	if (0 == strcmp(answer, "GAS:  OK\n")) {
		displayGas(GAS);
	}
	return 0;
}

/**********************************************************
 *  Function: freno
 *********************************************************/
int freno() {
	//clear request and answer
	memset(request, '\0', 10);
	memset(answer, '\0', 10);
	
	//Comprobamos la variable BRK
	if (BRK == 1){
		//Se activa el freno
		strcpy(request, "BRK: SET\n");
	}
	if (BRK == 0){
		//Se desactiva el freno
		strcpy(request, "BRK: CLR\n");
	}
	
	//uncomment to use the simulator
	simulator(request, answer);
	// uncoment to access serial module
	//writeSerialMod_9(request);
	//readSerialMod_9(answer);
	
	// Se muestra la respuesta si recibimos OK
	if (0 == strcmp(answer, "BRK:  OK\n")) {
		displayBrake(BRK);
	}
	return 0;
}

/**********************************************************
 *  Function: addTime 
 *********************************************************/
void addTime(struct timespec end, 
			  struct timespec start, 
			  struct timespec *add) 
{
	unsigned long aux;
	aux = start.tv_nsec + end.tv_nsec;
	add->tv_sec = start.tv_sec + end.tv_sec + 
			      (aux / NS_PER_S);
	add->tv_nsec = aux % NS_PER_S;
}

/**********************************************************
 *  Function: compTime 
 *********************************************************/
int compTime(struct timespec t1, 
			  struct timespec t2)
{
	if (t1.tv_sec == t2.tv_sec) {
		if (t1.tv_nsec == t2.tv_nsec) {
			return (0);
		} else if (t1.tv_nsec > t2.tv_nsec) {
			return (1);
		} else if (t1.tv_nsec < t2.tv_nsec) {
			return (-1);
		}
	} else if (t1.tv_sec > t2.tv_sec) {
		return (1);
	} else if (t1.tv_sec < t2.tv_sec) {
		return (-1);
	} 
	return (0);
}

/**********************************************************
 *  Function: diffTime 
 *********************************************************/
void diffTime(struct timespec end, 
			  struct timespec start, 
			  struct timespec *diff) 
{
	if (end.tv_nsec < start.tv_nsec) {
		diff->tv_nsec = NS_PER_S - start.tv_nsec + end.tv_nsec;
		diff->tv_sec = end.tv_sec - (start.tv_sec+1);
	} else {
		diff->tv_nsec = end.tv_nsec - start.tv_nsec;
		diff->tv_sec = end.tv_sec - start.tv_sec;
	}
}

/**********************************************************
 *  Function: mezclador
 *********************************************************/
int mezclador() {
	//clear request and answer
	memset(request, '\0', 10);
	memset(answer, '\0', 10);
	struct timespec tiempo, tiempo_mezcla, comp;
	
	comp.tv_sec = 30;
	comp.tv_nsec = 0;
	clock_gettime(CLOCK_REALTIME, &tiempo);
	
	//Se restan los tiempos y se comparan
	diffTime(tiempo, tiempo_mezclador, &tiempo_mezcla);
	if (compTime(tiempo_mezcla, comp) == 1) {
		//Se activa el mezclador
		if (MIX == 0) {
			MIX = 1;
			clock_gettime(CLOCK_REALTIME, &tiempo_mezclador);
			strcpy(request, "MIX: SET\n");
		} else {
			//Se desactiva el mezclador
			if (MIX == 1) {
				MIX = 0;
				clock_gettime(CLOCK_REALTIME, &tiempo_mezclador);
				strcpy(request, "MIX: CLR\n");
			}
		}
		//uncomment to use the simulator
		simulator(request, answer);
		// uncoment to access serial module
		//writeSerialMod_9(request);
		//readSerialMod_9(answer);
		
		// Se muestra la respuesta si recibimos OK
		if (0 == strcmp(answer, "MIX:  OK\n")) {
			displayMix(MIX);
		}
	}
	return 0;
}

/**********************************************************
 *  Function: sensor_luz
 *********************************************************/
int sensor_luz() {
	//clear request and answer
	memset(request, '\0', 10);
	memset(answer, '\0', 10);
	//Se obtiene la luminosidad
	strcpy(request, "LIT: REQ\n");
	
	//uncomment to use the simulator
	simulator(request, answer);
	// uncoment to access serial module
	//writeSerialMod_9(request);
	//readSerialMod_9(answer);
	
	if (1 == sscanf(answer, "LIT: %i%\n", &LIT)) {
		//Comprobamos luminosidad
		if (LIT <= 50) {
			//Activamos los focos
			LAM = 1;
			displayLightSensor(LAM);
		} else {
			//Desactivamos los focos
			LAM = 0;
			displayLightSensor(LAM);
		}
	}
	return 0;
}

/**********************************************************
 *  Function: focos
 *********************************************************/
int focos() {
	//clear request and answer
	memset(request, '\0', 10);
	memset(answer, '\0', 10);
	//Se comprueba la variable LAM para activar/desactivar los focos
	if (LAM == 1) {
		//Activamos los focos
		strcpy(request, "LAM: SET\n");
	}else{
		//Desactivamos los focos
		strcpy(request, "LAM: CLR\n");
	}
	//uncomment to use the simulator
	simulator(request, answer);
	// uncoment to access serial module
	//writeSerialMod_9(request);
	//readSerialMod_9(answer);
	
	// Se muestra la respuesta si recibimos OK
	if (0 == strcmp(answer, "LAM:  OK\n")) {
		displayLamps(LAM);
	}
	return 0;
}

/**********************************************************
 *  Function: velModoFrenado
 *********************************************************/
int velModoFrenado() {
	//clear request and answer
	memset(request, '\0', 10);
	memset(answer, '\0', 10);
	
	//Se obtiene la velocidad
	strcpy(request,"SPD: REQ\n");
	
	//uncomment to use the simulator
	simulator(request, answer);
	// uncoment to access serial module
	//writeSerialMod_9(request);
	//readSerialMod_9(answer);
	
	//Se comprueba la velodidad
	if (1 == sscanf (answer,"SPD:%f\n",&speed)){
		displaySpeed(speed);
		//Si es menor de 3.5 ponemos a 1 la variable de GAS
		if (speed < 3.5) {
			BRK = 0;
			GAS = 1;	
		}
		//Sino, ponemos a 1 la variable de BRK
		else{
			BRK = 1;
			GAS = 0;
		}
	}
	return 0;
}

/**********************************************************
 *  Function: distancia
 *********************************************************/
int distancia() {
	//clear request and answer
	memset(request, '\0', 10);
	memset(answer, '\0', 10);
	//Se obtiene la distancia a la que esta el deposito
	strcpy(request,"DS:  REQ\n");
	
	//uncomment to use the simulator
	simulator(request, answer);
	// uncoment to access serial module
	//writeSerialMod_9(request);
	//readSerialMod_9(answer);
	
	//Si la distancia es mayor o igual a 0 se muestra.
	if (1 == sscanf(answer,"DS:%5d\n",&DS)){
		if (DS >= 0){
			displayDistance(DS);
		}
	}
	return 0;
}

/**********************************************************
 *  Function: finDescarga
 *********************************************************/
int finDescarga() {
	//clear request and answer
	memset(request, '\0', 10);
	memset(answer, '\0', 10);
	//Se obtiene el estado de movimiento
	strcpy(request, "STP: REQ\n");
	
	//uncomment to use the simulator
	simulator(request, answer);
	// uncoment to access serial module
	//writeSerialMod_9(request);
	//readSerialMod_9(answer);
	
	//Se comprueba si ha acabado de moverse o sigue en movimiento
	if (0 == strcmp(answer, "STP:  GO\n")) {
		STP = 0;
		displayStop(STP);
	}
	if (0 == strcmp(answer, "STP:STOP\n")) {
		STP = 1;
		displayStop(STP);
	}
	return 0;
}

/**********************************************************
 *  Function: modoNormal
 *********************************************************/
int modoNormal() {
	struct timespec tiempoInicial, tiempoFin, tiempoResta, tiempoPeriodo;
	int cicloSec = 0;
	printf("%f",cicloSec);
	//clear request and answer
	memset(request, '\0', 10);
	memset(answer, '\0', 10);
	
	tiempoPeriodo.tv_sec = TIEMPO_CICLO_SECUNDARIO_SEGUNDOS;
	tiempoPeriodo.tv_nsec = TIEMPO_CICLO_SECUNDARIO_NANOSEGUNDOS;
	
    clock_gettime(CLOCK_REALTIME, &tiempoInicial);
	
	// Se comprueba la distancia del deposito
	distancia();
	
	//Bucle while
	while (DS >= 11000) {
		switch (cicloSec) {
			case 0:
				sensor_luz();
				focos();
				task_slope();
				task_speed();
				acelerador();
				break;
			case 1:
				sensor_luz();
				focos();
				freno();
				mezclador();
				distancia();
				printf("%f",cicloSec);
				break;
		}
		//Se modifica cicloSec para acceder a case 0 o case 1
		cicloSec = (cicloSec + 1) % NUM_CICLOS_SECUNDARIOS;
		
		clock_gettime(CLOCK_REALTIME, &tiempoFin);
		
		diffTime(tiempoFin, tiempoInicial, &tiempoResta);
		diffTime(tiempoPeriodo, tiempoResta, &tiempoResta);
		
		//Si al hacer la resta obtenemos un tiempo negativo,
		//se muestra un mensaje de tiempo negativo.
		if (compTime(tiempoPeriodo, tiempoResta)<0 ){
			printf("TIEMPO NEGATIVO");
		}
		
		//Dormimos en nanosegundos el tiempo de la diferencia
		nanosleep(&tiempoResta, NULL);
		addTime(tiempoInicial, tiempoPeriodo, &tiempoInicial);
	}
	return 0;
}

/**********************************************************
 *  Function: modoFrenado
 *********************************************************/
int modoFrenado() {
	struct timespec tiempoInicial, tiempoFin, tiempoResta, tiempoPeriodo;
	int cicloSec = 0;
	
	//clear request and answer
	memset(request, '\0', 10);
	memset(answer, '\0', 10);

	tiempoPeriodo.tv_sec = TIEMPO_CICLO_SECUNDARIO_SEGUNDOS;
	tiempoPeriodo.tv_nsec = TIEMPO_CICLO_SECUNDARIO_NANOSEGUNDOS;
	
    clock_gettime(CLOCK_REALTIME, &tiempoInicial);
	
	// Se activan los focos de manera continua en el modoFrenado
	LAM = 1;
	
	// Se comprueba la distancia del deposito
	distancia();
	
	//Bucle while
	while ((DS > 0) || (speed >= 10)) {
		switch (cicloSec) {
			case 0:
				velModoFrenado();
				acelerador();
				freno();
				task_slope();
				mezclador();
				break;
			case 1:
				velModoFrenado();
				acelerador();
				freno();
				distancia();
				focos();
				break;
		}
		//Se modifica cicloSec para acceder a case 0 ó case 1
		cicloSec = (cicloSec + 1) % NUM_CICLOS_SECUNDARIOS;
		
		clock_gettime(CLOCK_REALTIME, &tiempoFin);
		
		diffTime(tiempoFin, tiempoInicial, &tiempoResta);
		diffTime(tiempoPeriodo, tiempoResta, &tiempoResta);
		
		//Si al hacer la resta obtenemos un tiempo negativo,
		//se muestra un mensaje de tiempo negativo.
		if (compTime(tiempoPeriodo, tiempoResta)<0 ){ 
			printf("TIEMPO NEGATIVO");
		}
		
		//Dormimos en nanosegundos el tiempo de la diferencia
		nanosleep(&tiempoResta, NULL);
		addTime(tiempoInicial, tiempoPeriodo, &tiempoInicial);
	}
	return 0;
}

/**********************************************************
 *  Function: modoDescargar
 *********************************************************/
int modoDescargar() {
	struct timespec tiempoInicial, tiempoFin, tiempoResta, tiempoPeriodo;

	//clear request and answer
	memset(request, '\0', 10);
	memset(answer, '\0', 10);
	
	tiempoPeriodo.tv_sec = TIEMPO_CICLO_SECUNDARIO_SEGUNDOS;
	tiempoPeriodo.tv_nsec = TIEMPO_CICLO_SECUNDARIO_NANOSEGUNDOS;
	
    clock_gettime(CLOCK_REALTIME, &tiempoInicial);
	
	// Se activan los focos de manera continua en el modoDescargar
	LAM = 1;
	
	//Bucle while
	while (STP == 1) {
		finDescarga();
		mezclador();
		focos();
		
		clock_gettime(CLOCK_REALTIME, &tiempoFin);
		
		diffTime(tiempoFin, tiempoInicial, &tiempoResta);
		diffTime(tiempoPeriodo, tiempoResta, &tiempoResta);
		
		//Si al hacer la resta obtenemos un tiempo negativo,
		//se muestra un mensaje de tiempo negativo.
		if (compTime(tiempoPeriodo, tiempoResta)<0 ){
			printf("TIEMPO NEGATIVO");
		}	
		
		//Dormimos en nanosegundos el tiempo de la diferencia
		nanosleep(&tiempoResta, NULL);
		addTime(tiempoInicial, tiempoPeriodo, &tiempoInicial);
	}
	// Para volver a descargar, se reestablece el valor a 1 de STP
	STP = 1;
	return 0;
}

/**********************************************************
 *  Function: controller
 *********************************************************/
void *controller(void *arg){

    clock_gettime(CLOCK_REALTIME, &tiempo_mezclador);
	
	//Bucle while
    while(1){
		modoNormal();
		modoFrenado();
		modoDescargar();
    }
}

/**********************************************************
 *  Function: main
 *********************************************************/
int main ()
{
    pthread_t thread_ctrl;
	sigset_t alarm_sig;
	int i;

	/* Block all real time signals so they can be used for the timers.
	   Note: this has to be done in main() before any threads are created
	   so they all inherit the same mask. Doing it later is subject to
	   race conditions */
	sigemptyset (&alarm_sig);
	for (i = SIGRTMIN; i <= SIGRTMAX; i++) {
		sigaddset (&alarm_sig, i);
	}
	sigprocmask (SIG_BLOCK, &alarm_sig, NULL);

    // init display
	displayInit(SIGRTMAX);
	
	// initSerialMod_9600 uncomment to work with serial module
	//initSerialMod_WIN_9600();

    /* Create first thread */
    pthread_create (&thread_ctrl, NULL, controller, NULL);
    pthread_join (thread_ctrl, NULL);
    return (0);
}
    
