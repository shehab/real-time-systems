/***********************************************************
 *  INCLUDES
 *********************************************************/
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <ioLib.h>
#include "displayA.h"
#include "serialcallLib.h"

/**********************************************************
 *  Constants
 **********************************************************/
#define NS_PER_S 1000000000
#define TIEMPO_CICLO_SECUNDARIO_SEGUNDOS 10 
#define TIEMPO_CICLO_SECUNDARIO_NANOSEGUNDOS 0
 
/**********************************************************
 *  Global Variables 
 *********************************************************/
float speed = 0.0;		
int gas = 0;
int brk = 0;
int mix = 0;
struct timespec tiempo_mezclador;

char request[10];
char answer[10];

/**********************************************************
 *  Function: task_speed
 *********************************************************/
int task_speed(){

    //clear request and answer
    memset(request,'\0',10);
    memset(answer,'\0',10);
    
    // request speed
	strcpy(request,"SPD: REQ\n");
		
	//uncomment to use the simulator
	//simulator(request, answer);
		
	// uncoment to access serial module
	writeSerialMod_9(request);
	readSerialMod_9(answer);
	
	
	
	// display speed
	if (1 == sscanf (answer,"SPD:%f\n",&speed)) {
		displaySpeed(speed); 
		//Speed control
		if (speed < 55.0) {
			brk = 0;
			gas = 1;
		}
		if (speed >= 55.0) {
			brk = 1;
			gas = 0;
		}
	}
	return 0;
}

/**********************************************************
 *  Function: task_slope
 *********************************************************/
int task_slope(){

	//clear request and answer
	memset(request,'\0',10);
	memset(answer,'\0',10);

	// request slope
	strcpy(request,"SLP: REQ\n");

	//uncomment to use the simulator
	//simulator(request, answer);

	// uncoment to access serial module
	writeSerialMod_9(request);
	readSerialMod_9(answer);  

	// display slope
	if (0 == strcmp(answer,"SLP:DOWN\n")) displaySlope(-1);	
	if (0 == strcmp(answer,"SLP:FLAT\n")) displaySlope(0);	
	if (0 == strcmp(answer,"SLP:  UP\n")) displaySlope(1);

	return 0;
}

/**********************************************************
 *  Function: acelerador
 *********************************************************/
int acelerador() {
	//clear request and answer
	memset(request, '\0', 10);
	memset(answer, '\0', 10);
	
	//Comprobamos la variable GAS
	if (gas == 1){
		//Se activa el acelerador
		strcpy(request, "GAS: SET\n");
	}
	if (gas == 0){
		// Se desactiva el acelerador
		strcpy(request, "GAS: CLR\n");
	}	
	//uncomment to use the simulator
	//simulator(request, answer);
	
	// uncoment to access serial module
	writeSerialMod_9(request);
	readSerialMod_9(answer);
	
	// Se muestra la respuesta si recibimos OK
	if (0 == strcmp(answer, "GAS:  OK\n")) {
		displayGas(gas);
	}
	return 0;
}

/**********************************************************
 *  Function: freno
 *********************************************************/
int freno() {
	//clear request and answer
	memset(request, '\0', 10);
	memset(answer, '\0', 10);
	
	//Comprobamos la variable BRK
	if (brk == 1){
		//Se activa el freno
		strcpy(request, "BRK: SET\n");
	}
	if (brk == 0){
		//Se desactiva el freno
		strcpy(request, "BRK: CLR\n");
	}	
	//uncomment to use the simulator
	//simulator(request, answer);
	
	// uncoment to access serial module
	writeSerialMod_9(request);
	readSerialMod_9(answer);
	
	// Se muestra la respuesta si recibimos OK
	if (0 == strcmp(answer, "BRK:  OK\n")) {
		displayBrake(brk);
	}
	return 0;
}

/**********************************************************
 *  Function: addTime 
 *********************************************************/
void addTime(struct timespec end, 
			  struct timespec start, 
			  struct timespec *add) 
{
	unsigned long aux;
	aux = start.tv_nsec + end.tv_nsec;
	add->tv_sec = start.tv_sec + end.tv_sec + (aux / NS_PER_S);
	add->tv_nsec = aux % NS_PER_S;
}

/**********************************************************
 *  Function: compTime 
 *********************************************************/
int compTime(struct timespec t1, 
			  struct timespec t2)
{
	if (t1.tv_sec == t2.tv_sec) {
		if (t1.tv_nsec == t2.tv_nsec) {
			return (0);
		} else if (t1.tv_nsec > t2.tv_nsec) {
			return (1);
		} else if (t1.tv_nsec < t2.tv_nsec) {
			return (-1);
		}
	} else if (t1.tv_sec > t2.tv_sec) {
		return (1);
	} else if (t1.tv_sec < t2.tv_sec) {
		return (-1);
	} 
	return (0);
}

/**********************************************************
 *  Function: diffTime 
 *********************************************************/
void diffTime(struct timespec end, 
			  struct timespec start, 
			  struct timespec *diff) 
{
	if (end.tv_nsec < start.tv_nsec) {
		diff->tv_nsec = NS_PER_S - start.tv_nsec + end.tv_nsec;
		diff->tv_sec = end.tv_sec - (start.tv_sec+1);
	} else {
		diff->tv_nsec = end.tv_nsec - start.tv_nsec;
		diff->tv_sec = end.tv_sec - start.tv_sec;
	}
}

/**********************************************************
 *  Function: mezclador
 *********************************************************/
int mezclador() {
	//clear request and answer
	memset(request, '\0', 10);
	memset(answer, '\0', 10);
	struct timespec tiempo, tiempo_mezcla, comp;
	
	//Establecemos el tiempo del mezclador
	comp.tv_sec = 30;
	comp.tv_nsec = 0;
	clock_gettime(CLOCK_REALTIME, &tiempo);
	
	//Se restan los tiempos de reloj y mezclador para obtener el tiempo de mezcla
	diffTime(tiempo, tiempo_mezclador, &tiempo_mezcla);
	
	//Se compara el tiempo de mezcla con el tiempo actual
	if (compTime(tiempo_mezcla, comp) == 1) {
		//Se activa el mezclador
		if (mix == 0) {
			mix = 1;
			clock_gettime(CLOCK_REALTIME, &tiempo_mezclador);
			strcpy(request, "MIX: SET\n");
		} else {
			//Se desactiva el mezclador
			if (mix == 1) {
				mix = 0;
				clock_gettime(CLOCK_REALTIME, &tiempo_mezclador);
				strcpy(request, "MIX: CLR\n");
			}
		}
		//uncomment to use the simulator
		//simulator(request, answer);
		
		// uncoment to access serial module
		writeSerialMod_9(request);
		readSerialMod_9(answer);
		
		// Se muestra la respuesta si recibimos OK
		if (0 == strcmp(answer, "MIX:  OK\n")) {
			displayMix(mix);
		}
	}
	return 0;
}



/**********************************************************
 *  Function: controller
 *********************************************************/
void *controller(void *arg){

    //clear request and answer
	memset(request, '\0', 10);
	memset(answer, '\0', 10);
	struct timespec tiempoInicial, tiempoFin, tiempoPeriodo, tiempoResta;
	
	tiempoPeriodo.tv_sec = TIEMPO_CICLO_SECUNDARIO_SEGUNDOS;
	tiempoPeriodo.tv_nsec = TIEMPO_CICLO_SECUNDARIO_NANOSEGUNDOS;
	
    clock_gettime(CLOCK_REALTIME, &tiempoInicial);
    clock_gettime(CLOCK_REALTIME, &tiempo_mezclador);
	
	//Duerme un segundo.
	sleep(1);
    // Endless loop
    while(1) {
	
    	// calling task of speed
    	task_speed();
		
		// caling task of slope
		task_slope();
		
		// Se llama a la funcion acelerador
		acelerador();
		
		// Se llama a la funcion freno
		freno();
		
		// Se llama a la funcion mezclador
		mezclador();
		
		/*Ahora calculamos el tiempo de la tarea y del ciclo,
		 * si el ciclo de la tarea(tiempoPeriodo) es superior 
		 * al tiempo de ciclo(tiempoFin) se obtendr� un tiempo negativo,
		 * por lo que se muestra un mensaje de tiempo negativo.
		*/
		clock_gettime(CLOCK_REALTIME, &tiempoFin);		
		diffTime(tiempoFin, tiempoInicial, &tiempoResta);
		diffTime(tiempoPeriodo, tiempoResta, &tiempoResta);
		if (compTime(tiempoPeriodo, tiempoResta)<0 ){ 
			printf("Error, tiempo negativo");
		}
		
		//Dormimos en nanosegundos el tiempo de la diferencia
		nanosleep(&tiempoResta, NULL);
		addTime(tiempoInicial, tiempoPeriodo, &tiempoInicial);
    }
}

/**********************************************************
 *  Function: main
 *********************************************************/
int main ()
{
    pthread_t thread_ctrl;
	sigset_t alarm_sig;
	int i;

	/* Block all real time signals so they can be used for the timers.
	   Note: this has to be done in main() before any threads are created
	   so they all inherit the same mask. Doing it later is subject to
	   race conditions */
	sigemptyset (&alarm_sig);
	for (i = SIGRTMIN; i <= SIGRTMAX; i++) {
		sigaddset (&alarm_sig, i);
	}
	sigprocmask (SIG_BLOCK, &alarm_sig, NULL);

    // init display
	displayInit(SIGRTMAX);
	
	// initSerialMod_9600 uncomment to work with serial module
	initSerialMod_WIN_9600 ();

    /* Create first thread */
    pthread_create (&thread_ctrl, NULL, controller, NULL);
    pthread_join (thread_ctrl, NULL);
    return (0);
}
    

