#include <string.h>
#include <stdio.h>
#include <stdint.h>

#define MILLIS_IN_A_SECOND   1000

/*
 * Constants
 */

#define STATE_SET_DISTANCE     0
#define STATE_MOVING           1

#define STILL                   0
#define MOVING                  1

// Acceleration in m/s
#define ACCELERATION_SLOPE  0.25f
#define ACCELERATION_GAS     0.5f
#define ACCELERATION_BRAKE  -0.5f

 //Speed values
#define MAX_SPEED              70
#define MIN_SPEED              40
#define STOPPING_THRESHOLD     10

//Slope values
#define SLOPE_DOWN             -1
#define SLOPE_FLAT              0
#define SLOPE_UP                1

//Binary values for events
#define GAS_ENABLED             1
#define GAS_DISABLED            0
#define BRAKE_ENABLED           1
#define BRAKE_DISABLED          0
#define MIXING_ENABLED          1
#define MIXING_DISABLED         0
#define LAMP_ENABLED            1
#define LAMP_DISABLED           0

// Pins
#define PIN_LED_ACCELERATING   13
#define PIN_LED_BRAKING        12
#define PIN_LED_MIXER          11
#define PIN_LED_SPEED          10
#define PIN_LED_LAMP            7

#define PIN_SLOPE_UP            9
#define PIN_SLOPE_DOWN          8

#define PIN_LDR                A0

#define PIN_POTENTIOMETER      A1
#define PIN_SCREEN_A            2
#define PIN_SCREEN_B            3
#define PIN_SCREEN_C            4
#define PIN_SCREEN_D            5
#define PIN_BUTTON              6

//Main and secondadary cycles.
//Decide the secondary and primary cycle.
#define MAIN_CYCLE           200
#define SECONDARY_CYCLE      100
#define NUMBER_SCYCLES         2

#define DISTANCE_SELECTION_MODE 1
#define APPROACH_MODE           2
#define STOP_MODE               3

#define HAS_BEEN_PRESSED        1
#define HAS_NOT_BEEN_PRESSED    0


//Time in milliseconds
unsigned long last_speed_timestamp    = 0;
unsigned long current_speed_timestamp = 0;
unsigned long last_timestamp          = 0;
unsigned long current_timestamp       = 0;

//double speed                          = 9;
double speed                          =55;

int speed_led                         = 0;
int slope                             = 0;

byte switch_to_mode                  = 0;

byte gas_active                       = 0;
byte brake_active                     = 0;
byte mixer_active                     = 0;
byte lamp_active                      = 0;
byte is_moving                        = MOVING;
byte execution_mode                   = 1;

double delta_t                        = 0;
float elapsed_time                    = 0;
double acceleration                   = 0;
long remaining_distance               = 90000;
byte secondary_cycle                  = 0;
byte ldr_value                        = 0;
byte digit_screen                     = 0;
byte state_distance                   = 0;

int button_pressed                    = 0;
byte released                         = 1;



void update_speed() {
    if(execution_mode == STOP_MODE) {
        speed = 0;
    } else {
        current_speed_timestamp = millis();
        acceleration            = (ACCELERATION_SLOPE * slope) + (ACCELERATION_GAS * gas_active) + (ACCELERATION_BRAKE * brake_active);
        speed                   = acceleration * (current_speed_timestamp - last_speed_timestamp) / (double) MILLIS_IN_A_SECOND + speed; //a*t + Vo
        //delta_t                 = (current_timestamp - last_speed_timestamp) / MILLIS_IN_A_SECOND;
        delta_t                 = (current_timestamp - last_speed_timestamp);
        delta_t                 /= MILLIS_IN_A_SECOND;
        // Serial.print("delta_t: ");
        // Serial.println(delta_t);
        // Serial.print("remaining_distance: ");
        // Serial.println(remaining_distance);
        // Serial.print("Speed: ");
        // Serial.println(speed);
        last_speed_timestamp = current_speed_timestamp;
        if(speed < 0) speed = 0;

        if(execution_mode == APPROACH_MODE && remaining_distance > 0) {
        //if(execution_mode == APPROACH_MODE) {
            // x = a/2*t^2 + Vo*t + x0
            remaining_distance = remaining_distance - (acceleration/2 * delta_t*delta_t + speed * delta_t);
            if(remaining_distance < 0) remaining_distance = 0;
            if(remaining_distance == 0 && speed < STOPPING_THRESHOLD) {
                speed = 0;
                is_moving = STILL;
                switch_to_mode = STOP_MODE;
            }

            //The braking process ends when the distance to the deposit is 0 and the speed is less than 10 m/s (Otherwise the download won’t happen).
            else if (remaining_distance == 0 && speed >= STOPPING_THRESHOLD) {
                switch_to_mode = DISTANCE_SELECTION_MODE;
            }
        }

        speed_led = map(speed, MIN_SPEED, MAX_SPEED, 0, 255);

        if (speed < MIN_SPEED || speed > MAX_SPEED) {
            speed_led = LOW;
        }
    }

}


int comm_server() {
    int  i;
    char request[10];
    char answer[10];
    int  speed_int;
    int  speed_dec;

    // while there is enough data for a request
    if (Serial.available() >= 9) {
        // read the request
        i = 0;
        while ( (i<9) && (Serial.available() >= (9-i)) ) {
            // read one character
            request[i] = Serial.read();

            // if the new line is positioned wrong
            if ( (i != 8) && (request[i] == '\n') ) {
                // Send error and start all over again
                //Serial.println("error bitch");
                sprintf(answer,"MSG: ERR\n");
                Serial.print(answer);
                memset(request,'\0', 9);
                i=0;
            } else {
              // read the next character
              i++;
          }
      }
      request[9]='\0';

        sprintf(answer,"MSG: ERR\n"); //Default value

        //Check for all common messages (i.e. present in every mode)
        if (strcmp("SPD: REQ\n",request) == 0) {
            if(execution_mode != STOP_MODE) {
                // send the answer for speed request
                speed_int = (int)speed;
                speed_dec = ((int)(speed*10)) % 10;
                sprintf(answer,"SPD:%2d.%d\n",speed_int,speed_dec);
            }
        } else if (strcmp("SLP: REQ\n",request) == 0) {
            if(execution_mode != STOP_MODE) {
                if(slope == SLOPE_UP)   sprintf(answer, "SLP:  UP\n");
                if(slope == SLOPE_FLAT) sprintf(answer, "SLP:FLAT\n");
                if(slope == SLOPE_DOWN) sprintf(answer, "SLP:DOWN\n");
            }
        } else if (strcmp("GAS: SET\n", request) == 0) {
            if(execution_mode != STOP_MODE) {
                //Check for the brake?
                gas_active = GAS_ENABLED;
                sprintf(answer, "GAS:  OK\n");
            }
        } else if (strcmp(request, "GAS: CLR\n") == 0) {
            if(execution_mode != STOP_MODE) {
                gas_active = GAS_DISABLED;
                sprintf(answer, "GAS:  OK\n");
            }
        } else if (strcmp(request, "BRK: SET\n") == 0) {
            if(execution_mode != STOP_MODE) {
                brake_active = BRAKE_ENABLED;
                sprintf(answer, "BRK:  OK\n");
            }
        } else if (strcmp(request, "BRK: CLR\n") == 0) {
            if(execution_mode != STOP_MODE) {
                brake_active = BRAKE_DISABLED;
                sprintf(answer, "BRK:  OK\n");
            }
        } else if (strcmp(request, "MIX: SET\n") == 0) {

            mixer_active = MIXING_ENABLED;
            sprintf(answer, "MIX:  OK\n");

        } else if (strcmp(request, "MIX: CLR\n") == 0) {

            mixer_active = MIXING_DISABLED;
            sprintf(answer, "MIX:  OK\n");

        } else if (strcmp(request, "LIT: REQ\n") == 0) {
            if(execution_mode != STOP_MODE) {
                sprintf(answer, "LIT: %2i%%\n", ldr_value);
            }
        } else if (strcmp(request, "LAM: SET\n") == 0) {

            lamp_active = LAMP_ENABLED;
            sprintf(answer, "LAM:  OK\n");

        } else if (strcmp(request, "LAM: CLR\n") == 0) {

            lamp_active = LAMP_DISABLED;
            sprintf(answer, "LAM:  OK\n");

        } else if (strcmp(request, "STP: REQ\n") == 0) {

            is_moving == MOVING ? sprintf(answer, "STP:  GO\n") : sprintf(answer, "STP:STOP\n");

        } else if (strcmp(request, "DS:  REQ\n") == 0) {
            //DESIGN DECISION: COMMENT IN THE REPORT
            //TODO WRITE A COMMENT THERE SO THAT WE REMEMBER TO ASK HIM ABOUT THAT
            execution_mode == DISTANCE_SELECTION_MODE ? sprintf(answer, "DS:%5ld\n", 99999) : sprintf(answer, "DS:%5ld\n", remaining_distance);
        }

        Serial.print(answer);
    }

    return 0;
}


void manage_input() {
    switch(execution_mode){
        case DISTANCE_SELECTION_MODE: {
            if(digitalRead(PIN_SLOPE_UP)   == HIGH && digitalRead(PIN_SLOPE_DOWN) == LOW ) slope = SLOPE_UP;
            if(digitalRead(PIN_SLOPE_UP)   == LOW  && digitalRead(PIN_SLOPE_DOWN) == LOW ) slope = SLOPE_FLAT;
            if(digitalRead(PIN_SLOPE_DOWN) == HIGH && digitalRead(PIN_SLOPE_UP)   == LOW ) slope = SLOPE_DOWN;

            ldr_value              = map(analogRead(PIN_LDR), 0, 1023, 0, 99);
            //TODO
            digit_screen           = map(analogRead(PIN_POTENTIOMETER), 100, 987, 1, 9);
            //Serial.print("digit_screen: ");
            //Serial.println(digit_screen);
                // aux = analogRead(PIN_POTENTIOMETER);
                // if(aux < res_pot) res_pot = aux;
            //Serial.println(res_pot);
            if(digit_screen == 10) digit_screen--;
            //DEsign decision REPORT
            //114
            //987

            if(check_button() == HAS_BEEN_PRESSED) {
                remaining_distance = digit_screen;
                remaining_distance *= 10000;
                //Serial.print("remaining_distance: ");
                //Serial.println(remaining_distance);
                switch_to_mode     = APPROACH_MODE;
            }

            break;
        }

        case APPROACH_MODE: {

            if(digitalRead(PIN_SLOPE_UP)   == HIGH && digitalRead(PIN_SLOPE_DOWN) == LOW ) slope = SLOPE_UP;
            if(digitalRead(PIN_SLOPE_UP)   == LOW  && digitalRead(PIN_SLOPE_DOWN) == LOW ) slope = SLOPE_FLAT;
            if(digitalRead(PIN_SLOPE_DOWN) == HIGH && digitalRead(PIN_SLOPE_UP)   == LOW ) slope = SLOPE_DOWN;
            break;
        }

        case STOP_MODE: {

            lamp_active           = HIGH;

            if(check_button() == HAS_BEEN_PRESSED) {
                is_moving       = MOVING;
                lamp_active     = 0;
                switch_to_mode = DISTANCE_SELECTION_MODE;
            }

            break;
        }

    }

}


int check_button() {
    button_pressed = digitalRead(PIN_BUTTON);

    if(released && button_pressed) { //Detect rising edge
        released = 0;
    }

    if(released == 0 && !button_pressed) {
        // Lowering edge
        released = 1;
        return HAS_BEEN_PRESSED;
    } else {
        return HAS_NOT_BEEN_PRESSED;
    }
}

void manage_output() {
    switch(execution_mode) {
        case DISTANCE_SELECTION_MODE:
        case APPROACH_MODE: {

            digitalWrite(PIN_LED_ACCELERATING, gas_active);
            digitalWrite(PIN_LED_BRAKING,      brake_active);
            digitalWrite(PIN_LED_MIXER,        mixer_active);
            digitalWrite(PIN_LED_LAMP,         lamp_active);
            analogWrite(PIN_LED_SPEED,         speed_led);
            set_screen(execution_mode == DISTANCE_SELECTION_MODE ? digit_screen : remaining_distance/10000);

            break;
        }

        case STOP_MODE: {

            digitalWrite(PIN_LED_MIXER, mixer_active);

            break;
        }
    }

}


void clear_screen() {
    digitalWrite(PIN_SCREEN_A, LOW);
    digitalWrite(PIN_SCREEN_B, LOW);
    digitalWrite(PIN_SCREEN_C, LOW);
    digitalWrite(PIN_SCREEN_D, LOW);
}

void set_screen(int digit) {
    switch(digit) {
        case 0:
            digitalWrite(PIN_SCREEN_A, LOW);
            digitalWrite(PIN_SCREEN_B, LOW);
            digitalWrite(PIN_SCREEN_C, LOW);
            digitalWrite(PIN_SCREEN_D, LOW);
            break;
        case 1:
            digitalWrite(PIN_SCREEN_A, HIGH);
            digitalWrite(PIN_SCREEN_B, LOW);
            digitalWrite(PIN_SCREEN_C, LOW);
            digitalWrite(PIN_SCREEN_D, LOW);
            break;
        case 2:
            digitalWrite(PIN_SCREEN_A, LOW);
            digitalWrite(PIN_SCREEN_B, HIGH);
            digitalWrite(PIN_SCREEN_C, LOW);
            digitalWrite(PIN_SCREEN_D, LOW);
            break;
        case 3:
            digitalWrite(PIN_SCREEN_A, HIGH);
            digitalWrite(PIN_SCREEN_B, HIGH);
            digitalWrite(PIN_SCREEN_C, LOW);
            digitalWrite(PIN_SCREEN_D, LOW);
            break;
        case 4:
            digitalWrite(PIN_SCREEN_A, LOW);
            digitalWrite(PIN_SCREEN_B, LOW);
            digitalWrite(PIN_SCREEN_C, HIGH);
            digitalWrite(PIN_SCREEN_D, LOW);
            break;
        case 5:
            digitalWrite(PIN_SCREEN_A, HIGH);
            digitalWrite(PIN_SCREEN_B, LOW);
            digitalWrite(PIN_SCREEN_C, HIGH);
            digitalWrite(PIN_SCREEN_D, LOW);
            break;
        case 6:
            digitalWrite(PIN_SCREEN_A, LOW);
            digitalWrite(PIN_SCREEN_B, HIGH);
            digitalWrite(PIN_SCREEN_C, HIGH);
            digitalWrite(PIN_SCREEN_D, LOW);
            break;
        case 7:
            digitalWrite(PIN_SCREEN_A, HIGH);
            digitalWrite(PIN_SCREEN_B, HIGH);
            digitalWrite(PIN_SCREEN_C, HIGH);
            digitalWrite(PIN_SCREEN_D, LOW);
            break;
        case 8:
            digitalWrite(PIN_SCREEN_A, LOW);
            digitalWrite(PIN_SCREEN_B, LOW);
            digitalWrite(PIN_SCREEN_C, LOW);
            digitalWrite(PIN_SCREEN_D, HIGH);
            break;
        case 9:
            digitalWrite(PIN_SCREEN_A, HIGH);
            digitalWrite(PIN_SCREEN_B, LOW);
            digitalWrite(PIN_SCREEN_C, LOW);
            digitalWrite(PIN_SCREEN_D, HIGH);
            break;
        default:
            clear_screen();
    }
}


void setup() {
    Serial.begin(9600);
    pinMode(PIN_LED_ACCELERATING, OUTPUT);
    pinMode(PIN_LED_BRAKING,      OUTPUT);
    pinMode(PIN_LED_MIXER,        OUTPUT);
    pinMode(PIN_LED_SPEED,        OUTPUT);
    pinMode(PIN_LED_LAMP,         OUTPUT);

    pinMode(PIN_SCREEN_A,         OUTPUT);
    pinMode(PIN_SCREEN_B,         OUTPUT);
    pinMode(PIN_SCREEN_C,         OUTPUT);
    pinMode(PIN_SCREEN_D,         OUTPUT);

    pinMode(PIN_SLOPE_UP,         INPUT);
    pinMode(PIN_SLOPE_DOWN,       INPUT);
    pinMode(PIN_LDR,              INPUT);
    pinMode(PIN_BUTTON,           INPUT);
    pinMode(PIN_POTENTIOMETER,    INPUT);


    //millis(): Returns the number of milliseconds since the Arduino board began running the current program.
    last_speed_timestamp = millis();
}

void finish_secondary_cycle (int duration) {
    delay(1);
    secondary_cycle    = (secondary_cycle + 1) % (MAIN_CYCLE / duration);
    current_timestamp  = millis();

    //T = SC - (max_time_int -t1 + t2)
    elapsed_time       = current_timestamp - last_timestamp;

    if(elapsed_time > duration) {
        //ERROR
        Serial.print("The maximum duration of the secondary cycle has been exeeded!\n");
    }

    //Chech that the amount of milis to sleep is smaller than 10
    delay(duration - elapsed_time);

    last_timestamp += duration;
}

void loop() {

    switch (execution_mode) {
        case DISTANCE_SELECTION_MODE: {
            switch_to_mode = DISTANCE_SELECTION_MODE;

            switch (secondary_cycle) {
                case 0: {
                    comm_server();
                    manage_output();
                    manage_input();
                    update_speed();
                    break;
                }
                case 1: {
                    manage_output();
                    manage_input();
                    update_speed();
                    break;
                }
            }
            break;

        }
        case APPROACH_MODE: {
            switch_to_mode = APPROACH_MODE;

            switch (secondary_cycle) {
                case 0: {
                    comm_server();
                    manage_output();
                    manage_input();
                    update_speed();
                    break;
                }
                case 1: {
                    manage_output();
                    manage_input();
                    update_speed();
                    break;
                }
            }
            break;
        }
        case STOP_MODE: {
            switch_to_mode = STOP_MODE;
            //only continue with mixer

            switch (secondary_cycle) {
                case 0: {
                    comm_server();
                    manage_output();
                    manage_input();
                    update_speed();
                    break;
                }
                case 1: {
                    manage_input();
                    manage_output();
                    //speed = 0
                    update_speed();
                    //check pushbutton to change mode

                    break;
                }
            }
            break;
        }

    }
    execution_mode = switch_to_mode;
    finish_secondary_cycle(SECONDARY_CYCLE);
    //finish_secondary_cycle(execution_mode == APPROACH_MODE ? SECONDARY_CYCLE / 2 : SECONDARY_CYCLE);
}


