#include <string.h>
#include <stdio.h>


/*
 * Constants
 */
// Acceleration in m/s
#define ACCELERATION_SLOPE  0.25f
#define ACCELERATION_GAS    0.5f
#define ACCELERATION_BRAKE -0.5f

#define SLOPE_DOWN -1
#define SLOPE_FLAT  0
#define SLOPE_UP    1


#define GAS_ENABLED     1
#define GAS_DISABLED    0
#define BRAKE_ENABLED   1
#define BRAKE_DISABLED  0
#define MIXING_ENABLED  1
#define MIXING_DISABLED 0

// Pins
#define LED_ACCELERATING 13
#define LED_BRAKING      12
#define LED_MIXER        11
#define LED_SPEED        10
#define LED_SLOPE_UP      9
#define LED_SLOPE_DOWN    8


unsigned long last_speed_timestamp; //Time in milliseconds
float speed       = 55.0f;
byte slope        = 0;
byte gas_active   = 0;
byte brake_active = 0;
byte mix_active   = 0;



void update_speed()
{
    current_timestamp    = millis()
    acceleration         = (ACCELERATION_SLOPE * slope) + (ACCELERATION_GAS * gas_active) + (ACCELERATION_BRAKE * brake_active);
    speed                = acceleration * (current_timestamp - last_speed_timestamp) + speed; //a*t + Vo

    last_speed_timestamp = current_timestamp;
}


int comm_server()
{
    int  i;
    char request[10];
    char answer[10];
    int  speed_int;
    int  speed_dec;

    // while there is enough data for a request
    if (Serial.available() >= 9) {
        // read the request
        i = 0;
        while ( (i<9) && (Serial.available() >= (9-i)) ) {
            // read one character
            request[i] = Serial.read();

            // if the new line is positioned wrong
            if ( (i != 8) && (request[i] == '\n') ) {
                // Send error and start all over again
                sprintf(answer,"MSG: ERR\n");
                Serial.print(answer);
                memset(request,'\0', 9);
                i=0;
            } else {
              // read the next character
              i++;
            }
        }
        request[9]='\0';

        sprintf(answer,"MSG: ERR\n"); //Default value

        if (strcmp("SPD: REQ\n",request) == 0) {

            // send the answer for speed request
            speed_int = (int)speed;
            speed_dec = ((int)(speed*10)) % 10;
            sprintf(answer,"SPD:%02d.%d\n",speed_int,speed_dec);

        } else if (strcmp("SLP: REQ\n",request) == 0) {

            if(slope == SLOPE_UP)   sprintf(answer, "SLP:  UP\n");
            if(slope == SLOPE_FLAT) sprintf(answer, "SLP:FLAT\n");
            if(slope == SLOPE_DOWN) sprintf(answer, "SLP:DOWN\n");

        } else if (strcmp(request, "GAS: SET\n") == 0) {

            //Check for the brake?
            gas_active = GAS_DISABLED;
            sprintf(answer, "GAS:  OK\n");

        } else if (strcmp(request, "GAS: CLR\n") == 0) {

            gas_active = GAS_DISABLED;
            sprintf(answer, "GAS:  OK\n");

        } else if (strcmp(request, "BRK: SET\n") == 0) {

            brake_active = BRAKE_ENABLED;
            sprintf(answer, "BRK:  OK\n");

        } else if (strcmp(request, "BRK: CLR\n") == 0) {

            brake_active = BRAKE_DISABLED;
            sprintf(answer, "BRK:  OK\n");

        } else if (strcmp(request, "MIX: SET\n") == 0) {

            mix_active = MIXING_ENABLED;
            sprintf(answer, "MIX:  OK\n");

        } else if (strcmp(request, "MIX: CLR\n") == 0) {

            mix_active = MIXING_DISABLED;
            sprintf(answer, "MIX:  OK\n");

        }

        Serial.print(answer);
    }

    return 0;
}


void manage_input()
{
    if(digitalRead(SLOPE_UP)   == HIGH && digitalRead(SLOPE_DOWN) == LOW ) slope = S;
    if(digitalRead(SLOPE_UP)   == LOW  && digitalRead(SLOPE_DOWN) == LOW ) slope = 0;
    if(digitalRead(SLOPE_DOWN) == HIGH && digitalRead(SLOPE_UP)   == LOW ) slope = -1;
}

void manage_output()
{

}


void setup()
{
    Serial.begin(9600);
    last_speed_timestamp = millis();
}


void loop()
{
    if(manage_input();
    comm_server();
    update_speed();
    manage_output();
}
