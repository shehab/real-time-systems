//Section A pin declaration
int gas_led = 13;
int break_led = 12;
int speed_led_pwm = 10;
int mixer_led = 11;
int down_slope_switch = 8;
int up_slope_switch = 9;

//Section B pin declaration
int headlight_led = 7;
int light_sensor = A0;

//Section C pin declaration
int distance_to_stop_pot = A1;
int screen_pin_A = 2;
int screen_pin_B = 3;
int screen_pin_C = 4;
int screen_pin_D = 5;

//Variable declaration
float current_speed = 0;
float gas_acc = 0.5;
float break_acc = -0.5;
float down_hill_acc = 0.25;
float up_hill_acc = -0.25;
byte distance_to_stop = 0; // 0 is unknown (infinity)
unsigned long last_time;

void setup(){
  
  Serial.begin(9600);
  
  //Output Pins
  pinMode(gas_led, OUTPUT);
  pinMode(break_led, OUTPUT);
  pinMode(speed_led_pwm, OUTPUT);
  pinMode(mixer_led, OUTPUT);
  pinMode(headlight_led, OUTPUT);
  pinMode(screen_pin_A, OUTPUT);
  pinMode(screen_pin_B, OUTPUT);
  pinMode(screen_pin_C, OUTPUT);
  pinMode(screen_pin_D, OUTPUT);
    
  //Input Pins  
  pinMode(light_sensor, INPUT);
  pinMode(distance_to_stop_pot, INPUT);
  
  //set the timer
  last_time = millis();
}

void loop(){
  if(millis() - last_time >= 1000){
    last_time = millis();
    set_speed();
  }
  process_message(recieve_message());
}

String recieve_message(){
  if(Serial.available() > 0){
    int i = 0;
    String message;
    for(i = 0; i < 9; i++){
      message += Serial.read();
    }
    return message;
  }
}

void set_speed(){
  if(gas_led == HIGH){
    current_speed += gas_acc;
  }
  if(break_led == HIGH){
    current_speed += break_acc;
  }
  if(digitalRead(down_slope_switch) == HIGH){
    current_speed += down_hill_acc;
  }
  if(digitalRead(up_slope_switch) == HIGH){
    current_speed += up_hill_acc;
  }
}

void process_message(String message){
  switch((int)message.substring(0,4)){
    case "GAS:":
      switch(message.substring(4)){
        case " SET\n":
          press_gas();
          success_message(1);
        case " CLR\n":
          release_gas();
          success_message(1);
        case default: error_reply();
      }
    case "BRK:":
      switch(message.substring(4)){
        case " SET\n":
          press_break();
          success_message(2);
        case " CLR\n":
          release_break();
          success_message(2);
        case default: error_reply();
      }
    case "MIX:":
      switch(message.substring(4)){
        case " SET\n":
          activate_mixer();
          success_message(3);
        case " CLR\n":
          deactivate_mixer();
          success_message(3);
        case default: error_reply();
      }
    case "SLP:":
      switch(message.substring(4)){
        case " REQ\n": send_slope();
        case default: error_reply();
      }      
    case "SPD:":
      switch(message.substring(4)){
        case " REQ\n": send_speed;
        case default: error_reply();
    case default: error_reply();
  }    
}

void send_speed(){
  Serial.write("SPD:00.0\n");
}

void send_slope(){
  if(digitalRead(down_slope_switch) == HIGH){
    Serial.write("SLP:DOWN\n");
  } else if(digitalRead(up_slope_switch) == HIGH){
    Serial.write("SLP:  UP\n");
  } else{
    Serial.write("SLP:FLAT\n");
  }
}

void error_reply(){
  Serial.write("MSG: ERR\n");
}

void press_gas(){
  digitalWrite(gas_led, HIGH);
}

void release_gas(){
  digitalWrite(gas_led, LOW);
}

void press_break(){
  digitalWrite(break_led, HIGH);
}

void release_break(){
  digitalWrite(break_led, LOW);
}

void activate_mixer(){
  digitalWrite(mixer_led, HIGH);
}

void deactivate_mixer(){
  digitalWrite(mixer_led, LOW);
}

void set_speed_led(){
  analogWrite(speed_led_pwm, map(current_speed, 40, 70, 0, 255));
}

void set_screen(){
  switch(map(read_pot(), 0, 1024, 1, 9){
    case 0:
      digitalWrite(screen_pin_A, LOW);
      digitalWrite(screen_pin_B, LOW);
      digitalWrite(screen_pin_C, LOW);
      digitalWrite(screen_pin_D, LOW);
    case 1:
      digitalWrite(screen_pin_A, HIGH);
      digitalWrite(screen_pin_B, LOW);
      digitalWrite(screen_pin_C, LOW);
      digitalWrite(screen_pin_D, LOW);
    case 2:
      digitalWrite(screen_pin_A, LOW);
      digitalWrite(screen_pin_B, HIGH);
      digitalWrite(screen_pin_C, LOW);
      digitalWrite(screen_pin_D, LOW);
    case 3:
      digitalWrite(screen_pin_A, HIGH);
      digitalWrite(screen_pin_B, HIGH);
      digitalWrite(screen_pin_C, LOW);
      digitalWrite(screen_pin_D, LOW);
    case 4:
      digitalWrite(screen_pin_A, LOW);
      digitalWrite(screen_pin_B, LOW);
      digitalWrite(screen_pin_C,HIGH);
      digitalWrite(screen_pin_D, LOW);
    case 5:
      digitalWrite(screen_pin_A, HIGH);
      digitalWrite(screen_pin_B, LOW);
      digitalWrite(screen_pin_C, HIGH);
      digitalWrite(screen_pin_D, LOW);
    case 6:
      digitalWrite(screen_pin_A, LOW);
      digitalWrite(screen_pin_B, HIGH);
      digitalWrite(screen_pin_C, HIGH);
      digitalWrite(screen_pin_D, LOW);
    case 7:
      digitalWrite(screen_pin_A, HIGH);
      digitalWrite(screen_pin_B, HIGH);
      digitalWrite(screen_pin_C, HIGH);
      digitalWrite(screen_pin_D, LOW);
    case 8:
      digitalWrite(screen_pin_A, LOW);
      digitalWrite(screen_pin_B, LOW);
      digitalWrite(screen_pin_C, LOW);
      digitalWrite(screen_pin_D, HIGH);
    case 9:
      digitalWrite(screen_pin_A, HIGH);
      digitalWrite(screen_pin_B, LOW);
      digitalWrite(screen_pin_C, LOW);
      digitalWrite(screen_pin_D, HIGH);
    case default:
      digitalWrite(screen_pin_A, LOW);
      digitalWrite(screen_pin_B, LOW);
      digitalWrite(screen_pin_C, LOW);
      digitalWrite(screen_pin_D, LOW);
  }
}

int read_pot(){
  return analogRead(distance_to_stop_pot);
}

int read_light_sensor(){
  return analogRead(light_sensor);
}

void activate_light(){
  digitalWrite(headlight_led, HIGH);
}

void deactivate_light(){
  digitalWrite(headlight_led, LOW);
}


