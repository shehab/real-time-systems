#include <string.h>
#include <stdio.h>

#define MILLIS_IN_A_SECOND   1000

/*
 * Constants
 */
// Acceleration in m/s
#define ACCELERATION_SLOPE  0.25f
#define ACCELERATION_GAS     0.5f
#define ACCELERATION_BRAKE  -0.5f

 //Speed values
#define MAX_SPEED              70
#define MIN_SPEED              40

//Slope values
#define SLOPE_DOWN             -1
#define SLOPE_FLAT              0
#define SLOPE_UP                1

//Binary values for events
#define GAS_ENABLED             1
#define GAS_DISABLED            0
#define BRAKE_ENABLED           1
#define BRAKE_DISABLED          0
#define MIXING_ENABLED          1
#define MIXING_DISABLED         0
#define LAMP_ENABLED            1
#define LAMP_DISABLED           0

// Pins
#define PIN_LED_ACCELERATING   13
#define PIN_LED_BRAKING        12
#define PIN_LED_MIXER          11
#define PIN_LED_SPEED          10
#define PIN_LED_LAMP            7

#define PIN_SLOPE_UP            9
#define PIN_SLOPE_DOWN          8

#define PIN_LDR                A0

//Main and secondadary cycles.
//Decide the secondary and primary cycle.
#define MAIN_CYCLE           200
#define SECONDARY_CYCLE      100
#define NUMBER_SCYCLES         2


unsigned long long last_speed_timestamp    = 0; //Time in milliseconds
unsigned long long current_speed_timestamp = 0;
unsigned long long last_timestamp          = 0;
unsigned long long current_timestamp       = 0;
         long long elapsed_time            = 0;


float speed                           = 55.0f;
int speed_led                         = 0;
int slope                             = 0;

byte gas_active                       = 0;
byte brake_active                     = 0;
byte mixer_active                     = 0;
byte lamp_active                      = 0;

byte secondary_cycle                  = 0;
float acceleration                    = 0;
byte ldr_value                        = 0;


int iteration                         = 1;
int max_communication                 = 0;
int max_input                         = 0;
int max_output                        = 0;
int max_speed                         = 0;
int res                               = 0;
int before                            = 0;
int after                             = 0;


void update_speed() {
    current_speed_timestamp    = millis();
    acceleration         = (ACCELERATION_SLOPE * slope) + (ACCELERATION_GAS * gas_active) + (ACCELERATION_BRAKE * brake_active);
    speed                = acceleration * (current_timestamp - last_speed_timestamp) / (double) MILLIS_IN_A_SECOND + speed; //a*t + Vo
    if(speed < 0) speed = 0;

    last_speed_timestamp = current_speed_timestamp;

    speed_led = map(speed, MIN_SPEED, MAX_SPEED, 0, 255);

    if (speed < MIN_SPEED || speed > MAX_SPEED) {
        speed_led = LOW;
    }

}


int comm_server() {
    int  i;
    char request[10];
    char answer[10];
    int  speed_int;
    int  speed_dec;

    // while there is enough data for a request
    if (Serial.available() >= 9) {
        // read the request
        i = 0;
        while ( (i<9) && (Serial.available() >= (9-i)) ) {
            // read one character
            request[i] = Serial.read();

            // if the new line is positioned wrong
            if ( (i != 8) && (request[i] == '\n') ) {
                // Send error and start all over again

                sprintf(answer,"MSG: ERR\n");
                Serial.print(answer);
                memset(request,'\0', 9);
                i=0;
            } else {
              // read the next character
              i++;
            }
        }
        //Serial.print("Available ("); Serial.print(Serial.available()); Serial.print(")\n");
        request[9]='\0';

        sprintf(answer,"MSG: ERR\n"); //Default value
        if (strcmp("SPD: REQ\n",request) == 0) {

            // send the answer for speed request
            speed_int = (int)speed;
            speed_dec = ((int)(speed*10)) % 10;
            sprintf(answer,"SPD:%02d.%d\n",speed_int,speed_dec);

        } else if (strcmp("SLP: REQ\n",request) == 0) {

            if(slope == SLOPE_UP)   sprintf(answer, "SLP:  UP\n");
            if(slope == SLOPE_FLAT) sprintf(answer, "SLP:FLAT\n");
            if(slope == SLOPE_DOWN) sprintf(answer, "SLP:DOWN\n");

        } else if (strcmp("GAS: SET\n", request) == 0) {

            //Check for the brake?
            gas_active = GAS_ENABLED;
            sprintf(answer, "GAS:  OK\n");

        } else if (strcmp(request, "GAS: CLR\n") == 0) {

            gas_active = GAS_DISABLED;
            sprintf(answer, "GAS:  OK\n");

        } else if (strcmp(request, "BRK: SET\n") == 0) {

            brake_active = BRAKE_ENABLED;
            sprintf(answer, "BRK:  OK\n");

        } else if (strcmp(request, "BRK: CLR\n") == 0) {

            brake_active = BRAKE_DISABLED;
            sprintf(answer, "BRK:  OK\n");

        } else if (strcmp(request, "MIX: SET\n") == 0) {

            mixer_active = MIXING_ENABLED;
            sprintf(answer, "MIX:  OK\n");

        } else if (strcmp(request, "MIX: CLR\n") == 0) {

            mixer_active = MIXING_DISABLED;
            sprintf(answer, "MIX:  OK\n");

        } else if (strcmp(request, "LIT: REQ\n") == 0) {

            sprintf(answer, "LIT: %2i%%\n", ldr_value);

        } else if (strcmp(request, "LAM: SET\n") == 0) {

            lamp_active = LAMP_ENABLED;
            sprintf(answer, "LIT:  OK\n");

        } else if (strcmp(request, "LAM: CLR\n") == 0) {

            lamp_active = LAMP_DISABLED;
            sprintf(answer, "LIT:  OK\n");

        }

        Serial.print(answer);
    }

    return 0;
}


void manage_input() {
    if(digitalRead(PIN_SLOPE_UP)   == HIGH && digitalRead(PIN_SLOPE_DOWN) == LOW ) slope = SLOPE_UP;
    if(digitalRead(PIN_SLOPE_UP)   == LOW  && digitalRead(PIN_SLOPE_DOWN) == LOW ) slope = SLOPE_FLAT;
    if(digitalRead(PIN_SLOPE_DOWN) == HIGH && digitalRead(PIN_SLOPE_UP)   == LOW ) slope = SLOPE_DOWN;

    ldr_value = map(analogRead(PIN_LDR), 0, 1023, 0, 99);
}

void manage_output() {
    digitalWrite(PIN_LED_ACCELERATING, gas_active);
    digitalWrite(PIN_LED_BRAKING,      brake_active);
    digitalWrite(PIN_LED_MIXER,        mixer_active);
    digitalWrite(PIN_LED_LAMP,         lamp_active);
    analogWrite(PIN_LED_SPEED,         speed_led);
}


void setup() {
    Serial.begin(9600);
    pinMode(PIN_LED_ACCELERATING, OUTPUT);
    pinMode(PIN_LED_BRAKING,      OUTPUT);
    pinMode(PIN_LED_MIXER,        OUTPUT);
    pinMode(PIN_LED_SPEED,        OUTPUT);
    pinMode(PIN_LED_LAMP,         OUTPUT);

    pinMode(PIN_SLOPE_UP,         INPUT);
    pinMode(PIN_SLOPE_DOWN,       INPUT);
    pinMode(PIN_LDR,              INPUT);

    //milis(): Returns the number of milliseconds since the Arduino board began running the current program.
    last_speed_timestamp = millis();
    last_timestamp       = millis();
}


void loop() {
    switch (secondary_cycle) {
        case 0: {
            Serial.print("iteration");
            before = micros();
            comm_server();
            after = micros();
            res= (after - before);
            if (res > max_communication) max_communication = res;
            Serial.print("max_communication");
            Serial.println(max_communication);

            before = micros();
            manage_output();
            after = micros();
            res= (after - before);
            if (res > max_output) max_output = res;
            Serial.print("max_output");
            Serial.println(max_output);

            before = micros();
            manage_input();
            after = micros();
            res= (after - before);
            if (res > max_input) max_input = res;
            Serial.print("max_input");
            Serial.println(max_input);

            before = micros();
            update_speed();
            after = micros();
            res= (after - before);
            if (res > max_speed) max_speed = res;
            Serial.print("max_speed");
            Serial.println(max_speed);
        }
        case 1: {
            manage_output();
            manage_input();
            update_speed();
            break;
        }
    }

    //COMPLETE CYCLE
    delay(1);

    secondary_cycle    = (secondary_cycle + 1) % (MAIN_CYCLE / SECONDARY_CYCLE);
    current_timestamp  = millis();
    elapsed_time       = current_timestamp - last_timestamp; //T = SC - (max_time_int -t1 + t2)

    if(elapsed_time > SECONDARY_CYCLE) {
        //ERROR
        Serial.print("The maximum duration of the secondary cycle has been exeeded!\n");
    }

    //Chech that the amount of milis to sleep is smaller than 10
    delay(SECONDARY_CYCLE - elapsed_time);

    //Either 1 or 2, can we trust our secondary cycle?
    last_timestamp += SECONDARY_CYCLE;//1

}
