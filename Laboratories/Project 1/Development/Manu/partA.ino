#include <string.h>
#include <stdio.h>

#define GAS_PIN   13
#define BREAK_PIN 12
#define MIXER_PIN 11
#define SPEED_PIN 10
#define SLOPE_UP 9
#define SLOPE_DOWN  8
/*
1. Calculate periods
2. Implement loop function, according to periods
3. Complete comm server
4. Check speed method
*/
//Global Variables
int current_time = 0;
int initial_time = 0;

//Binary variables
int gas_pedal    = 0;
int break_pedal  = 0;
int mixer        = 0;
int slope        = 0;
//int speed        = 0;

//--------------------------------------
float speed =55.0;

// --------------------------------------
// Function: comm_server
// --------------------------------------
int comm_server()
{
    int i;
    char request[10];
    char answer[10];
    int speed_int;
    int speed_dec;

    // while there is enough data for a request
    if (Serial.available() >= 9) {
        // read the request
        i=0;
        while ( (i<9) && (Serial.available() >= (9-i)) ) {
            // read one character
            request[i]=Serial.read();

            // if the new line is positioned wrong
            if ( (i!=8) && (request[i]=='\n') ) {
                // Send error and start all over again
                sprintf(answer,"MSG: ERR\n");
                Serial.print(answer);
                memset(request,'\0', 9);
                i=0;
            } else {
              // read the next character
              i++;
            }
        }
        request[9]='\0';

        // cast the request
        if (0 == strcmp("SPD: REQ\n",request)) {
            // send the answer for speed request
            speed_int = (int)speed;
            speed_dec = ((int)(speed*10)) % 10;
            sprintf(answer,"SPD:%02d.%d\n",speed_int,speed_dec);
            Serial.print(answer);

        } else if(0 == strcmp("GAS: SET\n",request)) {

            //TO COMPLETE
            sprintf(answer,"SPD:%02d.%d\n",speed_int,speed_dec);
            Serial.print(answer);

        } else if(0 == strcmp("GAS: CLR\n",request)) {
           //TO COMPLETE
            sprintf(answer,"SPD:%02d.%d\n",speed_int,speed_dec);
            Serial.print(answer);

        } else if(0 == strcmp("BRK: SET\n",request)) {
           //TO COMPLETE
            sprintf(answer,"SPD:%02d.%d\n",speed_int,speed_dec);
            Serial.print(answer);

        } else if(0 == strcmp("BRK: CLR\n",request)) {
           //TO COMPLETE
            sprintf(answer,"SPD:%02d.%d\n",speed_int,speed_dec);
            Serial.print(answer);

        } else if(0 == strcmp("MIX: SET\n",request)) {
           //TO COMPLETE
            sprintf(answer,"SPD:%02d.%d\n",speed_int,speed_dec);
            Serial.print(answer);

        } else if(0 == strcmp("MIX: CLR\n",request)) {
           //TO COMPLETE
            sprintf(answer,"SPD:%02d.%d\n",speed_int,speed_dec);
            Serial.print(answer);

        } else if(0 == strcmp("SLP: REQ\n",request)) {

           //TO COMPLETE
            sprintf(answer,"SPD:%02d.%d\n",speed_int,speed_dec);
            Serial.print(answer);

        } else {
            // error, send error message
            sprintf(answer,"MSG: ERR\n");
            Serial.print(answer);
        }
    }
    return 0;
}

int calculate_slope() {
        //ascending slope
    if(digitalRead(SLOPE_UP) == HIGH && digitalRead(SLOPE_DOWN) == LOW ){
        slope = 1;
    }

    if(digitalRead(SLOPE_DOWN) == HIGH && digitalRead(SLOPE_UP)==LOW ){
        //Descending slope
        slope = -1;
    }

    if(digitalRead(SLOPE_UP) == LOW && digitalRead(SLOPE_DOWN) == LOW ){
        //No slope
        slope = 0;
    }

    return 0;
}

int calculate_speed() {

    int new_speed = speed + (0,25 * slope + 0,5 * gas_pedal - 0,5 * break_pedal) * (current_time - initial_time);

    speed = newspeed;

    int speed_led = map(new_speed, 45, 70, 0, 255);

    if (speed < 40 || speed > 70) {
        speed_led = LOW;
    }

    analogWrite(SPEED_PIN, speed_led);

    return 0;
 }

int transmit_acceleration() {
    digitalWrite(GAS_PIN, gas_pedal);
    return 0;
}

int transmit_break() {
    digitalWrite(BREAK_PIN, break_pedal);
    return 0;
}

int transmit_mixer() {
    digitalWrite(MIXER_PIN, mixer);
    return 0;
}
// --------------------------------------
// Function: setup
// --------------------------------------
void setup() {
    Serial.begin(9600);
    pinMode(GAS_PIN,OUTPUT);
    pinMode(BREAK_PIN ,OUTPUT);
    pinMode(MIXER_PIN ,OUTPUT);
    pinMode(SPEED_PIN ,OUTPUT);
    pinMode(SLOPE_INIT,INPUT);
    pinMode(SLOPE_END ,INPUT);
    //milis(): Returns the number of milliseconds since the Arduino board began running the current program.
    initial_time = millis();
}

// --------------------------------------
// Function: loop
// --------------------------------------
void loop() {
    current_time = millis();
    comm_server();

}