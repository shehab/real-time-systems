#include <string.h>
#include <stdio.h>
/*
 * Constants
 */
// Acceleration in m/s
#define ACCELERATION_SLOPE  0.25f
#define ACCELERATION_GAS     0.5f
#define ACCELERATION_BRAKE  -0.5f
 //Speed values
#define MAX_SPEED              70
#define MIN_SPEED              40
//Slope values
#define SLOPE_DOWN             -1
#define SLOPE_FLAT              0
#define SLOPE_UP                1
//Binary values for events
#define GAS_ENABLED             1
#define GAS_DISABLED            0
#define BRAKE_ENABLED           1
#define BRAKE_DISABLED          0
#define MIXING_ENABLED          1
#define MIXING_DISABLED         0
// Pins
#define PIN_LED_ACCELERATING   13
#define PIN_LED_BRAKING        12
#define PIN_LED_MIXER          11
#define PIN_LED_SPEED          10
#define PIN_SLOPE_UP            9
#define PIN_SLOPE_DOWN          8
 //Main and secondadary cycles.
 //Decide the secondary and primary cycle.
 #define MAIN_CYCLE           200
 #define SECONDARY_CYCLE      100
 #define NUMBER_SCYCLES         2


unsigned long last_speed_timestamp    = 0; //Time in milliseconds
unsigned long current_speed_timestamp = 0;
unsigned long last_timestamp          = 0;
unsigned long current_timestamp       = 0;
float speed                           = 55.0f;
byte slope                            = 0;
byte gas_active                       = 0;
byte brake_active                     = 0;
byte mix_active                       = 0;
int elapsed_time                      = 0;
int secondary_cycle                   = 0;


void update_speed() {
    current_speed_timestamp    = millis();
    acceleration         = (ACCELERATION_SLOPE * slope) + (ACCELERATION_GAS * gas_active) + (ACCELERATION_BRAKE * brake_active);
    speed                = acceleration * (current_timestamp - last_speed_timestamp) + speed; //a*t + Vo

    last_speed_timestamp = current_speed_timestamp;

    int speed_led = map(speed, MIN_SPEED, MAX_SPEED, 0, 255);

    if (speed < MIN_SPEED || speed > MAX_SPEED) {
        speed_led = LOW;
    }

    analogWrite(PIN_LED_SPEED, speed_led);

    return 0;
}


int comm_server() {
    int  i;
    char request[10];
    char answer[10];
    int  speed_int;
    int  speed_dec;

    // while there is enough data for a request
    if (Serial.available() >= 9) {
        // read the request
        i = 0;
        while ( (i<9) && (Serial.available() >= (9-i)) ) {
            // read one character
            request[i] = Serial.read();

            // if the new line is positioned wrong
            if ( (i != 8) && (request[i] == '\n') ) {
                // Send error and start all over again
                sprintf(answer,"MSG: ERR\n");
                Serial.print(answer);
                memset(request,'\0', 9);
                i=0;
            } else {
              // read the next character
              i++;
          }
      }
      request[9]='\0';

        sprintf(answer,"MSG: ERR\n"); //Default value

        if (strcmp("SPD: REQ\n",request) == 0) {

            // send the answer for speed request
            speed_int = (int)speed;
            speed_dec = ((int)(speed*10)) % 10;
            sprintf(answer,"SPD:%02d.%d\n",speed_int,speed_dec);

        } else if (strcmp("SLP: REQ\n",request) == 0) {

            if(slope == SLOPE_UP)   sprintf(answer, "SLP:  UP\n");
            if(slope == SLOPE_FLAT) sprintf(answer, "SLP:FLAT\n");
            if(slope == SLOPE_DOWN) sprintf(answer, "SLP:DOWN\n");

        } else if (strcmp(request, "GAS: SET\n") == 0) {

            //Check for the brake?
            gas_active = GAS_ENABLED;
            sprintf(answer, "GAS:  OK\n");

        } else if (strcmp(request, "GAS: CLR\n") == 0) {

            gas_active = GAS_DISABLED;
            sprintf(answer, "GAS:  OK\n");

        } else if (strcmp(request, "BRK: SET\n") == 0) {

            brake_active = BRAKE_ENABLED;
            sprintf(answer, "BRK:  OK\n");

        } else if (strcmp(request, "BRK: CLR\n") == 0) {

            brake_active = BRAKE_DISABLED;
            sprintf(answer, "BRK:  OK\n");

        } else if (strcmp(request, "MIX: SET\n") == 0) {

            mix_active = MIXING_ENABLED;
            sprintf(answer, "MIX:  OK\n");

        } else if (strcmp(request, "MIX: CLR\n") == 0) {

            mix_active = MIXING_DISABLED;
            sprintf(answer, "MIX:  OK\n");

        }

        Serial.print(answer);
    }

    return 0;
}


void manage_input() {
    if(digitalRead(SLOPE_UP)   == HIGH && digitalRead(SLOPE_DOWN) == LOW ) slope = SLOPE_UP;
    if(digitalRead(SLOPE_UP)   == LOW  && digitalRead(SLOPE_DOWN) == LOW ) slope = SLOPE_FLAT;
    if(digitalRead(SLOPE_DOWN) == HIGH && digitalRead(SLOPE_UP)   == LOW ) slope = SLOPE_DOWN;
}

void manage_output() {
    digitalWrite(PIN_LED_ACCELERATING, gas_pedal);
    digitalWrite(PIN_LED_BRAKING, break_pedal);
    digitalWrite(PIN_LED_MIXER, mixer);
}


void setup() {
    Serial.begin(9600);
    pinMode(PIN_LED_ACCELERATING, OUTPUT);
    pinMode(PIN_LED_BRAKING ,     OUTPUT);
    pinMode(PIN_LED_MIXER ,       OUTPUT);
    pinMode(PIN_LED_SPEED ,       OUTPUT);
    pinMode(SLOPE_UP,             INPUT);
    pinMode(SLOPE_DOWN ,          INPUT);
    //milis(): Returns the number of milliseconds since the Arduino board began running the current program.
    last_speed_timestamp = millis();
}


void loop() {
    switch (secondary_cycle) {
        case 0: {
            comm_server();
            manage_output();
            manage_input();
            update_speed();
            break;
        }
        case 1: {
            manage_output();
            manage_input();
            update_speed();
            break;
        }
    }

    //COMPLETE CYCLE
    secondary_cycle    = (secondary_cycle + 1) % (MAIN_CYCLE / SECONDARY_CYCLE);

    current_timestamp  = millis();
    //T = SC - (max_time_int -t1 + t2)
    elpased_time       = current_timestamp - last_timestamp;
    //ERROR CASE: TAKE CARE
    if(elapsed_time > SECONDARY_CYCLE) {
        //ERROR
        Serial.print("The maximum duration of the secondary cycle has been exeeded!\n");
        //while(1);
    }
    //Chech that the amount of milis to sleep is smaller than 10
    delay(SECONDARY_CYCLE - elapsed_time);

    //Either 1 or 2, can we trust our secondary cycle?
    last_timestamp += SECONDARY_CYCLE;//1
    //last_speed_timestamp  = millis();//2

}


