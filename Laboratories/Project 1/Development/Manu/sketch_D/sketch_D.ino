#include <string.h>
#include <stdio.h>
#include <stdint.h>

#define MILLIS_IN_A_SECOND   1000

/*
 * Constants
 */

#define STATE_SET_DISTANCE     0
#define STATE_MOVING           1

#define STILL                   0
#define MOVING                  1
// Acceleration in m/s
#define ACCELERATION_SLOPE  0.25f
#define ACCELERATION_GAS     0.5f
#define ACCELERATION_BRAKE  -0.5f

 //Speed values
#define MAX_SPEED              70
#define MIN_SPEED              40
#define STOPPING_THRESHOLD     10

//Slope values
#define SLOPE_DOWN             -1
#define SLOPE_FLAT              0
#define SLOPE_UP                1

//Binary values for events
#define GAS_ENABLED             1
#define GAS_DISABLED            0
#define BRAKE_ENABLED           1
#define BRAKE_DISABLED          0
#define MIXING_ENABLED          1
#define MIXING_DISABLED         0
#define LAMP_ENABLED            1
#define LAMP_DISABLED           0

// Pins
#define PIN_LED_ACCELERATING   13
#define PIN_LED_BRAKING        12
#define PIN_LED_MIXER          11
#define PIN_LED_SPEED          10
#define PIN_LED_LAMP            7

#define PIN_SLOPE_UP            9
#define PIN_SLOPE_DOWN          8

#define PIN_LDR                A0

#define PIN_POTENTIOMETER      A1
#define PIN_SCREEN_A            2
#define PIN_SCREEN_B            3
#define PIN_SCREEN_C            4
#define PIN_SCREEN_D            5
#define PIN_BUTTON              6

//Main and secondadary cycles.
//Decide the secondary and primary cycle.
#define MAIN_CYCLE           200
#define SECONDARY_CYCLE      100
#define NUMBER_SCYCLES         2

//#define NORMAL_MODE
#define DISTANCE_SELECTION_MODE 1
#define APPROACH_MODE           2
#define STOP_MODE               3
#define EMERGENCY_MODE          4

#define HAS_BEEN_PRESSED        1
#define HAS_NOT_BEEN_PRESSED    0


unsigned long last_speed_timestamp    = 0; //Time in milliseconds
unsigned long current_speed_timestamp = 0;
unsigned long last_timestamp          = 0;
unsigned long current_timestamp       = 0;

//double speed                          = 55.0f;
double speed                          = 9;

int speed_led                         = 0;
int slope                             = 0;

byte gas_active                       = 0;
byte brake_active                     = 0;
byte mixer_active                     = 0;
byte lamp_active                      = 0;
byte is_moving                        = MOVING;
byte execution_mode                   = 1;

float elapsed_time                    = 0;
byte secondary_cycle                  = 0;
float acceleration                    = 0;
byte ldr_value                        = 0;
byte digit_screen                     = 0;
int16_t remaining_distance            = 0;
byte state_distance                   = 0;
double delta_t                        = 0;

int button_pressed                    = 0;
byte released                         = 1;



void update_speed() {
    current_speed_timestamp    = millis();
    delta_t                    = (current_timestamp - last_speed_timestamp);
    last_speed_timestamp       = current_speed_timestamp;
    acceleration               = (ACCELERATION_SLOPE * slope) + (ACCELERATION_GAS * gas_active) + (ACCELERATION_BRAKE * brake_active);


    speed = acceleration * delta_t + speed; //a*t + Vo

    if(execution_mode == APPROACH_MODE && remaining_distance > 0) {
        // x = a/2*t^2 + Vo*t + x0
        remaining_distance = remaining_distance - (acceleration/2 * delta_t*delta_t + speed * delta_t);
        if(remaining_distance < 0) remaining_distance = 0;
        if(remaining_distance == 0 && speed < STOPPING_THRESHOLD) {
            speed = 0;
            is_moving = STILL;
            execution_mode = STOP_MODE;
        }
        //The braking process ends when the distance to the deposit is 0 and the speed is less than 10 m/s (Otherwise the download won’t happen).
        else if (remaining_distance == 0 && speed >= STOPPING_THRESHOLD) {
            execution_mode = DISTANCE_SELECTION_MODE;
        }
        Serial.println("New remaining distance:");
        Serial.println(remaining_distance);
        Serial.println("New Speed");
        Serial.println(speed);
        Serial.println();
    }



    if(speed < 0) speed = 0;
    speed_led = map(speed, MIN_SPEED, MAX_SPEED, 0, 255);

    if (speed < MIN_SPEED || speed > MAX_SPEED) {
        speed_led = LOW;
    }


    //Where does this go in this method?

}

void update_speed_stop() {
    speed = 0;
}


int comm_server() {
    int  i;
    char request[10];
    char answer[10];
    int  speed_int;
    int  speed_dec;

    // while there is enough data for a request
    if (Serial.available() >= 9) {
        // read the request
        i = 0;
        while ( (i<9) && (Serial.available() >= (9-i)) ) {
            // read one character
            request[i] = Serial.read();

            // if the new line is positioned wrong
            if ( (i != 8) && (request[i] == '\n') ) {
                // Send error and start all over again
                Serial.println("error bitch");
                sprintf(answer,"MSG: ERR\n");
                Serial.print(answer);
                memset(request,'\0', 9);
                i=0;
            } else {
              // read the next character
              i++;
          }
      }
      request[9]='\0';

        sprintf(answer,"MSG: ERR\n"); //Default value

        if (strcmp("SPD: REQ\n",request) == 0) {

            // send the answer for speed request
            speed_int = (int)speed;
            speed_dec = ((int)(speed*10)) % 10;
            sprintf(answer,"SPD:%02d.%d\n",speed_int,speed_dec);

        } else if (strcmp("SLP: REQ\n",request) == 0) {

            if(slope == SLOPE_UP)   sprintf(answer, "SLP:  UP\n");
            if(slope == SLOPE_FLAT) sprintf(answer, "SLP:FLAT\n");
            if(slope == SLOPE_DOWN) sprintf(answer, "SLP:DOWN\n");

        } else if (strcmp("GAS: SET\n", request) == 0) {

            //Check for the brake?
            gas_active = GAS_ENABLED;
            sprintf(answer, "GAS:  OK\n");

        } else if (strcmp(request, "GAS: CLR\n") == 0) {

            gas_active = GAS_DISABLED;
            sprintf(answer, "GAS:  OK\n");

        } else if (strcmp(request, "BRK: SET\n") == 0) {

            brake_active = BRAKE_ENABLED;
            sprintf(answer, "BRK:  OK\n");

        } else if (strcmp(request, "BRK: CLR\n") == 0) {

            brake_active = BRAKE_DISABLED;
            sprintf(answer, "BRK:  OK\n");

        } else if (strcmp(request, "MIX: SET\n") == 0) {

            mixer_active = MIXING_ENABLED;
            sprintf(answer, "MIX:  OK\n");

        } else if (strcmp(request, "MIX: CLR\n") == 0) {

            mixer_active = MIXING_DISABLED;
            sprintf(answer, "MIX:  OK\n");

        } else if (strcmp(request, "LIT: REQ\n") == 0) {

            sprintf(answer, "LIT: %2i%%\n", ldr_value);

        } else if (strcmp(request, "LIT: SET\n") == 0) {

            lamp_active = LAMP_ENABLED;
            sprintf(answer, "LIT:  OK\n");

        } else if (strcmp(request, "LIT: CLR\n") == 0) {

            lamp_active = LAMP_DISABLED;
            sprintf(answer, "LIT:  OK\n");

        }else if (strcmp(request, "STP: REQ\n") == 0) {

            if(is_moving == MOVING)   sprintf(answer, "STP:  GO\n");
            if(is_moving == STILL)    sprintf(answer, "STP:STOP\n");

        }else if (strcmp(request, "DS:  REQ\n") == 0) {

            sprintf(answer, "DS:%5d\n", remaining_distance);

        } else if (strcmp(request, "ERR: SET\n") == 0) {
            execution_mode = EMERGENCY_MODE;
            sprintf(answer, "ERR:  OK\n");

        }

        Serial.print(answer);
    }

    return 0;
}


void manage_input() {
    if(digitalRead(PIN_SLOPE_UP)   == HIGH && digitalRead(PIN_SLOPE_DOWN) == LOW ) slope = SLOPE_UP;
    if(digitalRead(PIN_SLOPE_UP)   == LOW  && digitalRead(PIN_SLOPE_DOWN) == LOW ) slope = SLOPE_FLAT;
    if(digitalRead(PIN_SLOPE_DOWN) == HIGH && digitalRead(PIN_SLOPE_UP)   == LOW ) slope = SLOPE_DOWN;

    ldr_value           = map(analogRead(PIN_LDR), 0, 1023, 0, 99);
    digit_screen        = map(analogRead(PIN_POTENTIOMETER), 0, 1000, 1, 10); // ASK TO DA CHEETAH
    if(digit_screen == 10) digit_screen--;
    //BUTTON - IF PRESSED, CHANGE EXECUTION MODE
    if(check_button() == HAS_BEEN_PRESSED) {
        remaining_distance = digit_screen*1000;
        execution_mode     = APPROACH_MODE;
    }

}

void manage_input_approach() {
    if(digitalRead(PIN_SLOPE_UP)   == HIGH && digitalRead(PIN_SLOPE_DOWN) == LOW ) slope = SLOPE_UP;
    if(digitalRead(PIN_SLOPE_UP)   == LOW  && digitalRead(PIN_SLOPE_DOWN) == LOW ) slope = SLOPE_FLAT;
    if(digitalRead(PIN_SLOPE_DOWN) == HIGH && digitalRead(PIN_SLOPE_UP)   == LOW ) slope = SLOPE_DOWN;
    lamp_active = HIGH;
    if(digit_screen == 10) digit_screen--;
    //BUTTON - IF PRESSED, CHANGE EXECUTION MODE


}
void manage_input_stop(){
    ldr_value           = HIGH;
}



int check_button() {
    button_pressed = digitalRead(PIN_BUTTON);

    if(released && button_pressed) { //Detect rising edge
        released = 0;
    }

    if(released == 0 && !button_pressed) {
        // Lowering edge
        released = 1;
        return HAS_BEEN_PRESSED;
    } else {
        return HAS_NOT_BEEN_PRESSED;
    }
}

void manage_output() {
    digitalWrite(PIN_LED_ACCELERATING, gas_active);
    digitalWrite(PIN_LED_BRAKING,      brake_active);
    digitalWrite(PIN_LED_MIXER,        mixer_active);
    digitalWrite(PIN_LED_LAMP,         lamp_active);
    analogWrite(PIN_LED_SPEED,         speed_led);
    //execution_mode == DISTANCE_SELECTION_MODE ? set_screen(digit_screen) : set_screen(remaining_distance);
    //clear_screen();
    set_screen(execution_mode == DISTANCE_SELECTION_MODE ? digit_screen : remaining_distance/1000);
}


void manage_output_stop() {
    digitalWrite(PIN_LED_MIXER, mixer_active);
}

void clear_screen() {
    digitalWrite(PIN_SCREEN_A, LOW);
    digitalWrite(PIN_SCREEN_B, LOW);
    digitalWrite(PIN_SCREEN_C, LOW);
    digitalWrite(PIN_SCREEN_D, LOW);
}

void set_screen(int digit) {
    switch(digit) {
        case 0:
        digitalWrite(PIN_SCREEN_A, LOW);
        digitalWrite(PIN_SCREEN_B, LOW);
        digitalWrite(PIN_SCREEN_C, LOW);
        digitalWrite(PIN_SCREEN_D, LOW);
        break;
        case 1:
        digitalWrite(PIN_SCREEN_A, HIGH);
        digitalWrite(PIN_SCREEN_B, LOW);
        digitalWrite(PIN_SCREEN_C, LOW);
        digitalWrite(PIN_SCREEN_D, LOW);
        break;
        case 2:
        digitalWrite(PIN_SCREEN_A, LOW);
        digitalWrite(PIN_SCREEN_B, HIGH);
        digitalWrite(PIN_SCREEN_C, LOW);
        digitalWrite(PIN_SCREEN_D, LOW);
        break;
        case 3:
        digitalWrite(PIN_SCREEN_A, HIGH);
        digitalWrite(PIN_SCREEN_B, HIGH);
        digitalWrite(PIN_SCREEN_C, LOW);
        digitalWrite(PIN_SCREEN_D, LOW);
        break;
        case 4:
        digitalWrite(PIN_SCREEN_A, LOW);
        digitalWrite(PIN_SCREEN_B, LOW);
        digitalWrite(PIN_SCREEN_C, HIGH);
        digitalWrite(PIN_SCREEN_D, LOW);
        break;
        case 5:
        digitalWrite(PIN_SCREEN_A, HIGH);
        digitalWrite(PIN_SCREEN_B, LOW);
        digitalWrite(PIN_SCREEN_C, HIGH);
        digitalWrite(PIN_SCREEN_D, LOW);
        break;
        case 6:
        digitalWrite(PIN_SCREEN_A, LOW);
        digitalWrite(PIN_SCREEN_B, HIGH);
        digitalWrite(PIN_SCREEN_C, HIGH);
        digitalWrite(PIN_SCREEN_D, LOW);
        break;
        case 7:
        digitalWrite(PIN_SCREEN_A, HIGH);
        digitalWrite(PIN_SCREEN_B, HIGH);
        digitalWrite(PIN_SCREEN_C, HIGH);
        digitalWrite(PIN_SCREEN_D, LOW);
        break;
        case 8:
        digitalWrite(PIN_SCREEN_A, LOW);
        digitalWrite(PIN_SCREEN_B, LOW);
        digitalWrite(PIN_SCREEN_C, LOW);
        digitalWrite(PIN_SCREEN_D, HIGH);
        break;
        case 9:
        digitalWrite(PIN_SCREEN_A, HIGH);
        digitalWrite(PIN_SCREEN_B, LOW);
        digitalWrite(PIN_SCREEN_C, LOW);
        digitalWrite(PIN_SCREEN_D, HIGH);
        break;
        default:
        clear_screen();
    }
}


void setup() {
    Serial.begin(9600);
    pinMode(PIN_LED_ACCELERATING, OUTPUT);
    pinMode(PIN_LED_BRAKING,      OUTPUT);
    pinMode(PIN_LED_MIXER,        OUTPUT);
    pinMode(PIN_LED_SPEED,        OUTPUT);
    pinMode(PIN_LED_LAMP,         OUTPUT);

    pinMode(PIN_SCREEN_A,         OUTPUT);
    pinMode(PIN_SCREEN_B,         OUTPUT);
    pinMode(PIN_SCREEN_C,         OUTPUT);
    pinMode(PIN_SCREEN_D,         OUTPUT);

    pinMode(PIN_SLOPE_UP,         INPUT);
    pinMode(PIN_SLOPE_DOWN,       INPUT);
    pinMode(PIN_LDR,              INPUT);
    pinMode(PIN_BUTTON,           INPUT);
    pinMode(PIN_POTENTIOMETER,    INPUT);


    //milis(): Returns the number of milliseconds since the Arduino board began running the current program.
    last_speed_timestamp = millis();
}

void finish_secondary_cycle (int duration) {

    delay(1);
    secondary_cycle    = (secondary_cycle + 1) % (MAIN_CYCLE / duration);
    current_timestamp  = millis();

    //T = SC - (max_time_int -t1 + t2)
    elapsed_time       = current_timestamp - last_timestamp;
    //ERROR CASE: TAKE CARE
    if(elapsed_time > duration) {
        //ERROR
        Serial.print("The maximum duration of the secondary cycle has been exeeded!\n");
    }
    //Chech that the amount of milis to sleep is smaller than 10
    delay(duration - elapsed_time);

    //Either 1 or 2, can we trust our secondary cycle?
    last_timestamp += duration;//1
}

void loop() {

    switch (execution_mode) {
        case DISTANCE_SELECTION_MODE: {
            Serial.println("DISTANCE_SELECTION_MODE");

            switch (secondary_cycle) {
                case 0: {
                    comm_server();
                    manage_output();
                    manage_input();
                    update_speed();
                    break;
                }
                case 1: {
                    manage_output();
                    manage_input();
                    update_speed();
                    break;
                }
            }
            break;

        }
        case APPROACH_MODE: {
            Serial.println("APPROACH_MODE");

            switch (secondary_cycle) {
                case 0: {
                    comm_server();
                    manage_output();
                    manage_input_approach();
                    update_speed();
                    break;
                }
                case 1: {
                    manage_output();
                    manage_input_approach();
                    update_speed();
                    break;
                }
            }
            break;
        }
        case STOP_MODE: {
            //only continue with mixer
            Serial.println("STOP MODE");
            comm_server();

            switch (secondary_cycle) {
                case 0: {
                    comm_server();
                    manage_input_stop();
                    manage_output_stop();
                    //speed = 0
                    update_speed_stop();
                    //check pushbutton to change mode
                    if(check_button() == HAS_BEEN_PRESSED) {
                        is_moving = MOVING;
                        execution_mode = DISTANCE_SELECTION_MODE;
                    }
                    break;
                }
                case 1: {
                    manage_input_stop();
                    manage_output_stop();
                    //speed = 0
                    update_speed_stop();
                    //check pushbutton to change mode
                    if(check_button() == HAS_BEEN_PRESSED) {
                        is_moving      = MOVING;
                        lamp_active    = 0;
                        execution_mode = DISTANCE_SELECTION_MODE;
                    }
                    break;
                }
            }
            break;
        }
        case EMERGENCY_MODE: {
            Serial.println("EMERGENCY_MODE");

            switch (secondary_cycle) {
                case 0: {
                    comm_server();
                    manage_output();
                    //manage_input();
                    update_speed();
                    break;
                }
                case 1: {
                    manage_output();
                    //manage_input();
                    update_speed();
                    break;
                }
            }
            break;
        }
    }
    finish_secondary_cycle(SECONDARY_CYCLE);
}
