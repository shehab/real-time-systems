/**********************************************************
 *  INCLUDES
 *********************************************************/
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <ioLib.h>
#include <math.h>
#include <time.h>
#include "displayD.h"
#include "serialcallLib.h"

/**********************************************************
 *  Constants
 **********************************************************/
#define MAJOR_CYCLE_S_WC                     30
#define MAJOR_CYCLE_S_NWC                    10
#define SECONDARY_CYCLE_S_WC                  1
#define SECONDARY_CYCLE_S_NWC                 5
#define SECONDARY_CYCLE_NS                    0
#define MESSAGE_LENGTH                       10
#define NANOSECONDS_IN_A_SECOND      1000000000
#define NANOSECONDS_IN_A_MILLISECOND    1000000
#define MILLIS_IN_A_SECOND                 1000

 // Acceleration in m/s
#define ACCELERATION_SLOPE                0.25f
#define ACCELERATION_GAS                   0.5f
#define ACCELERATION_BRAKE                -0.5f

 //Speed values
#define MAX_SPEED                          70.0
#define MIN_SPEED                          40.0
#define MEDIUM_SPEED                       55.0

//Slope values
#define SLOPE_DOWN                           -1
#define SLOPE_FLAT                            0
#define SLOPE_UP                              1

//Binary values for events
#define GAS_ENABLED                           1
#define GAS_DISABLED                          0
#define BRAKE_ENABLED                         1
#define BRAKE_DISABLED                        0
#define MIXING_ENABLED                        1
#define MIXING_DISABLED                       0
#define LAMP_ENABLED                          1
#define LAMP_DISABLED                         0
#define LAMP_ENABLED                          1
#define LAMP_DISABLED                         0

#define LIGHT_THRESHOLD                      50

#define DISTANCE_SELECTION_MODE               1
#define APPROACH_MODE                         2
#define STOP_MODE                             3
#define ERROR_MODE                            4

#define MOVING                                0
#define STILL                                 1

#define DSM_DISTANCE                          -1
#define TARGET_SPEED                          10
#define OSCILLATING_SPEED                     2.5
//#define SIMULATOR

#define WORDING_COMPLIENT

/**********************************************************
 *  Global Variables
 *********************************************************/
double speed                          = 55.0;
char gas                              = 0;
char brake                            = 0;
char mixer                            = 0;
char time_mixer_called                = 0;

char request[MESSAGE_LENGTH];
char answer[MESSAGE_LENGTH];
char lamp                             = 0;
char light_sensor;

char execution_mode                   = 1;
char is_stopped                       = MOVING;
int distance_brake                    = 0;
int remaining_distance                = 99999;
struct timespec current_timestamp, last_timestamp, time_to_sleep, elapsed_time, secondary_cycle,
       last_mixer_timestamp, current_mixer_timestamp, elapsed_time_mixer, mixer_cycle;

char n_secondary_cycle                = 0;


void diffTime(struct timespec end, struct timespec start, struct timespec *diff) {
    if (end.tv_nsec < start.tv_nsec) {
        diff->tv_nsec = NANOSECONDS_IN_A_SECOND - start.tv_nsec + end.tv_nsec;
        diff->tv_sec = end.tv_sec - (start.tv_sec+1);
    } else {
        diff->tv_nsec = end.tv_nsec - start.tv_nsec;
        diff->tv_sec = end.tv_sec - start.tv_sec;
    }
}


void addTime(struct timespec end, struct timespec start, struct timespec *add)
{
    unsigned long aux;
    aux = start.tv_nsec + end.tv_nsec;
    add->tv_sec = start.tv_sec + end.tv_sec +
                  (aux / NANOSECONDS_IN_A_SECOND);
    add->tv_nsec = aux % NANOSECONDS_IN_A_SECOND;
}


int compTime(struct timespec t1, struct timespec t2)
{
    if (t1.tv_sec == t2.tv_sec) {
        if (t1.tv_nsec == t2.tv_nsec) {
            return (0);
        } else if (t1.tv_nsec > t2.tv_nsec) {
            return (1);
        } else if (t1.tv_nsec < t2.tv_nsec) {
            return (-1);
        }
    } else if (t1.tv_sec > t2.tv_sec) {
        return (1);
    } else if (t1.tv_sec < t2.tv_sec) {
        return (-1);
    }
    return (0);
}
/**********************************************************
 *  Function: task_speed
 *********************************************************/
int task_speed()
{

    //--------------------------------
    //  request speed and display it
    //--------------------------------

    //clear request and answer
    memset(request,'\0',MESSAGE_LENGTH);
    memset(answer,'\0',MESSAGE_LENGTH);

    // request speed
    strcpy(request,"SPD: REQ\n");

    //uncomment to use the simulator
#ifdef SIMULATOR
    simulator(request, answer);
#endif
    // uncoment to access serial module
#ifndef SIMULATOR
   writeSerialMod_9(request);
   readSerialMod_9(answer);
#endif

    // display speed
    if (1 == sscanf (answer,"SPD:%lf\n",&speed)) {

        displaySpeed(speed);

        switch(execution_mode) {
            case DISTANCE_SELECTION_MODE: {
                if (speed >= MEDIUM_SPEED) {
                    gas     = GAS_DISABLED;
                    brake   = BRAKE_ENABLED;
                }
                if (speed < MEDIUM_SPEED) {
                    gas     = GAS_ENABLED;
                    brake   = BRAKE_DISABLED;
                }
                break;
            }
            case APPROACH_MODE: {
                if (speed >= OSCILLATING_SPEED){
                    gas     = GAS_DISABLED;
                    brake   = BRAKE_ENABLED;
                } else {
                    gas     = GAS_ENABLED;
                    brake   = BRAKE_DISABLED;
                }
                break;
            }
            case ERROR_MODE: {
                gas         = GAS_DISABLED;
                brake       = BRAKE_ENABLED;
            }
        }
    }
    return 0;
}

/**********************************************************
 *  Function: task_slope
 *********************************************************/
int task_slope()
{
    // char request[MESSAGE_LENGTH];
    // char answer[MESSAGE_LENGTH];

    //--------------------------------
    //  request slope and display it
    //--------------------------------

    //clear request and answer
    memset(request,'\0',MESSAGE_LENGTH);
    memset(answer,'\0',MESSAGE_LENGTH);

    // request slope
    strcpy(request,"SLP: REQ\n");

    //uncomment to use the simulator
#ifdef SIMULATOR
    simulator(request, answer);
#endif
    // uncoment to access serial module

#ifndef SIMULATOR
   writeSerialMod_9(request);
   readSerialMod_9(answer);
#endif

    // display slope
    if (0 == strcmp(answer,"SLP:DOWN\n")) displaySlope(SLOPE_DOWN);
    if (0 == strcmp(answer,"SLP:FLAT\n")) displaySlope(SLOPE_FLAT);
    if (0 == strcmp(answer,"SLP:  UP\n")) displaySlope(SLOPE_UP);

    return 0;
}

/**********************************************************
 *  Function: task_gas
 *********************************************************/
int task_gas()
{
    // char request[MESSAGE_LENGTH];
    // char answer[MESSAGE_LENGTH];

    //--------------------------------
    //  request slope and display it
    //--------------------------------

    //clear request and answer
    memset(request,'\0',MESSAGE_LENGTH);
    memset(answer,'\0',MESSAGE_LENGTH);

    // request gas
    if (execution_mode != ERROR_MODE) {
        gas == GAS_ENABLED ? strcpy(request, "GAS: SET\n") : strcpy(request, "GAS: CLR\n");
    } else strcpy(request, "GAS: CLR\n");
    //uncomment to use the simulator
#ifdef SIMULATOR
    simulator(request, answer);
#endif
    // uncoment to access serial module
#ifndef SIMULATOR
   writeSerialMod_9(request);
   readSerialMod_9(answer);
#endif

    //Show answer
    if (0 == strcmp(answer, "GAS:  OK\n")) {
        displayGas(gas);
    } else {
        execution_mode = ERROR_MODE;
    }
    return 0;
}

/**********************************************************
 *  Function: task_brake
 *********************************************************/
int task_brake()
{
    // char request[MESSAGE_LENGTH];
    // char answer[MESSAGE_LENGTH];

    //--------------------------------
    //  request slope and display it
    //--------------------------------

    //clear request and answer
    memset(request,'\0',MESSAGE_LENGTH);
    memset(answer,'\0',MESSAGE_LENGTH);

    // request brake
    if (execution_mode != ERROR_MODE) {
        brake == BRAKE_ENABLED ? strcpy(request, "BRK: SET\n") : strcpy(request, "BRK: CLR\n");
    } else strcpy(request, "BRK: SET\n");    //uncomment to use the simulator
#ifdef SIMULATOR
    simulator(request, answer);
#endif
    // uncoment to access serial module
#ifndef SIMULATOR
   writeSerialMod_9(request);
   readSerialMod_9(answer);
#endif

    //Show answer
    if (0 == strcmp(answer, "BRK:  OK\n")) {
        displayBrake(brake);
    } else {
        execution_mode = ERROR_MODE;
    }
    return 0;
}

/**********************************************************
 *  Function: task_mixer
 *********************************************************/
int task_mixer()
{
    clock_gettime(CLOCK_REALTIME, &current_mixer_timestamp);
    diffTime(current_mixer_timestamp, last_mixer_timestamp, &elapsed_time_mixer);
    if(compTime(mixer_cycle, elapsed_time_mixer) == -1){
        mixer = 1 - mixer;

        memset(request,'\0',MESSAGE_LENGTH);
        memset(answer,'\0',MESSAGE_LENGTH);

        clock_gettime(CLOCK_REALTIME, &last_mixer_timestamp);
        mixer == MIXING_ENABLED ? strcpy(request, "MIX: SET\n") : strcpy(request, "MIX: CLR\n");

#ifdef SIMULATOR
        simulator(request, answer);
#endif

#ifndef SIMULATOR
        writeSerialMod_9(request);
        readSerialMod_9(answer);
#endif

        //Show answer
        if (0 == strcmp(answer, "MIX:  OK\n")) {
            displayMix(mixer);
        } else {
            execution_mode = ERROR_MODE;
        }
    }

    return 0;
}


int task_light_sensor()
{

    //--------------------------------
    //  request speed and display it
    //--------------------------------

    //clear request and answer
    memset(request,'\0',MESSAGE_LENGTH);
    memset(answer,'\0',MESSAGE_LENGTH);

    // request speed
    strcpy(request,"LIT: REQ\n");

        //uncomment to use the simulator
#ifdef SIMULATOR
        simulator(request, answer);
#endif
        //Check #endifwhat amount of time has passed, and consider the previous state of the mixer

        // uncoment to access serial module
#ifndef SIMULATOR
       writeSerialMod_9(request);
       readSerialMod_9(answer);
#endif
    if (1 == sscanf (answer,"LIT: %d%\n",&light_sensor)) {
        lamp = light_sensor >= LIGHT_THRESHOLD ? LAMP_DISABLED : LAMP_ENABLED;
        displayLightSensor(lamp);
        //speed < 55.0 ? (brake = 0) & (gas = 1) : (brake = 1) & (gas = 0);
    }else {
        execution_mode = ERROR_MODE;
    }

    // display speed
    return 0;
}

int task_lamp()
{   //clear request and answer
    memset(request,'\0',MESSAGE_LENGTH);
    memset(answer,'\0',MESSAGE_LENGTH);

    switch(execution_mode) {
        case DISTANCE_SELECTION_MODE: {
            lamp == LAMP_ENABLED ? strcpy(request, "LAM: SET\n") : strcpy(request, "LAM: CLR\n");
            break;
        }
        case APPROACH_MODE:
        case ERROR_MODE:
        case STOP_MODE:{
            strcpy(request, "LAM: SET\n");
            break;
        }
    }

#ifdef SIMULATOR
        simulator(request, answer);
#endif

#ifndef SIMULATOR
       writeSerialMod_9(request);
       readSerialMod_9(answer);
#endif
    if (0 == strcmp(answer, "LAM:  OK\n")) {
        displayLamps(lamp);
    } else {
        execution_mode = ERROR_MODE;
    }

    // display speed
    return 0;
}

int task_stop()
{
    //clear request and answer
    memset(request,'\0',MESSAGE_LENGTH);
    memset(answer,'\0',MESSAGE_LENGTH);

    strcpy(request, "STP: REQ\n");

    //uncomment to use the simulator
#ifdef SIMULATOR
    simulator(request, answer);
#endif
    //Check #endifwhat amount of time has passed, and consider the previous state of the mixer

    // uncoment to access serial module
#ifndef SIMULATOR
   writeSerialMod_9(request);
   readSerialMod_9(answer);
#endif

    //Show answer
    if (0 == strcmp(answer, "STP:  GO\n")) {
        is_stopped     = MOVING;
        if(execution_mode == STOP_MODE)execution_mode = DISTANCE_SELECTION_MODE;
        displayStop(is_stopped  );
    } else if (0 == strcmp(answer, "STP:STOP\n")){
        is_stopped     = STILL;
        execution_mode = STOP_MODE;
        displayStop(is_stopped);
    }else {
        execution_mode = ERROR_MODE;
    }
    return 0;
}

int task_distance()
{

    //clear request and answer
    memset(request,'\0',MESSAGE_LENGTH);
    memset(answer,'\0',MESSAGE_LENGTH);

    // request gas
    strcpy(request, "DS:  REQ\n");
    //uncomment to use the simulator
#ifdef SIMULATOR
    simulator(request, answer);
#endif
    // uncoment to access serial module
#ifndef SIMULATOR
   writeSerialMod_9(request);
   readSerialMod_9(answer);
#endif

    //Show answer
    if (1 == sscanf (answer,"DS:%5d\n",&remaining_distance)) {
        //if(remaining_distance <= 9600) execution_mode = APPROACH_MODE;
        if (execution_mode == DISTANCE_SELECTION_MODE) {
            double tc = -(TARGET_SPEED-speed)/ACCELERATION_SLOPE;
            distance_brake = (-ACCELERATION_SLOPE * tc*tc / 2) + speed*tc    +    speed + 0.75*10; //See design decision in the report

            execution_mode = (remaining_distance <= distance_brake) ? APPROACH_MODE : DISTANCE_SELECTION_MODE;
        }

        displayDistance(remaining_distance);
    }else {
        //error
        execution_mode = ERROR_MODE;
    }
    return 0;
}


int set_emergency_mode()
{   //clear request and answer
    memset(request,'\0',MESSAGE_LENGTH);
    memset(answer,'\0',MESSAGE_LENGTH);

    strcpy(request, "ERR: SET\n");


#ifdef SIMULATOR
        simulator(request, answer);
#endif

#ifndef SIMULATOR
       writeSerialMod_9(request);
       readSerialMod_9(answer);
#endif
    if (0 == strcmp(answer, "ERR:  OK\n")) {
        //Cf design decision
        ;
    } else {
        //Error case
        execution_mode = ERROR_MODE;
    }
    return 0;
}


//void finish_secondary_cycle() {
//
//     long last_time_merged = last_timestamp.tv_sec  * MILLIS_IN_A_SECOND
//                           + last_timestamp.tv_nsec / NANOSECONDS_IN_A_MILLISECOND;
//
//    clock_gettime(CLOCK_REALTIME, &current_timestamp);
//
//    long current_time_merged = current_timestamp.tv_sec  * MILLIS_IN_A_SECOND
//                             + current_timestamp.tv_nsec / NANOSECONDS_IN_A_MILLISECOND;
//
//    long elapsed_time_merged  = current_time_merged - last_time_merged;
//    long time_to_sleep_merged = SECONDARY_CYCLE * MILLIS_IN_A_SECOND - elapsed_time_merged;
//    time_to_sleep.tv_sec      = time_to_sleep_merged / MILLIS_IN_A_SECOND;
//    time_to_sleep.tv_nsec     = (time_to_sleep_merged%1000)*NANOSECONDS_IN_A_MILLISECOND;
//
//    nanosleep(&time_to_sleep, NULL);
//    last_timestamp.tv_sec += SECONDARY_CYCLE;
//}


/**********************************************************
 *  Function: controller
 *********************************************************/

#ifndef WORDING_COMPLIENT
void *controller(void *arg)
{   //http://linux.die.net/man/3/clock_gettime

    // Endless loop
    secondary_cycle.tv_sec  = SECONDARY_CYCLE_S_NWC;
    secondary_cycle.tv_nsec = SECONDARY_CYCLE_NS;
    clock_gettime(CLOCK_REALTIME, &last_timestamp);
    clock_gettime(CLOCK_REALTIME, &last_mixer_timestamp);

    while(1) {
        switch(execution_mode) {
            case DISTANCE_SELECTION_MODE: {
                switch(n_secondary_cycle) {
                    case 0:
                        task_speed();
                        task_slope();
                        task_gas();
                        task_light_sensor();
                        task_lamp();
                        break;
                    case 1:
                        task_mixer();
                        task_brake();
                        task_light_sensor();
                        task_lamp();
                        task_distance();
                        break;
                }
                break;
            }
            case APPROACH_MODE: {
                switch(n_secondary_cycle) {
                    case 0:
                        task_speed();
                        task_slope();
                        task_gas();
                        task_lamp();
                        task_stop();
                        break;
                    case 1:
                        task_speed();
                        task_mixer();
                        task_brake();
                        task_lamp();
                        task_distance();
                        break;
                }
                break;
            }
            case STOP_MODE: {
                switch(n_secondary_cycle) {
                    case 0:
                        task_lamp();
                        task_mixer();
                        task_stop();
                        break;
                    case 1:
                        task_lamp();
                        task_distance();
                        task_stop();
                        break;
                }
                break;
            }
            case ERROR_MODE: {
                switch(n_secondary_cycle) {
                    case 0:
                        set_emergency_mode();
                        task_gas();
                        task_lamp();
                        break;

                    case 1:
                        task_brake();
                        task_lamp();
                        task_mixer();
                        break;
                }
            }
        }

        n_secondary_cycle = (n_secondary_cycle + 1) % (MAJOR_CYCLE_S_NWC / SECONDARY_CYCLE_S_NWC);
        clock_gettime(CLOCK_REALTIME, &current_timestamp);
        diffTime(current_timestamp, last_timestamp, &elapsed_time);
        if(compTime(secondary_cycle, elapsed_time) == -1){
            execution_mode = ERROR_MODE;
        }
        diffTime(secondary_cycle, elapsed_time, &time_to_sleep);
        nanosleep(&time_to_sleep, NULL);
        addTime(last_timestamp, secondary_cycle, &last_timestamp);
    }


}
#endif

#ifdef WORDING_COMPLIENT
void *controller(void *arg)
{   //http://linux.die.net/man/3/clock_gettime

    // Endless loop
    secondary_cycle.tv_sec  = SECONDARY_CYCLE_S_WC;
    secondary_cycle.tv_nsec = SECONDARY_CYCLE_NS;
    clock_gettime(CLOCK_REALTIME, &last_timestamp);
    clock_gettime(CLOCK_REALTIME, &last_mixer_timestamp);

    while(1) {
        switch(execution_mode) {
            case DISTANCE_SELECTION_MODE: {
                switch(n_secondary_cycle) {
                    case 0:
                        task_light_sensor();
                        break;
                    case 1:
                        task_lamp();
                        break;
                    case 2:
                        task_speed();
                        break;
                    case 3:
                        task_slope();
                        break;
                    case 4:
                        task_gas();
                        break;
                    case 5:
                        task_brake();
                        break;
                    case 6:
                        task_distance();
                        break;
                    case 7:
                        task_stop();
                        break;
                    case 8:
                        task_mixer();
                        break;
                    case 9:
                        task_light_sensor();
                        break;
                    case 10:
                        task_lamp();
                        break;
                    case 11:
                        task_speed();
                        break;
                    case 12:
                        task_slope();
                        break;
                    case 13:
                        task_brake();
                        break;
                    case 14:
                        task_gas();
                        break;
                    case 15:
                        task_distance();
                        break;
                    case 16:
                        task_light_sensor();
                        break;
                    case 17:
                        task_lamp();
                        break;
                    case 18:
                        task_stop();
                        break;
                    case 19:
                        task_mixer();
                        break;
                    case 20:
                        task_speed();
                        break;
                    case 21:
                        task_slope();
                        break;
                    case 22:
                        task_light_sensor();
                        break;
                    case 23:
                        task_lamp();
                        break;
                    case 24:
                        task_distance();
                        break;
                    case 25:
                        task_gas();
                        break;
                    case 26:
                        task_brake();
                        break;
                    case 27:
                        task_stop();
                        break;
                    case 28:
                        task_light_sensor();
                        break;
                    case 29:
                        task_lamp();
                        break;
                }
                break;
            }
            //Remeber to double the check speed to ease teh braking precission
            case APPROACH_MODE: {
                switch(n_secondary_cycle) {
                    case 1:
                        task_lamp();
                        break;
                    case 2:
                        task_speed();
                        break;
                    case 3:
                        task_slope();
                        break;
                    case 4:
                        task_gas();
                        break;
                    case 5:
                        task_brake();
                        break;
                    case 6:
                        task_distance();
                        break;
                    case 7:
                        task_stop();
                        break;
                    case 8:
                        task_mixer();
                        break;
                    case 10:
                        task_lamp();
                        break;
                    case 11:
                        task_speed();
                        break;
                    case 12:
                        task_slope();
                        break;
                    case 13:
                        task_brake();
                        break;
                    case 14:
                        task_gas();
                        break;
                    case 15:
                        task_distance();
                        break;
                    case 17:
                        task_lamp();
                        break;
                    case 18:
                        task_stop();
                        break;
                    case 19:
                        task_mixer();
                        break;
                    case 20:
                        task_speed();
                        break;
                    case 21:
                        task_slope();
                        break;
                    case 23:
                        task_lamp();
                        break;
                    case 24:
                        task_distance();
                        break;
                    case 25:
                        task_gas();
                        break;
                    case 26:
                        task_brake();
                        break;
                    case 27:
                        task_stop();
                        break;
                    case 29:
                        task_lamp();
                        break;

                    default:
                        break;
                }
                break;
            }
            case STOP_MODE: {
                switch(n_secondary_cycle) {
                    case 1:
                        task_lamp();
                        break;
                    case 6:
                        task_distance();
                        break;
                    case 7:
                        task_stop();
                        break;
                    case 8:
                        task_mixer();
                        break;
                    case 10:
                        task_lamp();
                        break;
                    case 15:
                        task_distance();
                        break;
                    case 17:
                        task_lamp();
                        break;
                    case 18:
                        task_stop();
                        break;
                    case 19:
                        task_mixer();
                        break;
                    case 23:
                        task_lamp();
                        break;
                    case 24:
                        task_distance();
                        break;
                    case 27:
                        task_stop();
                        break;
                    case 29:
                        task_lamp();
                        break;
                    default:
                        break;
                }
                break;
            }
            case ERROR_MODE: {
                switch(n_secondary_cycle) {
                    case 0:
                        set_emergency_mode();
                        break;
                    case 1:
                        task_gas();
                        break;
                    case 2:
                        task_lamp();
                        break;
                    case 3:
                        task_lamp();
                        break;
                    case 4:
                        break;
                    case 5:
                        task_brake();
                        break;
                    case 6:
                        task_mixer();
                        break;
                    case 7:
                        task_lamp();
                        break;
                    case 8:
                        break;
                    case 9:
                        break;
                    case 10:
                        task_lamp();
                        break;
                    case 11:
                        set_emergency_mode();
                        break;
                    case 12:
                        task_gas();
                        break;
                    case 13:
                        task_lamp();
                        break;
                    case 14:
                        task_lamp();
                        break;
                    case 15:
                        task_distance();
                        break;
                    case 16:
                        task_light_sensor();
                        break;
                    case 17:
                        task_lamp();
                        break;
                    case 18:
                        task_stop();
                        break;
                    case 19:
                        task_mixer();
                        break;
                    case 20:
                        task_speed();
                        break;
                    case 21:
                        task_slope();
                        break;
                    case 22:
                        task_light_sensor();
                        break;
                    case 23:
                        task_lamp();
                        break;
                    case 24:
                        task_distance();
                        break;
                    case 25:
                        task_gas();
                        break;
                    case 26:
                        task_brake();
                        break;
                    case 27:
                        task_stop();
                        break;
                    case 28:
                        task_light_sensor();
                        break;
                    case 29:
                        task_lamp();
                        break;
                    default:
                        break;
                }
                break;
            }
        }

        n_secondary_cycle = (n_secondary_cycle + 1) % (MAJOR_CYCLE_S_WC / SECONDARY_CYCLE_S_WC);
        clock_gettime(CLOCK_REALTIME, &current_timestamp);
        diffTime(current_timestamp, last_timestamp, &elapsed_time);
        if(compTime(secondary_cycle, elapsed_time) == -1){
            execution_mode = ERROR_MODE;
        }
        diffTime(secondary_cycle, elapsed_time, &time_to_sleep);
        nanosleep(&time_to_sleep, NULL);
        addTime(last_timestamp, secondary_cycle, &last_timestamp);
    }


}

#endif

/**********************************************************
 *  Function: main
 *********************************************************/
int main ()
{
    pthread_t thread_ctrl;
    sigset_t alarm_sig;
    int i;

    /* Block all real time signals so they can be used for the timers.
       Note: this has to be done in main() before any threads are created
       so they all inherit the same mask. Doing it later is subject to
       race conditions */
    sigemptyset (&alarm_sig);
    for (i = SIGRTMIN; i <= SIGRTMAX; i++) {
        sigaddset (&alarm_sig, i);
    }
    sigprocmask (SIG_BLOCK, &alarm_sig, NULL);

    // init display
    displayInit(SIGRTMAX);

    // initSerialMod_9600 uncomment to work with serial module
#ifndef SIMULATOR
    initSerialMod_WIN_9600 ();
#endif
    /* Create first thread */
    pthread_create (&thread_ctrl, NULL, controller, NULL);
    pthread_join (thread_ctrl, NULL);
    return (0);
}

