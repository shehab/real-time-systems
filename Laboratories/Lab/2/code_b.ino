#include <string.h>
#include <stdio.h>

#define MILLIS_IN_A_SECOND   1000



#define PIN_LDR                A0
int value;
void setup() {
    Serial.begin(9600);
    pinMode(3      , OUTPUT);
    pinMode(9      , OUTPUT);
    pinMode(PIN_LDR, INPUT);

    TCCR2A = _BV(WGM20) | _BV(WGM21) | _BV(COM2B1) | _BV(COM2A1);
    TCCR2B = _BV(CS22);
    value = 0;
}


void loop() {
    if(value == 0){
        OCR2A = 5;
        OCR2B = 250;
    } else {
        OCR2A = 250;
        OCR2B = 5;
    }
    delay(5000);
    value = 1 - value;
}
