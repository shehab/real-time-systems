dd if=/dev/zero of=disk.img bs=516096c count=60

sudo losetup /dev/loop0 disk.img

sudo fdisk -u –C60 -S63 -H16 /dev/loop0

    n
    p
    1

    t
    83

    a
    1

    p

    w

sudo losetup –d /dev/loop0

sudo losetup –o32256 /dev/loop0 disk.img

sudo mke2fs /dev/loop0

mkdir montaje

sudo mount /dev/loop0 montaje

tar xvzf codigo_apoyo_ej2.tgz

sudo mkdir –p montaje/boot/grub

sudo cp –a Ejercicio2/grub/* montaje/boot/grub

sudo cp -a Ejercicio2/kernel/* montaje/boot

sudo vim montaje/boot/grub/menu.lst

    default 0
    timeout 8
    title Linux_empotrado
    root (hd0,0)
    kernel /boot/vmlinuz-2.6.25.5-1.1-pae root=/dev/sda1
    vga=normal noapic
    initrd /boot/initrd-2.6.25.5-1.1-pae

sudo umount /dev/loop0

sudo losetup –d /dev/loop0

grub --no-floppy

    device (hd0) disk.img
    root (hd0,0)
    setup (hd0)
    quit

qemu -hda disk.img

sudo losetup –o32256 /dev/loop0 disk.img

sudo mount /dev/loop0 montaje

sudo cp –a Ejercicio2/busybox/* montaje

sudo mkdir montaje/dev

sudo vim montaje/init.sh

    #!/bin/sh
    echo “Ejecutando programa inicial ... FIN”

sudo chmod +x montaje/init.sh

sudo vim montaje/boot/grub/menu.lst

    default 0
    timeout 8
    title Linux_empotrado
    root (hd0,0)
    kernel /boot/vmlinuz-2.6.25.5-1.1-pae init=/init.sh
    root=/dev/sda1 vga=normal noapic
    initrd /boot/initrd-2.6.25.5-1.1-pae

sudo umount /dev/loop0

sudo losetup –d /dev/loop0

grub --no-floppy

    device (hd0) disk.img
    root (hd0,0)
    setup (hd0)
    quit

qemu -hda disk.img

sudo losetup –o32256 /dev/loop0 disk.img

sudo mount /dev/loop0 montaje

sudo cp Ejercicio2/reproductor/jp2a montaje

sudo ldd montaje/jp2a

sudo cp -a Ejercicio2/reproductor/lib montaje

sudo mkdir montaje/cdrom

sudo cp Ejercicio2/init.sh montaje

sudo umount /dev/loop0

sudo losetup –d /dev/loop0

qemu –hda disk.img -hdb Ejercicio2/imagenes.iso

