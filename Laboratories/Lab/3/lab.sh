starx

%make sure that the adapter is bridged
%Open two terminals and change to the server one
%Configure the network interface and obtain an IP address using DHCP
sudo ifconfig eth1 up
sudo dhcclient eth1

%install iperf
sudo apt-get install iperf

%create virutal interface that will be used for tests
sudo ifconfig eth1:0 192.168.1.10

%In the server terminal start iperf in server mode
iperf -s -B 192.168.1.10

%In the client terminal, launch the iperf and check the available bandwidth

iperf -c 192.168.1.10


%Create a queue and a root clas
sudo tc qdisc add dev lo root handle 1: htb default 1
sudo tc class add dev lo parent 1: classid 1:1 htb rate 15000kbit ceil 15000kbit

%Change to the client terminal and check the available bandwidth
iperf -c 192.168.1.10

%Add subclasses
sudo tc class add adev lo parent 1:1 classid 1:21 htb rate 10000kbit ceil 10000kbit prio 0
sudo tc class add adev lo parent 1:1 classid 1:22 htb rate 5000kbit ceil 5000kbit prio 1

%Add associated queues
sudo tc qdisc add dev lo parent 1:21 handle 21: sfq perturb 10
sudo tc qdisc add dev lo parent 1:22 handle 22: sfq perturb 10

%Add associated filters
sudo tc filter add dev lo protocol ip parent 1: handle 21 fw classid 1:21
sudo tc filter add dev lo protocol ip parent 1: handle 22 fw classid 1:22

%Mark packets on postrouting
sudo iptables -t mangle -A POSTROUTING -p tcp --dport 4001:6000 -j MARK --set-mark 21
sudo iptables -t mangle -A POSTROUTING -p tcp --dport 6001:8000 -j MARK --set-mark 22

%Check again the client speed
iperf -c 192.168.1.10

%Change the listening port on the server
iperf -s -B 192.168.1.10 -p 7000

%Check again the speed on the client
iperf -c 192.168.1.10 -p 7000