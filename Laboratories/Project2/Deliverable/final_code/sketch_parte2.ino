#define SAMPLE_TIME           250
#define PIN_SPEAKER            11
#define PIN_LED                13
#define PIN_BUTTON              7

#define HAS_BEEN_PRESSED        1
#define HAS_NOT_BEEN_PRESSED    0

#define IS_MUTED                1
#define IS_NOT_MUTED            0

#define MAIN_CYCLE           100
#define SECONDARY_CYCLE      100

#define SAMPLE_FREQUENCY     4000

unsigned long time_origin;

int  melody[] = {3,4,5,5, 4,3,2,1, 1,2,3,3, 2,3,4,5, 5,4,3,2, 1,1,2,3, 2,1,0,0};
long time[]   = {2,1,1,1, 1,1,1,1, 1,1,1,2, 2,2,1,1, 1,1,1,1, 1,1,1,1, 2,1,4,4};
int  tones[]  = {0, 261, 294, 329, 349, 392, 440, 493, 523};
int time_a    = 0;
int time_b    = 0;
int before_a  = 0;
int before_b  = 0;
int after_a   = 0;
int after_b   = 0;
int aux_a     = 0;
int aux_b     = 0;


long tempo         = 180;
long MAX_NOTAS     = 28;
long compValue     = 0;
long CLOCK_SYSTEM  = 16000000;
long preEscalado   = 64;

int button_pressed = 0;
byte released      = 1;
byte muted         = 0;

unsigned long last_timestamp          = 0;
unsigned long current_timestamp       = 0;
unsigned long elapsed_time            = 0;

ISR(TIMER1_COMPA_vect)
{   
    noInterrupts();
    play_bit();
    interrupts();     
}

void play_bit()
{ 
    static unsigned char data = 0;
    if (Serial.available()>1) {
        data = Serial.read();
    }
    if(!muted){
        OCR2A = data;
    }else{ 
    OCR2A = 0;
    }
}

void show_playback_mode() {
    if(check_button() == HAS_BEEN_PRESSED){
        //muted = muted == IS_MUTED ? IS_NOT_MUTED : IS_MUTED;
        noInterrupts();
        muted = 1 - muted;
        interrupts(); 
    }
    digitalWrite(PIN_LED, muted);
}

int check_button() {
    button_pressed = digitalRead(PIN_BUTTON);

    if(released && button_pressed) { //Detect rising edge
        released = 0;
    }

    if(released == 0 && !button_pressed) {
        released = 1;
        return HAS_BEEN_PRESSED;
    } else {
        return HAS_NOT_BEEN_PRESSED;
    }

}

void setup()
{
    pinMode(PIN_SPEAKER, OUTPUT);
    pinMode(PIN_LED, OUTPUT);
    pinMode(PIN_BUTTON, INPUT);
    Serial.begin(115200);

    TCCR1A = 0; // set entire TCCR1A register to 0
    TCCR1B = 0; // same for TCCR1B
    
    TCNT1  = 0; //initialize counter value to 0

    // set compare match register for 1hz increments
    OCR1A = F_CPU / (1 * SAMPLE_FREQUENCY) - 1;// 16,000,000Hz/ (prescaler * desired interrupt frequency) - 1
    // turn on CTC mode
    TCCR1B |= _BV(WGM12);

    // Set CS10 and CS12 bits for 1024 prescaler
    TCCR1B |= _BV(CS10);

    // enable timer compare interrupt, it will be handled by ISR(TIMER1_COMPA_vect)
    TIMSK1 |= _BV(OCIE1A);
    
    TCCR2A = _BV(WGM21) | _BV(WGM20) | _BV(COM2A1);
    TCCR2B = _BV(CS20);
    last_timestamp = micros();
}
void loop()
{
    show_playback_mode();
    
    delay(1);
   
    //COMPLETE CYCLE   
    current_timestamp  = micros();
    elapsed_time       = current_timestamp - last_timestamp; //T = SC - (max_time_int -t1 + t2)

    if(elapsed_time > SECONDARY_CYCLE) {
        Serial.println(last_timestamp);
        Serial.println(current_timestamp);
        Serial.println(elapsed_time);
        Serial.println("The maximum duration of the secondary cycle has been exeeded!\n");
    }

    delayMicroseconds(SECONDARY_CYCLE - elapsed_time);

    last_timestamp += SECONDARY_CYCLE*1000;
}
