/**********************************************************
 *  INCLUDES
 *********************************************************/
#include <vxWorksCommon.h>
#include <vxWorks.h>
#include <stdio.h>
#include <fcntl.h>
#include <ioLib.h>
#include <string.h>
#include <pthread.h>
#include <time.h>
#include <selectLib.h>
#include <string.h>
#include <strings.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/select.h>
#include <sioLib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <errno.h>


// Uncomment to test on the Arduino with the serial module
//
#define PC
#define ONE_BIT
#define SERIAL_BUFFER_SIZE       256
#define PERIOD_TD_SEC              0
#define PERIOD_TD_NSEC     512000000
#define PERIOD_STS_SEC             2
#define PERIOD_STS_NSEC            0
#define PERIOD_SS_SEC              5
#define PERIOD_SS_NSEC             0


#ifndef PC
#include "serialcallLib.h"
#endif
// Uncomment to test on the PC
#ifdef PC
#include "audiocallLib.h"
#endif
//#define PC

/**********************************************************
 *  CONSTANTS
 *********************************************************/
#define NSEC_PER_SEC 1000000000UL
#define DEV_NAME "/tyCo/1"

// Path of audio file in windows
#ifndef ONE_BIT
#define FILE_NAME "host:/C/Users/str/Desktop/let_it_be.raw"
#else
#define FILE_NAME "host:/C/Users/str/Desktop/let_it_be_1bit.raw"
#endif

#ifndef PC
// Uncomment to test on the Arduino
#define PERIOD_TASK_SEC	0			/* Period of Task   */
#define PERIOD_TASK_NSEC  512000000	/* Period of Task   */
#define SEND_SIZE 256    /* BYTES */
#endif
#ifdef PC
// Uncomment to test on the PC
#define PERIOD_TASK_SEC	1			/* Period of Task   */
#define PERIOD_TASK_NSEC  0	/* Period of Task   */
//#define SEND_SIZE 500    /* BYTES */
#define SEND_SIZE 4000    /* BYTES */
#endif

#define PAUSE      1
#define RESUME     0

int state = 0;
//static volatile sig_atomic_t state = 0; 
pthread_mutex_t mutex_state;


/**********************************************************
 *  GLOBALS
 *********************************************************/

/**********************************************************
 *  IMPLEMENTATION
 *********************************************************/

/*
 * Function: unblockRead
 */
int unblockRead(int fd, char *buffer, int size)
{
    fd_set readfd;
    struct timeval wtime;
	int ret;
    
    FD_ZERO(&readfd);
    FD_SET(fd, &readfd);
    wtime.tv_sec=0;
	wtime.tv_usec=0;
    ret = select(2048,&readfd,NULL,NULL,&wtime);
    if (ret < 0) {
    	perror("ERROR: select");
		return ret;
    }
    if (FD_ISSET(fd, &readfd)) {
        ret = read(fd, buffer, size);
        return (ret);
    } else {
 		return (0);
	}
}

/*
 * Function: diffTime
 */
void diffTime(struct timespec end, 
			  struct timespec start, 
			  struct timespec *diff) 
{
	if (end.tv_nsec < start.tv_nsec) {
		diff->tv_nsec = NSEC_PER_SEC - start.tv_nsec + end.tv_nsec;
		diff->tv_sec = end.tv_sec - (start.tv_sec+1);
	} else {
		diff->tv_nsec = end.tv_nsec - start.tv_nsec;
		diff->tv_sec = end.tv_sec - start.tv_sec;
	}
}

/*
 * Function: addTime
 */
void addTime(struct timespec end, 
			  struct timespec start, 
			  struct timespec *add) 
{
	unsigned long aux;
	aux = start.tv_nsec + end.tv_nsec;
	add->tv_sec = start.tv_sec + end.tv_sec + 
			      (aux / NSEC_PER_SEC);
	add->tv_nsec = aux % NSEC_PER_SEC;
}

/*
 * Function: compTime
 */
int compTime(struct timespec t1, 
			  struct timespec t2)
{
	if (t1.tv_sec == t2.tv_sec) {
		if (t1.tv_nsec == t2.tv_nsec) {
			return (0);
		} else if (t1.tv_nsec > t2.tv_nsec) {
			return (1);
		} else if (t1.tv_sec < t2.tv_sec) {
			return (-1);
		}
	} else if (t1.tv_sec > t2.tv_sec) {
		return (1);
	} else if (t1.tv_sec < t2.tv_sec) {
		return (-1);
	} 
	return (0);
}
void transmit_data(int fd) 
{
	struct timespec start,end,diff,cycle;
	int ret = 0;
	int i = 0;
	// loading cycle time
	cycle.tv_sec=PERIOD_TD_SEC;
	cycle.tv_nsec=PERIOD_TD_NSEC;
	clock_gettime(CLOCK_REALTIME,&start);
	while(1){
		
		char buffer[SERIAL_BUFFER_SIZE] = NULL;
		pthread_mutex_lock(&mutex_state);
		if(state == RESUME){
			pthread_mutex_unlock(&mutex_state);
			ret = read(fd, buffer, SERIAL_BUFFER_SIZE);
			if (ret == -1){
				switch(errno){
					case EINTR:
						break;
					case EINVAL:
						break;
					default:
						break;
				}
			}
			
		} else {
			pthread_mutex_unlock(&mutex_state);
			for(i = 0; i < SERIAL_BUFFER_SIZE; i++) buffer[i] = 0;
		}
		//pthread_mutex_unlock(&mutex_state);
	#ifndef PC
			
			// write to serial port  		
			// Uncomment to test on the Arduino
		ret = writeSerialMod_256 (buffer);
	#endif
	#ifdef PC		
			// Uncomment to test on the PC
	
		#ifdef ONE_BIT
		ret = reproducir_1bit_4000 (buffer);
		#else
		ret = reproducir_4000 (buffer);
		#endif
		if (ret < 0) {
					printf("write: error writting serial\n");
					return NULL;
		}
	#endif
	//finish "secondary cycle"
		clock_gettime(CLOCK_REALTIME,&end);
		diffTime(end,start,&diff);
		if (0 >= compTime(cycle,diff)) {
			printf("ERROR: lasted long than the cycle\n");
			return NULL;
		}
		diffTime(cycle,diff,&diff);
		nanosleep(&diff,NULL);   
		addTime(start,cycle,&start);
	}
}

void set_state()
{
	
	int fd;
	int size;
	int ret;
	char buffer_state = 0;
	struct timespec start,end,diff,cycle;
	cycle.tv_sec=PERIOD_STS_SEC;
	cycle.tv_nsec=PERIOD_STS_NSEC;
	clock_gettime(CLOCK_REALTIME,&start);

	while(666){
		ret = unblockread(STDIN_FILENO, &buffer_state, size);
		if(!ret){
			printf("something went wrong with STDIN\n");
		}
		pthread_mutex_lock(&mutex_state);
		
		//state = atoi(buffer_state);
		if(buffer_state == '0')       state = PAUSE;
		else if (buffer_State == '1') state = RESUME;
		pthread_mutex_unlock(&mutex_state);
		
		clock_gettime(CLOCK_REALTIME,&end);
		diffTime(end,start,&diff);
		if (0 >= compTime(cycle,diff)) {
			printf("ERROR: lasted long than the cycle\n");
			return NULL;
		}
		diffTime(cycle,diff,&diff);
		nanosleep(&diff,NULL);   
		addTime(start,cycle,&start);
	}
}

void show_state()
{
	int fd;
	int size;
	int ret;
	char buffer_state = 0;
	struct timespec start,end,diff,cycle;
	cycle.tv_sec=PERIOD_SS_SEC;
	cycle.tv_nsec=PERIOD_SS_NSEC;
	clock_gettime(CLOCK_REALTIME,&start);

	while(666){
		
		pthread_mutex_lock(&mutex_state);
		printf(state == PAUSE ? "Reproduction paused\n" : "Reproduction resumed\n");
		pthread_mutex_unlock(&mutex_state);
		
		clock_gettime(CLOCK_REALTIME,&end);
		diffTime(end,start,&diff);
		if (0 >= compTime(cycle,diff)) {
			printf("ERROR: lasted long than the cycle\n");
			return NULL;
		}
		diffTime(cycle,diff,&diff);
		nanosleep(&diff,NULL);   
		addTime(start,cycle,&start);
	}
	
}
/*****************************************************************************
 * Function: main()
 *****************************************************************************/
int main()
{
    
    unsigned char buf[SEND_SIZE];
    int fd_file = -1;
    int fd_serie = -1;
    int ret = 0;
    pthread_mutex_init(&mutex_state, NULL);
    
#ifndef PC    
	// Uncomment to test on the Arduino
    fd_serie = initSerialMod_WIN_115200 ();
#endif
#ifdef PC    
	// Uncomment to test on the PC
	iniciarAudio_Windows ();
#endif
	/* Open music file */
	printf("open file %s begin\n",FILE_NAME);
	fd_file = open (FILE_NAME, O_RDONLY, 0644);
	if (fd_file < 0) {
		printf("open: error opening file\n");
		return -1;
	}

    
    
}
