#define SAMPLE_TIME           250
#define PIN_SPEAKER            11
#define PIN_LED                13
#define PIN_BUTTON              7

#define HAS_BEEN_PRESSED        1
#define HAS_NOT_BEEN_PRESSED    0

#define IS_MUTED                1
#define IS_NOT_MUTED            0

#define MAIN_CYCLE           100
#define SECONDARY_CYCLE      100

#define SAMPLE_FREQUENCY     4000

unsigned long time_origin;

int  melody[] = {3,4,5,5, 4,3,2,1, 1,2,3,3, 2,3,4,5, 5,4,3,2, 1,1,2,3, 2,1,0,0};
long time[]   = {2,1,1,1, 1,1,1,1, 1,1,1,2, 2,2,1,1, 1,1,1,1, 1,1,1,1, 2,1,4,4};
int  tones[]  = {0, 261, 294, 329, 349, 392, 440, 493, 523};
int time_a    = 0;
int time_b    = 0;
int before_a  = 0;
int before_b  = 0;
int after_a   = 0;
int after_b   = 0;
int aux_a     = 0;
int aux_b     = 0;


long tempo         = 180;
long MAX_NOTAS     = 28;
long compValue     = 0;
long CLOCK_SYSTEM  = 16000000;
long preEscalado   = 64;

int button_pressed = 0;
byte released      = 1;
byte muted         = 0;

unsigned long last_timestamp          = 0;
unsigned long current_timestamp       = 0;
unsigned long elapsed_time            = 0;

ISR(TIMER1_COMPA_vect)
{   
    //before_a = micros();
    noInterrupts();
    play_bit();
    //after_a = micros();
    //((aux_a = after_a - before_a;
    //if(aux_a > time_a) time_a = aux_a;
    //Serial.print("Time a ");
    //Serial.println(time_a);
    interrupts();
}

void play_bit()
{ 
    static int bitwise = 1;
    static unsigned char data = 0;
    bitwise = (bitwise * 2);
    if (bitwise > 128) {
        bitwise = 1;
        if (Serial.available() > 1) {
           data = Serial.read();
        }
    }
    digitalWrite(PIN_SPEAKER, (muted ? 0 : (data & bitwise)));
   
}

void show_playback_mode() {
    if(check_button() == HAS_BEEN_PRESSED){
        //muted = muted == IS_MUTED ? IS_NOT_MUTED : IS_MUTED;
        noInterrupts();
        muted = 1 - muted;
        interrupts(); 
    }
    digitalWrite(PIN_LED, muted);
}

int check_button() {
    button_pressed = digitalRead(PIN_BUTTON);

    if(released && button_pressed) { //Detect rising edge
        released = 0;
    }

    if(released == 0 && !button_pressed) {
        // Lowering edge
        released = 1;
        return HAS_BEEN_PRESSED;
    } else {
        return HAS_NOT_BEEN_PRESSED;
    }

}

void setup()
{
    pinMode(PIN_SPEAKER, OUTPUT);
    pinMode(PIN_LED, OUTPUT);
    pinMode(PIN_BUTTON, INPUT);
    Serial.begin(115200);

    TCCR1A = 0; // set entire TCCR1A register to 0
    TCCR1B = 0; // same for TCCR1B
    
    TCNT1  = 0; //initialize counter value to 0

    // set compare match register for 1hz increments
    OCR1A = F_CPU / (1 * SAMPLE_FREQUENCY) - 1;// 16,000,000Hz/ (prescaler * desired interrupt frequency) - 1
    // turn on CTC mode
    TCCR1B |= _BV(WGM12);

    // Set CS10 and CS12 bits for 1024 prescaler
    TCCR1B |= _BV(CS10);

    // enable timer compare interrupt, it will be handled by ISR(TIMER1_COMPA_vect)
    TIMSK1 |= _BV(OCIE1A);
    
    /*Manu thinks that this should be included as well. Please verify. He might be wrong.*/
    /*
        COM1A1 set compare output mode - fast PWM
        WCM10  waveform generation mode - PWM10 - we dont set it because we dont use it?
    */
    // TCCR1A = _BV(WCM10) | _BV(COM1A1);
    /*End of section */

    // TCCR2A = _BV(COM2A1) | _BV(COM2B1) | _BV(WGM21) | _BV(WGM20);
    // TCCR2B = _BV(CS22);
    // OCR2A  = 180;
    // OCR2B  = 50;

    //time_origin = micros();
    last_timestamp = micros();
}
void loop()
{
    // unsigned long timeDiff;

    // play_bit();
    // timeDiff = SAMPLE_TIME - (micros() - time_origin);
    // time_origin = time_origin + SAMPLE_TIME;
    // delayMicroseconds(timeDiff);

    //show_playback_mode();

    // int nota;
    // int compValue;
    // int tones;
    // for (nota = 0; nota < MAX_NOTAS; nota++) {
    //     int note  = melody[nota];
    //     compValue = CLOCK_SYSTEM / (preEscalado * 4 * tones[note]);
    //     if (melody[nota] == 0) compValue = 0;
    //     OCR2A =     compValue;
    //     OCR2B =     compValue/2;
    //     delay(time[nota]*tempo);
    //     OCR2A =     0;
    //     OCR2B =     0;
    //     delay(tempo/4);
    // }
    
    //before_b = micros();
    show_playback_mode();
    //after_b = micros();
    //aux_b = after_b - before_b;
    delay(1);
    //if(aux_b > time_b) time_b = aux_b;
    //Serial.print("Time b ");
    //Serial.println(time_b);
    //COMPLETE CYCLE
    current_timestamp  = micros();
    elapsed_time       = current_timestamp - last_timestamp; //T = SC - (max_time_int -t1 + t2)

    if(elapsed_time > SECONDARY_CYCLE) {
        //ERROR
        Serial.println(last_timestamp);
        Serial.println(current_timestamp);
        Serial.println(elapsed_time);
        Serial.println("The maximum duration of the secondary cycle has been exeeded!\n");
    }

    //Chech that the amount of milis to sleep is smaller than 10
    delayMicroseconds(SECONDARY_CYCLE - elapsed_time);

    //Either 1 or 2, can we trust our secondary cycle?
    last_timestamp += SECONDARY_CYCLE*1000;
}

/*

pinMode(3, OUTPUT);
pinMode(11, OUTPUT);
TCCR2A = BV(COM2A1) | BV(COM2B1) | BV(WGM21) | BV(WGM20); TCCR2B = BV(CS22);
OCR2A = 180;
OCR2B = 50;

*/

