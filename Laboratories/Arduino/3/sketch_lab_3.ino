///tones = {pausa, c, d, e, f, g, a, b, C}
//tones = {0, 1, 2, 3, 4, 5, 6, 7, 8}
long tones[]  = {0, 261, 294, 329, 349, 392, 440, 493, 523};
//Song: notes and times
long melody[] = {3,4,5,5, 4,3,2,1, 1,2,3,3, 2,3,4,5, 5,4,3,2, 1,1,2,3, 2,1,0,0};
long time[]   = {2,1,1,1, 1,1,1,1, 1,1,1,2, 2,2,1,1, 1,1,1,1, 1,1,1,1, 2,1,4,4};


long tempo        = 180;
long MAX_NOTAS    = 28;
long compValue    = 0;
long CLOCK_SYSTEM = 16000000;
long preEscalado  = 64;
int nota   = 0;
int esNota = 0;

#ifdef PART1
void setup(){
    pinMode(3, OUTPUT);
    pinMode(11, OUTPUT);
    TCCR2A = _BV(COM2A0) | _BV(COM2B1) | _BV(WGM20);
    TCCR2B = _BV(WGM22)  | _BV(CS22);
    //no idea for these values
    OCR2A = 180;
    OCR2B = 50;
}

void loop() {
    long tempo        = 180;
    long MAX_NOTAS    = 28;
    long compValue    = 0;
    long CLOCK_SYSTEM = 16000000;
    long preEscalado  = 64;

    int nota;
    for (nota = 0; nota<MAX_NOTAS; nota++) {
        compValue = CLOCK_SYSTEM/(preEscalado * 4 * tones[melody[nota]]);
        if (melody[nota] == 0) compValue = 0;
        OCR2A = compValue;
        OCR2B = compValue / 2;
        delay(time[nota]*tempo);
        OCR2A = 0;
        OCR2B = 0;
        delay(tempo/4);
    }
}

#endif

#ifndef PART1

ISR(TIMER1_COMPA_vect)
{
    // note_number;
    // compValue = CLOCK_SYSTEM/(1024 * 4 * tones[melody[note_number]]);
    // if (melody[note_number] == 0) compValue = 0;
    // OCR2A = compValue;
    // OCR2B = compValue / 2;
    // delay()
    // OCR1A = F_CPU / (1024 * time[note_number]*tempo / 60) - 1;
    // note_number++;
    if (esNota == 1) {
        compValue=CLOCK_SYSTEM/(preEscalado * 4 * tones[melody[nota]]);
        if (melody[nota] == 0) compValue = 0;
        OCR2A=compValue;
        OCR2B=compValue/2;
        OCR1A=(time[nota]*tempo)*16;
        esNota=1-esNota;
        nota = nota +1;
        if (nota == MAX_NOTAS) nota = 0;
    } else {
        OCR2A=0;
        OCR2B=0;
        OCR1A = (tempo/4)*16;
        esNota=1-esNota;
    }
}

void setup(){
    pinMode(3, OUTPUT);
    pinMode(11, OUTPUT);
    TCCR2A = _BV(COM2A0) | _BV(COM2B1) | _BV(WGM20);
    TCCR2B = _BV(WGM22)  | _BV(CS22);
    //no idea for these values
    OCR2A = 180;
    OCR2B = 50;

    TCCR1A = 0; // set entire TCCR1A register to 0
    TCCR1B = 0; // same for TCCR1B
    TCNT1  = 0; //initialize counter value to 0

    // set compare match register for 1hz increments
    OCR1A = F_CPU / (1024 * tempo * time[0] / 60) - 1;// 16,000,000Hz/ (prescaler * desired interrupt frequency) - 1
    // turn on CTC mode
    TCCR1B |= _BV(WGM12);

    // Set CS10 and CS12 bits for 1024 prescaler
    TCCR1B |= _BV(CS12) | _BV(CS10);

    // enable timer compare interrupt, it will be handled by ISR(TIMER1_COMPA_vect)
    TIMSK1 |= _BV(OCIE1A);
}

void loop() {}

#endif
