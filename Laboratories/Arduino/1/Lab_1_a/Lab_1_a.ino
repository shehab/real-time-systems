//Pin Mapping
#define PIN_LED 9
#define PIN_BUTTON 6

int switch_state = 0;
int state = LOW;
byte released = 1;

void setup()
{
    pinMode(PIN_LED,OUTPUT);
    pinMode(PIN_BUTTON,INPUT);
}

void loop()
{
    switch_state = digitalRead(PIN_BUTTON);
    
    if(released && switch_state == HIGH) { //checks if the button is pressed
        state = state == HIGH ? LOW : HIGH;
        digitalWrite(PIN_LED, state);
        released = 0;
    } else if (switch_state == LOW) {
         released = 1;
    }
}
