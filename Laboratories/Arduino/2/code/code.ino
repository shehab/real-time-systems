#include <string.h>
#include <stdio.h>

#define MILLIS_IN_A_SECOND   1000

/*
 * Constants
 */
// Acceleration in m/s


// Pins

#define PIN_LED_LAMP            9

#define PIN_LDR                A0

//Main and secondadary cycles.
//Decide the secondary and primary cycle.
#define MAIN_CYCLE           200
#define SECONDARY_CYCLE      100
#define NUMBER_SCYCLES         2

byte secondary_cycle                  = 0;
byte ldr_value                        = 0;
long secondary_cycle                  = 0;
long timestamp                        = 0;
long elapsed_time                     = 0;



void setup() {
    Serial.begin(9600);
    pinMode(PIN_LED_LAMP,         OUTPUT);
    pinMode(PIN_LDR,              OUTPUT);
    pinMode(10,                   OUTPUT);

    TCCR1A = _BV(COM1A1) | _BV(COM1B1) | _BV(WGM10);
    TCCR1B = _BV(WGM12)  | _BV(CS11)   | _BV(CS10);
}


void loop() {
    ldr_value = map(analogRead(PIN_LDR), 0, 1023, 0, 255);
    OCR1A     = ldr_value;
    // digitalWrite(PIN_LED_LAMP,         ldr_value);

    // ldr_value = analogRead(PIN_LDR);
    // OCR1A     = ldr_value / 4;

    delay(10);
    //COMPLETE CYCLE
    // delay(1);

    // secondary_cycle    = (secondary_cycle + 1) % (MAIN_CYCLE / SECONDARY_CYCLE);
    // current_timestamp  = millis();
    // elapsed_time       = current_timestamp - last_timestamp; //T = SC - (max_time_int -t1 + t2)

    // if(elapsed_time > SECONDARY_CYCLE) {
    //     //ERROR
    //     Serial.print("The maximum duration of the secondary cycle has been exeeded!\n");
    // }

    //Chech that the amount of milis to sleep is smaller than 10
    // delay(SECONDARY_CYCLE - elapsed_time);

    //Either 1 or 2, can we trust our secondary cycle?
    // last_timestamp += SECONDARY_CYCLE;//1

}
