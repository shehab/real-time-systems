/**********************************************************
 *  INCLUDES
 *********************************************************/
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <ioLib.h>
#include <time.h>
#include "displayB.h"
#include "serialcallLib.h"

/**********************************************************
 *  Constants
 **********************************************************/
#define MAJOR_CYCLE_S                   30
#define SECONDARY_CYCLE_S                6
#define SECONDARY_CYCLE_NS               0
#define MIXER_CYCLE_S                   30
#define MIXER_CYCLE_NS                   0
#define MESSAGE_LENGTH                  10
#define NANOSECONDS_IN_A_SECOND      1000000000
#define NANOSECONDS_IN_A_MILLISECOND 1000000
#define MILLIS_IN_A_SECOND             1000

 // Acceleration in m/s
#define ACCELERATION_SLOPE  0.25f
#define ACCELERATION_GAS     0.5f
#define ACCELERATION_BRAKE  -0.5f

 //Speed values
#define MAX_SPEED            70.0
#define MIN_SPEED            40.0
#define MEDIUM_SPEED         55.0

//Slope values
#define SLOPE_DOWN             -1
#define SLOPE_FLAT              0
#define SLOPE_UP                1

//Binary values for events
#define GAS_ENABLED             1
#define GAS_DISABLED            0
#define BRAKE_ENABLED           1
#define BRAKE_DISABLED          0
#define MIXING_ENABLED          1
#define MIXING_DISABLED         0
#define LAMP_ENABLED            1
#define LAMP_DISABLED           0
#define LIGHT_THRESHOLD        50


#define SIMULATOR
/**********************************************************
 *  Global Variables
 *********************************************************/
//Part A
float speed            = 0.0;
char gas               = 0;
char brake             = 0;
char mixer             = 0;
char time_mixer_called = 0;
int n_secondary_cycle  = 0;
//Part B
char lamp              = 0;
char light_sensor;

char request[MESSAGE_LENGTH];
char answer[MESSAGE_LENGTH];

struct timespec current_timestamp, last_timestamp, time_to_sleep, elapsed_time, secondary_cycle,
       last_mixer_timestamp, current_mixer_timestamp, elapsed_time_mixer, mixer_cycle;

void diffTime(struct timespec end, struct timespec start, struct timespec *diff) {
    if (end.tv_nsec < start.tv_nsec) {
        diff->tv_nsec = NANOSECONDS_IN_A_SECOND - start.tv_nsec + end.tv_nsec;
        diff->tv_sec = end.tv_sec - (start.tv_sec+1);
    } else {
        diff->tv_nsec = end.tv_nsec - start.tv_nsec;
        diff->tv_sec = end.tv_sec - start.tv_sec;
    }
}
void addTime(struct timespec end, struct timespec start, struct timespec *add)
{
    unsigned long aux;
    aux = start.tv_nsec + end.tv_nsec;
    add->tv_sec = start.tv_sec + end.tv_sec +
                  (aux / NANOSECONDS_IN_A_SECOND);
    add->tv_nsec = aux % NANOSECONDS_IN_A_SECOND;
}
int compTime(struct timespec t1, struct timespec t2)
{
    if (t1.tv_sec == t2.tv_sec) {
        if (t1.tv_nsec == t2.tv_nsec) {
            return (0);
        } else if (t1.tv_nsec > t2.tv_nsec) {
            return (1);
        } else if (t1.tv_nsec < t2.tv_nsec) {
            return (-1);
        }
    } else if (t1.tv_sec > t2.tv_sec) {
        return (1);
    } else if (t1.tv_sec < t2.tv_sec) {
        return (-1);
    }
    return (0);
}
/**********************************************************
 *  Function: task_speed
 *********************************************************/
int task_speed()
{

    //--------------------------------
    //  request speed and display it
    //--------------------------------

    //clear request and answer
    memset(request,'\0',MESSAGE_LENGTH);
    memset(answer,'\0',MESSAGE_LENGTH);

    // request speed
    strcpy(request,"SPD: REQ\n");

    //uncomment to use the simulator
#ifdef SIMULATOR
    simulator(request, answer);
#endif
    // uncoment to access serial module
#ifndef SIMULATOR
   writeSerialMod_9(request);
   readSerialMod_9(answer);
#endif

    // display speed
    if (1 == sscanf (answer,"SPD:%f\n",&speed)) {
        displaySpeed(speed);
        //speed < 55.0 ? (brake = 0) & (gas = 1) : (brake = 1) & (gas = 0);
        if (speed >= MEDIUM_SPEED) {
            gas     = GAS_DISABLED;
            brake   = BRAKE_ENABLED;
        }
        if (speed < MEDIUM_SPEED) {
            gas     = GAS_ENABLED;
            brake   = BRAKE_DISABLED;
        }
    }
    return 0;
}

/**********************************************************
 *  Function: task_slope
 *********************************************************/
int task_slope()
{
    // char request[MESSAGE_LENGTH];
    // char answer[MESSAGE_LENGTH];

    //--------------------------------
    //  request slope and display it
    //--------------------------------

    //clear request and answer
    memset(request,'\0',MESSAGE_LENGTH);
    memset(answer,'\0',MESSAGE_LENGTH);

    // request slope
    strcpy(request,"SLP: REQ\n");

    //uncomment to use the simulator
#ifdef SIMULATOR
    simulator(request, answer);
#endif
    // uncoment to access serial module

#ifndef SIMULATOR
   writeSerialMod_9(request);
   readSerialMod_9(answer);
#endif

    // display slope
    if (0 == strcmp(answer,"SLP:DOWN\n")) displaySlope(SLOPE_DOWN);
    if (0 == strcmp(answer,"SLP:FLAT\n")) displaySlope(SLOPE_FLAT);
    if (0 == strcmp(answer,"SLP:  UP\n")) displaySlope(SLOPE_UP);
    else {
        //error case
        ;
    }
    return 0;
}

/**********************************************************
 *  Function: task_gas
 *********************************************************/
int task_gas()
{
    // char request[MESSAGE_LENGTH];
    // char answer[MESSAGE_LENGTH];

    //--------------------------------
    //  request slope and display it
    //--------------------------------

    //clear request and answer
    memset(request,'\0',MESSAGE_LENGTH);
    memset(answer,'\0',MESSAGE_LENGTH);

    // request gas
    gas == GAS_ENABLED ? strcpy(request, "GAS: SET\n") : strcpy(request, "GAS: CLR\n");
    //uncomment to use the simulator
#ifdef SIMULATOR
    simulator(request, answer);
#endif
    // uncoment to access serial module
#ifndef SIMULATOR
   writeSerialMod_9(request);
   readSerialMod_9(answer);
#endif

    //Show answer
    if (0 == strcmp(answer, "GAS:  OK\n")) {
        displayGas(gas);
    } else {
        //Error case
        ;
    }
    return 0;
}

/**********************************************************
 *  Function: task_brake
 *********************************************************/
int task_brake()
{
    // char request[MESSAGE_LENGTH];
    // char answer[MESSAGE_LENGTH];

    //--------------------------------
    //  request slope and display it
    //--------------------------------

    //clear request and answer
    memset(request,'\0',MESSAGE_LENGTH);
    memset(answer,'\0',MESSAGE_LENGTH);

    // request gas
    brake == BRAKE_ENABLED ? strcpy(request, "BRK: SET\n") : strcpy(request, "BRK: CLR\n");
    //uncomment to use the simulator
#ifdef SIMULATOR
    simulator(request, answer);
#endif
    // uncoment to access serial module
#ifndef SIMULATOR
   writeSerialMod_9(request);
   readSerialMod_9(answer);
#endif

    //Show answer
    if (0 == strcmp(answer, "BRK:  OK\n")) {
        displayBrake(brake);
    } else {
        //Error case
        ;
    }
    return 0;
}

/**********************************************************
 *  Function: task_mixer
 *********************************************************/
int task_mixer()
{
    //if(time_mixer_called == 0) {
    clock_gettime(CLOCK_REALTIME, &current_mixer_timestamp);
    diffTime(current_mixer_timestamp, last_mixer_timestamp, &elapsed_time_mixer);
    if(compTime(mixer_cycle, elapsed_time_mixer) == -1){
        mixer = 1 - mixer;
    //clear request and answer
    memset(request,'\0',MESSAGE_LENGTH);
    memset(answer,'\0',MESSAGE_LENGTH);

    clock_gettime(CLOCK_REALTIME, &last_mixer_timestamp);
    mixer == MIXING_ENABLED ? strcpy(request, "MIX: SET\n") : strcpy(request, "MIX: CLR\n");

#ifdef SIMULATOR
    simulator(request, answer);
#endif

#ifndef SIMULATOR
   writeSerialMod_9(request);
   readSerialMod_9(answer);
#endif

    //Show answer
        if (0 == strcmp(answer, "MIX:  OK\n")) {
            displayMix(mixer);

        } else {
            //Error case
            ;
        }
    }
    //}

    //time_mixer_called = (time_mixer_called + 1) % 2;
    return 0;
}

int task_light_sensor()
{

    //--------------------------------
    //  request speed and display it
    //--------------------------------

    //clear request and answer
    memset(request,'\0',MESSAGE_LENGTH);
    memset(answer,'\0',MESSAGE_LENGTH);

    // request speed
    strcpy(request,"LIT: REQ\n");

        //uncomment to use the simulator
#ifdef SIMULATOR
        simulator(request, answer);
#endif
        //Check #endifwhat amount of time has passed, and consider the previous state of the mixer

        // uncoment to access serial module
#ifndef SIMULATOR
       writeSerialMod_9(request);
       readSerialMod_9(answer);
#endif
       printf(answer);
       printf("\n");
    if (1 == sscanf (answer,"LIT: %d%\n",&light_sensor)) {
        lamp = light_sensor >= LIGHT_THRESHOLD ? LAMP_DISABLED : LAMP_ENABLED;
        printf("light_sensor = %d \n", light_sensor);
        printf("lamp = %d \n", lamp);
        displayLightSensor(lamp);
        //speed < 55.0 ? (brake = 0) & (gas = 1) : (brake = 1) & (gas = 0);
    }else {
        //error
        ;
    }

    // display speed
    return 0;
}

int task_lamp()
{

    //--------------------------------
    //  request speed and display it
    //--------------------------------

    //clear request and answer
    memset(request,'\0',MESSAGE_LENGTH);
    memset(answer,'\0',MESSAGE_LENGTH);

    // request speed
    lamp == LAMP_ENABLED ? strcpy(request, "LAM: SET\n") : strcpy(request, "LAM: CLR\n");
    printf(request);
    printf("\n");
        //uncomment to use the simulator
#ifdef SIMULATOR
        simulator(request, answer);
#endif
        //Check #endifwhat amount of time has passed, and consider the previous state of the mixer

        // uncoment to access serial module
#ifndef SIMULATOR
       writeSerialMod_9(request);
       readSerialMod_9(answer);
#endif
       printf(answer);
       printf("\n");
    if (0 == strcmp(answer, "LAM:  OK\n")) {
        displayLamps(lamp);
    } else {
        //Error case
        ;
    }

    // display speed
    return 0;
}

void finish_secondary_cycle() {

     long last_time_merged = last_timestamp.tv_sec  * MILLIS_IN_A_SECOND
                                 + last_timestamp.tv_nsec / NANOSECONDS_IN_A_MILLISECOND;

    clock_gettime(CLOCK_REALTIME, &current_timestamp);

    long current_time_merged = current_timestamp.tv_sec  * MILLIS_IN_A_SECOND
                             + current_timestamp.tv_nsec / NANOSECONDS_IN_A_MILLISECOND;

    long elapsed_time_merged = current_time_merged - last_time_merged;
    long time_to_sleep_merged = SECONDARY_CYCLE_S * MILLIS_IN_A_SECOND - elapsed_time_merged;
    time_to_sleep.tv_sec = time_to_sleep_merged / MILLIS_IN_A_SECOND;
    time_to_sleep.tv_nsec = (time_to_sleep_merged%1000)*NANOSECONDS_IN_A_MILLISECOND;
    //time_to_sleep.tv_nsec = (time_to_sleep_merged * NANOSECONDS_IN_A_MILLISECOND) % NANOSECONDS_IN_A_SECOND;
    printf("Time to sleep in seconds  %d and nanoseconds %dl \n", (long)time_to_sleep.tv_sec, time_to_sleep.tv_nsec);


    printf("Time to sleep = %d \n", time_to_sleep_merged);
    printf("current_timestamp = %d \n", current_time_merged);
    printf("last_timestamp = %d \n", last_time_merged);

    nanosleep(&time_to_sleep, NULL);
    last_timestamp.tv_sec += SECONDARY_CYCLE_S;
    //clock_gettime(CLOCK_REALTIME, &last_timestamp);



}


/**********************************************************
 *  Function: controller
 *********************************************************/
void *controller(void *arg)
{   //http://linux.die.net/man/3/clock_gettime

    // Endless loop
    secondary_cycle.tv_sec  = SECONDARY_CYCLE_S;
    secondary_cycle.tv_nsec = SECONDARY_CYCLE_NS;
    mixer_cycle.tv_sec      = MIXER_CYCLE_S;
    mixer_cycle.tv_nsec      = MIXER_CYCLE_NS;
    clock_gettime(CLOCK_REALTIME, &last_timestamp);
    clock_gettime(CLOCK_REALTIME, &last_mixer_timestamp);
    while(1) {
        task_light_sensor();
        task_lamp();
        switch (n_secondary_cycle) {
            case 0: {
                task_speed();
                task_slope();
                task_gas();
                task_brake();
                break;
            }
            case 1: {
                task_mixer();
                break;
            }
            case 2: {
                task_speed();
                task_slope();
                task_gas();
                task_brake();
                break;
            }
            case 3: {
                task_mixer();
                break;
            }
            case 4: {
                task_speed();
                task_slope();
                task_gas();
                task_brake();
                break;
            }
        }
        n_secondary_cycle = (n_secondary_cycle + 1) % (MAJOR_CYCLE_S/SECONDARY_CYCLE_S);
        clock_gettime(CLOCK_REALTIME, &current_timestamp);
        diffTime(current_timestamp, last_timestamp, &elapsed_time);
        if(compTime(secondary_cycle, elapsed_time) == -1){
            printf("The maximum duration of the secondary cycle has been exeeded!\n");
        }
        diffTime(secondary_cycle, elapsed_time, &time_to_sleep);
        nanosleep(&time_to_sleep, NULL);
        addTime(last_timestamp, secondary_cycle, &last_timestamp);
        //finish_secondary_cycle();
    }
}

/**********************************************************
 *  Function: main
 *********************************************************/
int main ()
{
    pthread_t thread_ctrl;
    sigset_t alarm_sig;
    int i;

    /* Block all real time signals so they can be used for the timers.
       Note: this has to be done in main() before any threads are created
       so they all inherit the same mask. Doing it later is subject to
       race conditions */
    sigemptyset (&alarm_sig);
    for (i = SIGRTMIN; i <= SIGRTMAX; i++) {
        sigaddset (&alarm_sig, i);
    }
    sigprocmask (SIG_BLOCK, &alarm_sig, NULL);

    // init display
    displayInit(SIGRTMAX);

    // initSerialMod_9600 uncomment to work with serial module
#ifndef SIMULATOR
    initSerialMod_WIN_9600 ();
#endif
    /* Create first thread */
    pthread_create (&thread_ctrl, NULL, controller, NULL);
    pthread_join (thread_ctrl, NULL);
    return (0);
}

